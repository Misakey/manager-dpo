import os
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

engine = create_engine(
    os.environ["SQLALCHEMY_URL"], client_encoding="utf8", pool_size=10, max_overflow=20
)
session = scoped_session(sessionmaker(bind=engine))


def close_db(e=None):
    session.remove()
