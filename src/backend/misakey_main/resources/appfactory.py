from flask_api import FlaskAPI
from flask_cors import CORS

from misakey_main.resources.config import load_config
from misakey_main.resources.routeur import attach_routes
from misakey_main.resources.errors import attach_errors
from misakey_main.resources.auth import Authenticator
from misakey_main.resources.config import config

def create_app():
    app = FlaskAPI(__name__)

    cors = CORS(app, 
        origins=config["ACCESS_CONTROL_ALLOW_ORIGIN"],
    )

    attach_errors(app)

    auth = Authenticator(app)

    app.auth = auth
    attach_routes(app)

    app.config['MAX_CONTENT_LENGTH'] = 10 * 1024 * 1024

    return app
