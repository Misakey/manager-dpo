"""Routes attached with mandates
"""
from misakey_main.controllers.message_controller import MessageController
from flask_cors import cross_origin


def attach_routes(app, prefix="/messages"):
    @app.route(prefix + "", methods=["POST"])
    @cross_origin()
    @app.auth.requires_auth
    def add_message():
        controller = MessageController()
        return controller.create()

    @app.route(prefix + "/<message_uuid>/files/<file_uuid>/download", methods=["GET"])
    @cross_origin()
    @app.auth.requires_auth
    def get_encrypted_file(message_uuid, file_uuid):
        """Accept TOS for authenticated user

        Returns:
            Object: { }
        """
        controller = MessageController()
        return controller.download_encrypted_file(message_uuid, file_uuid)

    @app.route(prefix + "/<message_uuid>/files/<file_uuid>", methods=["GET"])
    @cross_origin()
    @app.auth.requires_auth
    def get_encrypted_file_metadata(message_uuid, file_uuid):
        """Accept TOS for authenticated user

        Returns:
            Object: { }
        """
        controller = MessageController()
        return controller.get_encrypted_file_metadata(message_uuid, file_uuid)
