"""Routes attached with companies
"""
from misakey_main.controllers.company_controller import CompanyController
from flask_cors import cross_origin


def attach_routes(app, prefix="/companies"):
    """Attach all companies routes to the app.

    Args:
        app: The app to attach the routes
        prefix (str, optional): Path prefix for all routes
    """

    @app.route(prefix + "/<uuid>/<email>", methods=["HEAD"])
    @cross_origin()
    def check_if_company_exists(uuid, email):
        controller = CompanyController()
        return controller.check_if_company_exists(uuid, email)

    @app.route(prefix + "/<uuid>/<email>", methods=["GET"])
    @cross_origin()
    def get_company_from_email(uuid, email):
        controller = CompanyController()
        return controller.get_company_from_email(uuid, email)

    @app.route(prefix + "/<uuid>/users", methods=["GET"])
    @cross_origin()
    @app.auth.requires_auth
    def get_users_linked_to_company(uuid):
        """Accept TOS for authenticated user

        Returns:
            Object: { }
        """
        controller = CompanyController()
        return controller.get_users_list(uuid)

    @app.route(prefix + "/<uuid>", methods=["GET"])
    @cross_origin()
    @app.auth.requires_auth
    def get_company_details(uuid):
        """Get company details

        Args:
            uuid (String): UUID

        Returns:
            Array: [ {
                uuid: String,
                name: String,
                logo: String (url),
                dpo_email: String,
                is_active: Boolean,
                status: Enum,
                date_creation: Timestamp,
                last_interaction: Timestamp,
                messages: [ {

                } ]
            }]
        """
        controller = CompanyController()
        return controller.get_company_details(uuid)
