""" Routes associated with users
"""
from misakey_main.controllers.user_controller import UserController
from misakey_main.controllers.dpo_controller import DpoController
from flask_cors import cross_origin


def attach_routes(app, prefix="/users"):
    """Attach all user routes to the app.

    Args:
        app (TYPE): The app to attach the routes
        prefix (str, optional): Path prefix for all routes
    """

    @app.route(prefix + "/auth0/<sub>", methods=["GET"])
    @cross_origin()
    @app.auth.requires_auth
    def get_user_profile(sub):
        """ Get user profile

        Args:
            sub (string): The auth0 ID

        Returns:
            Object: User JSON
        """
        controller = UserController()
        return controller.get_user_profile_from_auth0(sub)

    @app.route(prefix, methods=["POST"])
    @cross_origin()
    def signup():
        """ Create the user

        Returns:
            Object: User JSON
        """
        controller = UserController()
        return controller.signup()

    @app.route(prefix + "/<email>", methods=["HEAD"])
    @cross_origin()
    def user_exists(email):
        controller = UserController()
        return controller.does_user_exist(email)

    @app.route(prefix + "/<uuid>", methods=["GET"])
    @cross_origin()
    @app.auth.requires_auth
    def get_user_as_dpo(uuid):
        controller = UserController()
        return controller.get_user_as_dpo(uuid)

    @app.route(prefix + "/<uuid>/mandate", methods=["GET"])
    @cross_origin()
    @app.auth.requires_auth
    def get_user_mandate(uuid):
        controller = UserController()
        return controller.get_user_mandate(uuid)

    @app.route(prefix + "/<uuid>/pubkey", methods=["GET"])
    @cross_origin()
    @app.auth.requires_auth
    def get_user_pubkey(uuid):
        controller = UserController()
        return controller.get_user_pubkey(uuid)

    @app.route(prefix + "/<uuid>/companies", methods=["GET"])
    @cross_origin()
    @app.auth.requires_auth
    def get_companies_linked_to_user(uuid):
        """Accept TOS for authenticated user

        Returns:
            Object: { }
        """
        controller = UserController()
        return controller.get_companies_list(uuid)

    @app.route(prefix + "/publicKey", methods=["POST"])
    @cross_origin()
    @app.auth.requires_auth
    def add_public_key():
        """ Add a public key for authenticated user

        Returns:
            Object: { }
        """
        controller = UserController()
        return controller.add_public_key()

    @app.route(prefix + "/is-owner/publicKey/<path:key>", methods=["GET"])
    @cross_origin()
    @app.auth.requires_auth
    def check_public_key_ownership(key):
        """Check if public key is owned by authenticated user
        
        Args:
            key (string): Public key to check
        
        Returns:
            Object: { 'is_owner': Boolean }
        """
        controller = UserController()
        return controller.check_public_key_ownership(key)

    @app.route(prefix + "/end-onboarding", methods=["GET"])
    @cross_origin()
    @app.auth.requires_auth
    def end_onboarding():
        """Set onboarding status to completed for authenticated user
        
        Returns:
            Object: User
        """
        controller = UserController()
        return controller.end_onboarding()

    @app.route(prefix, methods=["DELETE"])
    @cross_origin()
    @app.auth.requires_auth
    def delete_user():
        """ Delete user (from auth0, our DB)
        
        Returns:
            Object: { }
        """
        controller = UserController()
        return controller.delete_user()
