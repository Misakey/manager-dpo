""" Routes associated with users
"""
from misakey_main.controllers.dpo_controller import DpoController
from flask_cors import cross_origin


def attach_routes(app, prefix="/dpos"):
    """Attach all user routes to the app.

    Args:
        app (TYPE): The app to attach the routes
        prefix (str, optional): Path prefix for all routes
    """

    @app.route(prefix, methods=["POST"])
    @cross_origin()
    def signup_dpo():
        """ Create the user

        Returns:
            Object: User JSON
        """
        controller = DpoController()
        return controller.signup()
