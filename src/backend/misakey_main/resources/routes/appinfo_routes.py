"""Routes attached with mandates
"""
from flask_cors import cross_origin

from flask import current_app as app

def attach_routes(app, prefix="/appinfo"):
    @app.route(prefix + "", methods=["GET"])
    @cross_origin()
    def get_info():
        # TODO: To dynamise
        return {
            "app_version": "1.0.1"
        }

    @app.route(prefix + "/ping", methods=["GET"])
    @cross_origin()
    def ping():
        app.logger.info("API Pingged")
        return {
            "ping": "pong"
        }
