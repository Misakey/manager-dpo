from misakey_main.resources.routes.appinfo_routes import attach_routes as attach_appinfo_routes
from misakey_main.resources.routes.user_routes import attach_routes as attach_user_routes
from misakey_main.resources.routes.message_routes import (
    attach_routes as attach_message_routes,
)
from misakey_main.resources.routes.company_routes import (
    attach_routes as attach_company_routes,
)
from misakey_main.resources.routes.dpos_routes import attach_routes as attach_dpos_routes


def attach_routes(app):
    attach_user_routes(app, "/users")
    attach_message_routes(app, "/messages")
    attach_company_routes(app, "/companies")
    attach_dpos_routes(app, "/dpos")
    attach_appinfo_routes(app, "/appinfo")