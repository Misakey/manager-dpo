from os import environ, path
from flask.config import Config


def load_config(config):
    config.from_pyfile("../config.cfg")
    # Check if local configuration is enabled
    if (
        environ.get("MISAKEY_CONF_FILE_PATH") is not None
        and len(environ.get("MISAKEY_CONF_FILE_PATH")) > 0
    ):
        config.from_envvar("MISAKEY_CONF_FILE_PATH")
    
    for key, value in config.items():
        if key in environ and environ[key]:
            config[key] = environ[key]

# This config object is used for non flask applications
config = Config(path.dirname(path.abspath(__file__)))
load_config(config)
