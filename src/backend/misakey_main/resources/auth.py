from functools import wraps
import json
from six.moves.urllib.request import urlopen

from flask import request, _request_ctx_stack
from jose import jwt

from misakey_main.resources.errors import AuthError
from misakey_main.resources.config import config


class Authenticator:
    def __init__(self, app):
        self.app = app
        self.algorithms = ["RS256"]
        self.api_identifier = config["AUTH0_API_IDENTIFIER"]
        self.auth0_domain = config["AUTH0_DOMAIN"]

    def get_token_auth_header(self):
        """Obtains the access token from the Authorization Header
        """
        auth = request.headers.get("Authorization", None)
        if not auth:
            raise AuthError(
                {
                    "code": "authorization_header_missing",
                    "description": "Authorization header is expected",
                },
                401,
            )

        parts = auth.split()

        if parts[0].lower() != "bearer":
            raise AuthError(
                {
                    "code": "invalid_header",
                    "description": "Authorization header must start with" " Bearer ",
                },
                401,
            )
        elif len(parts) == 1:
            raise AuthError(
                {"code": "invalid_header", "description": "Token not found"}, 401
            )
        elif len(parts) > 2:
            raise AuthError(
                {
                    "code": "invalid_header",
                    "description": "Authorization header must be" " Bearer token",
                },
                401,
            )

        token = parts[1]
        return token

    def requires_scope(self, required_scope):
        """Determines if the required scope is present in the access token
        Args:
            required_scope (str): The scope required to access the resource
        """
        token = self.get_token_auth_header()
        unverified_claims = jwt.get_unverified_claims(token)
        if unverified_claims.get("scope"):
            token_scopes = unverified_claims["scope"].split()
            for token_scope in token_scopes:
                if token_scope == required_scope:
                    return True
        return False

    def requires_auth(self, f):
        """Determines if the access token is valid
        """

        @wraps(f)
        def decorated(*args, **kwargs):
            token = self.get_token_auth_header()
            jsonurl = urlopen("https://" + self.auth0_domain + "/.well-known/jwks.json")
            jwks = json.loads(jsonurl.read())
            try:
                unverified_header = jwt.get_unverified_header(token)
            except jwt.JWTError:
                raise AuthError(
                    {
                        "code": "invalid_header",
                        "description": "Invalid header. "
                        "Use an RS256 signed JWT Access Token",
                    },
                    401,
                )
            if unverified_header["alg"] == "HS256":
                raise AuthError(
                    {
                        "code": "invalid_header",
                        "description": "Invalid header, "
                        "Use an RS256 signed JWT Access Token",
                    },
                    401,
                )
            rsa_key = {}
            for key in jwks["keys"]:
                if key["kid"] == unverified_header["kid"]:
                    rsa_key = {
                        "kty": key["kty"],
                        "kid": key["kid"],
                        "use": key["use"],
                        "n": key["n"],
                        "e": key["e"],
                    }
            if rsa_key:
                try:
                    payload = jwt.decode(
                        token,
                        rsa_key,
                        algorithms=self.algorithms,
                        audience=self.api_identifier,
                        issuer="https://" + self.auth0_domain + "/",
                    )
                except jwt.ExpiredSignatureError:
                    raise AuthError(
                        {"code": "token_expired", "description": "token is expired"},
                        401,
                    )
                except jwt.JWTClaimsError as e:
                    raise e
                    raise AuthError(
                        {
                            "code": "invalid_claims",
                            "description": "incorrect claims,"
                            " please check the audience and issuer",
                        },
                        401,
                    )
                except Exception:
                    raise AuthError(
                        {
                            "code": "invalid_header",
                            "description": "Unable to parse authentication" " token.",
                        },
                        401,
                    )

                _request_ctx_stack.top.current_user = payload
                _request_ctx_stack.top.access_token = token
                return f(*args, **kwargs)
            raise AuthError(
                {
                    "code": "invalid_header",
                    "description": "Unable to find appropriate key",
                },
                401,
            )

        return decorated
