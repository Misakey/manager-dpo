import uuid
from misakey_main.resources.errors import InvalidInputError
from misakey_main.resources.tools import is_valid_email


def extract_and_validate_signup_data(request):
    data = request.form

    sub = data.get("sub", "")
    first_name = data.get("firstName", "")
    family_name = data.get("familyName", "")
    email = data.get("email", "")
    country = data.get("country", "")

    if "signature" not in request.files:
        raise InvalidInputError(
            {
                "code": "file_not_found",
                "description": "Please select a file as signature",
            }
        )
    signature = request.files["signature"]

    if (
        len(first_name) == 0
        or len(family_name) == 0
        or len(email) == 0
        or len(country) == 0
        or len(sub) == 0
    ):
        raise InvalidInputError(
            {
                "code": "uncomplete_fieldlist",
                "description": "Please fill all input fields: family name, first name, email & sub",
            }
        )

    if not is_valid_email(email):
        raise InvalidInputError(
            {"code": "invalid_email", "description": "The email is not valid"}
        )

    return sub, first_name, family_name, email, country, signature


def extract_and_validate_signup_dpo_data(request):
    data = request.get_json()

    sub = data.get("sub", "")
    email = data.get("email", "")
    company_uuid = data.get("company_uuid", "")

    print(company_uuid)
    try:
        uuid.UUID(company_uuid)
    except Exception as e:
        raise InvalidInputError(
            {"code": "invalid_uuid", "description": "The provided uuid is invalid !!"}
        )

    if len(company_uuid) == 0 or len(email) == 0 or len(sub) == 0:
        raise InvalidInputError(
            {
                "code": "uncomplete_fieldlist",
                "description": "Please fill all input fields: company_uuid, email, sub",
            }
        )

    return sub, email, company_uuid
