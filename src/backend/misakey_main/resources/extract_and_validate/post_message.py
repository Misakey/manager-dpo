import uuid

from flask import current_app as app

from misakey_main.resources.errors import InvalidInputError

from misakey_main.models.messages import MessageType

from misakey_main.resources.api_clients.s3 import S3APIClient

from werkzeug.utils import secure_filename


def extract_and_validate_post_message_data(request):
    # data = request.get_json()
    data = request.get_json() or request.form

    message_type = data.get("message_type", "")
    answering_to = sub = data.get("answering_to", "")

    if len(message_type) == 0 or len(answering_to) == 0:
        raise InvalidInputError(
            {
                "code": "uncomplete_fieldlist",
                "description": "Please fill all input fields: message_type & answering_to",
            }
        )
    try:
        casted_message_type = MessageType[message_type]
        return casted_message_type, answering_to
    except:
        raise InvalidInputError(
            {
                "code": "unknown_message_type",
                "description": "The message type is invalid",
            }
        )

def host_included_encrypted_files(request, max_files = 10):
    data = request.get_json() or request.form

    files = []

    # Security: Don't allow too many files
    if len(request.files) > max_files:
        raise ServerError({"code": "upload_error", "description": "Too many files"}, 500)

    # Note: Total file size is already limited to 10M

    s3 = S3APIClient()

    for file_field in request.files:
        file = request.files[file_field]

        if file.filename == "":
            # No file selected
            app.logger.info("No File selected")
            continue

        file_key_field = file_field + "_key"
        if file_key_field not in data or len(data[file_key_field]) > 100:
            app.logger.info("Missing pubkey")
            app.logger.info(file_key_field)
            # Missing file pubkey for signature
            continue

        file_nonce_field = file_field + "_nonce"
        if file_nonce_field not in data or len(data[file_nonce_field]) > 100:
            # Missing file nonce for decryption
            app.logger.info("Missing nonce")
            continue

        file.filename = secure_filename(file.filename)
        if len(file.filename) > 100:
            # File name too long
            app.logger.info("Filename too long")

            continue

        filename = str(uuid.uuid4())
        s3.addEncryptedFile(file, filename)

        files.append({
            'name': filename,
            'sign_pubkey':  data[file_key_field],
            'nonce': data[file_nonce_field],
        })

    return files
