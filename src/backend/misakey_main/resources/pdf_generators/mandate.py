from PyPDF2 import PdfFileWriter, PdfFileReader
import io
import tempfile
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
import os


def generate_mandate_pdf(fullname, email, country, date, signature_path):
    packet = io.BytesIO()
    # create a new PDF with Reportlab
    can = canvas.Canvas(packet)
    # can.setFont("Arial", 20)
    can.drawString(175, 658, fullname)
    can.drawString(162, 642, country)
    can.drawString(72, 412, email)
    can.drawString(311, 360, fullname)
    can.drawString(311, 214, date)

    can.drawImage(signature_path, 310, 50, width=250, height=122, mask="auto")

    # Isertt image

    can.save()

    # move to the beginning of the StringIO buffer
    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    # read your existing PDF

    template_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "templates/Mandat-template.pdf"
    )
    existing_pdf = PdfFileReader(open(template_path, "rb"))
    output = PdfFileWriter()
    # add the "watermark" (which is the new pdf) on the existing page
    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)
    # finally, write "output" to a real file

    pdf_path = "{}.pdf".format(signature_path.split(".")[0])

    outputStream = open(pdf_path, "wb")
    output.write(outputStream)
    outputStream.close()

    return pdf_path
