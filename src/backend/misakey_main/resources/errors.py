from flask import jsonify


# Format error response and append status code.
class AuthError(Exception):
    def __init__(self, error, status_code):
        self.error = error
        self.status_code = status_code


class ForbidenError(Exception):
    def __init__(self, error):
        self.error = error
        self.status_code = 403


class ServerError(Exception):
    def __init__(self, error, status_code):
        self.error = error
        self.status_code = status_code


class InvalidInputError(Exception):
    def __init__(self, error):
        self.error = error
        self.status_code = 400


def attach_errors(app):
    @app.errorhandler(AuthError)
    def handle_auth_error(ex):
        response = jsonify(ex.error)
        response.status_code = ex.status_code
        return response

    @app.errorhandler(ForbidenError)
    def handle_forbiden_error(ex):
        response = jsonify(ex.error)
        response.status_code = ex.status_code
        return response

    @app.errorhandler(ServerError)
    def handle_server_error(ex):
        response = jsonify(ex.error)
        response.status_code = ex.status_code
        return response

    @app.errorhandler(InvalidInputError)
    def handle_invalid_input_error(ex):
        response = jsonify(ex.error)
        response.status_code = ex.status_code
        return response
