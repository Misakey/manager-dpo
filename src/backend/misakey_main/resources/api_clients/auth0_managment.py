import http.client
import json
from flask import current_app as app

from misakey_main.resources.config import config
from misakey_main.resources.errors import ServerError


class Auth0ManagmentAPIClient:
    def __init__(self):
        self.domain = config["AUTH0_DOMAIN"]
        self.client_id = config["AUTH0_CLIENT_ID"]
        self.client_secret = config["AUTH0_CLIENT_SECRET"]
        self.api_identifier = config["AUTH0_API_IDENTIFIER"]
        self.refresh_connexion()

    def refresh_connexion(self):
        self.conn = http.client.HTTPSConnection(self.domain)

    def get_token(self):
        payload = {
            "client_id": self.client_id,
            "client_secret": self.client_secret,
            "audience": self.api_identifier,
            "grant_type": "client_credentials",
        }

        headers = {"content-type": "application/json"}

        self.conn.request("POST", "/oauth/token", json.dumps(payload), headers)

        res = self.conn.getresponse()
        data = res.read()

        token = json.loads(data.decode("utf-8"))

        if 'error' in token:
            raise ServerError(
                {
                    "code": token['error'],
                    "description": token['error_description'],
                },
                500,
            )
        else:
            return token

    def request(self, method, path, payload=None):
        payloadString = None
        token = self.get_token()
        print(token)
        headers = {"Authorization": token["token_type"] + " " + token["access_token"]}

        if payload:
            headers["Content-Type"] = "application/json"
            payloadString = json.dumps(payload)

        self.conn.request(method, path, payloadString, headers)

        return self.conn.getresponse()

    def delete_user(self, auth0_id):
        return self.request("DELETE", "/api/v2/users/" + auth0_id)

    def user_exists(self, auth0_id):
        status = self.request("GET", "/api/v2/users/" + auth0_id).status
        if status == 200:
            return True
        elif status == 404:
            return False
        raise ServerError(
            {
                "code": "auth0_error",
                "description": "Error while checking user into Auth0 - Response status: {}".format(
                    status
                ),
            },
            500,
        )
