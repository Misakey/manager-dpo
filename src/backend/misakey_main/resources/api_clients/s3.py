import boto3

from misakey_main.resources.config import config
from misakey_main.resources.errors import ServerError


class S3APIClient:
    def __init__(self):
        try:
            self.conn = boto3.client(
                "s3",
                region_name=config["AWS_S3_REGION"],
                aws_access_key_id=config["AWS_API_KEY"],
                aws_secret_access_key=config["AWS_API_SECRET"],
            )
        except Exception as e:
            raise ServerError({"code": "s3_connexion", "description": str(e)}, 500)

    def addSignature(self, signature, filename):
        try:
            self.conn.upload_fileobj(
                signature, config["AWS_S3_SIGNATURE_BUCKET"], filename
            )
        except Exception as e:
            raise ServerError({"code": "upload_error", "description": str(e)}, 500)

    def getSignature(self, filename):
        try:
            tmp_file_path = "/tmp/signature_{}.png".format(filename)
            self.conn.download_file(
                config["AWS_S3_SIGNATURE_BUCKET"],
                filename,
                tmp_file_path,
            )
            return tmp_file_path
        except Exception as e:
            print(str(e))
            raise ServerError({"code": "upload_error", "description": str(e)}, 500)

    def addEncryptedFile(self, encrypted_file, filename):
        try:
            self.conn.upload_fileobj(
                encrypted_file, config["AWS_S3_ENCRYPTED_FILE_BUCKET"], filename
            )
        except Exception as e:
            raise ServerError({"code": "upload_error", "description": str(e)}, 500)

    def getEncryptedFile(self, filename):
        try:
            tmp_file_path = "/tmp/encrypted_file_{}.png".format(filename)
            self.conn.download_file(
                config["AWS_S3_ENCRYPTED_FILE_BUCKET"],
                filename,
                tmp_file_path,
            )
            return tmp_file_path
        except Exception as e:
            print(str(e))
            raise ServerError({"code": "upload_error", "description": str(e)}, 500)