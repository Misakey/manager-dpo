import requests
import json
from flask import _request_ctx_stack
from misakey_main.resources.errors import ServerError

from misakey_main.resources.config import config

class Auth0AuthAPIClient:
    def __init__(self):
        self.endpoint = config["AUTH0_AUTH_ENDPOINT"]

    def request(self, path, should_auth=True):
        headers = {}
        if should_auth:
            headers = {
                "Content-Type": "application/json",
                "Authorization": "Bearer {0}".format(
                    _request_ctx_stack.top.access_token
                ),
            }
        try:
            response = requests.get("{}{}".format(self.endpoint, path), headers=headers)
            return response
        except Exception as e:
            raise ServerError(
                {
                    "code": "auth0__error",
                    "description": "An error occured while fetching {} from Auth0: {}".format(
                        path, str(e)
                    ),
                },
                500,
            )

    def get_user_info(self):
        response = self.request("userinfo")

        if response.status_code == 200:
            return json.loads(response.content.decode("utf-8"))
        raise ServerError(
            {
                "code": "auth0_userinfo_error",
                "description": "An error occured while fetching the userinfo on Auth0. Response code : {}".format(
                    response.status_code
                ),
            },
            500,
        )
