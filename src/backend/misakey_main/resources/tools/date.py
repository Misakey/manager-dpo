from datetime import datetime


def is_today(date):
    return date.date() == datetime.today().date()


def is_last_n_days(date, n):
    return date > datetime.now() - datetime.timedelta(days=n)
