import re


def is_valid_email(email):
    if re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", email) != None:
        return True
    return False


def allowed_file(filename, allowed_extensions=set(["png", "jpg", "jpeg", "gif"])):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in allowed_extensions


def get_country_string_from_code(country_code):
    # Todo
    return "France"
