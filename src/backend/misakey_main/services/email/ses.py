import boto3
from time import sleep

from misakey_main.services.email import IEmailService
from misakey_main.resources.config import config

class SesEmailService(IEmailService):
    def __init__(self):
        aws_api_key = config["AWS_API_KEY"]
        aws_api_secret = config["AWS_API_SECRET"]
        aws_region = config["AWS_SES_REGION"]

        self.ses_client = boto3.client(
            "ses",
            region_name=aws_region,
            aws_access_key_id=aws_api_key,
            aws_secret_access_key=aws_api_secret,
        )

    def send_email(self, email_object):
        try:
            ses_ret = self.ses_client.send_raw_email(
                RawMessage={"Data": email_object.as_bytes()}
            )
            sleep(1/14)
            return True, ses_ret
        except Exception as e:
            return False, e
