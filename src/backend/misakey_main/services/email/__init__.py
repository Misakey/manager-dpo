import abc


class IEmailService(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def send_email(self, email_object):
        raise NotImplementedError("users must define __str__ to use this base class")
