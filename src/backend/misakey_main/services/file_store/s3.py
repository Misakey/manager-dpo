import boto3

from misakey_main.services.file_store import IFileStoreService
from misakey_main.resources.config import config


class S3FileStoreService(IFileStoreService):
    def __init__(self):
        aws_api_key = config["AWS_API_KEY"]
        aws_api_secret = config["AWS_API_SECRET"]
        aws_region = config["AWS_S3_REGION"]

        self.s3_client = boto3.client(
            "s3",
            region_name=aws_region,
            aws_access_key_id=aws_api_key,
            aws_secret_access_key=aws_api_secret,
        )

    def send_file(self, remote_path, remote_store, local_path=None, file_object=None):
        # Remote path = Object key
        # Remote store = Bucket name
        try:
            if local_path != None:
                infos = self.s3_client.upload_file(
                    local_path, remote_store, remote_path
                )
                return True, infos

            if file_object != None:
                infos = self.s3_client.upload_fileobj(
                    file_object, remote_store, remote_path
                )
                return True, infos
        except Exception as e:
            return False, str(e)
