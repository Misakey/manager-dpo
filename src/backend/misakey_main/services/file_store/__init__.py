import abc


class IFileStoreService(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def send_file(self, remote_path, remote_store, local_path=None, file_object=None):
        raise NotImplementedError("FileStore should define a send file method")
