import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

import os
import sys
import logging
from misakey_main.resources.config import config

from misakey_main.resources import create_app
from misakey_main.resources.database import session

if 'SENTRY_DSN' in os.environ and os.environ['SENTRY_DSN']:
    sentry_sdk.init(
        dsn=os.environ['SENTRY_DSN'],
        integrations=[FlaskIntegration()],
        release="misakey-backend@1.0.1" # TODO: to dynamise
    )

logger = logging.getLogger('flask.app')

logger.setLevel(logging.DEBUG)

logFormatter = logging.Formatter(
    '[%(asctime)s] %(levelname)s in %(module)s: %(message)s'
)

default_handler = logging.StreamHandler(sys.stderr)
default_handler.setFormatter(logFormatter)
logger.addHandler(default_handler)

if config['LOG_FILE_PATH']:
    fileHandler = logging.FileHandler(config['LOG_FILE_PATH'])
    fileHandler.setFormatter(logFormatter)
    logger.addHandler(fileHandler)


app = create_app()


@app.teardown_appcontext
def cleanup(resp_or_exc):
    session.remove()


if __name__ == "__main__":
    app.run(debug=True)
