# coding=utf-8

from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship

from misakey_main.models import Base


class DpoCompanyLink(Base):
    __tablename__ = "dpo_company_links"

    id = Column(Integer, primary_key=True, autoincrement=True)

    dpo_id = Column(Integer, ForeignKey("dpos.id"))
    dpo = relationship("Dpo", back_populates="companies")

    company_id = Column(Integer, ForeignKey("companies.id"))
    company = relationship("Company", back_populates="dpos")

    def __init__(self, user_id=None, company_id=None):
        self.user_id = user_id
        self.company_id = company_id

    def __repr__(self):
        return "<DCL(dpo={did}, company={cid})>".format(
            did=self.dpo_id, cid=self.company_id
        )
