""" Module for handling all database models.
Notes:
    The models created with the inherited `Base` constant
    must be imported below the declaration for `Alembic`
    autogenerate to work.
"""

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.event import listens_for

Base = declarative_base()

# Will need to import all the models
from misakey_main.models.users import User
from misakey_main.models.keys import Key
from misakey_main.models.mandates import Mandate
from misakey_main.models.companies import Company
from misakey_main.models.dpos import Dpo
from misakey_main.models.dpo_company_links import DpoCompanyLink
from misakey_main.models.user_company_links import UserCompanyLink, LinkStatus
from misakey_main.models.messages import Message, MessageType
from misakey_main.models.emails import Email


@listens_for(UserCompanyLink, "after_insert")
def insert_first_message(mapper, connection, ucl):
    message_table = Message.__table__

    first_message = Message(MessageType.accessRequestAsked, ucl.id)

    connection.execute(
        message_table.insert(),
        uuid=first_message.uuid,
        message_type=first_message.message_type,
        date=first_message.date,
        user_company_link_id=first_message.user_company_link_id,
    )


@listens_for(Message, "after_insert")
def update_ucl_date(mapper, connection, message):
    ucl_table = UserCompanyLink.__table__
    user_company_link_id = message.user_company_link_id

    if type(message.message_type) is str:
        message_type_value = MessageType[message.message_type].value
    else:
        message_type_value = message.message_type.value

    message_type_value_mod = message_type_value % 100
    new_status = None

    if message_type_value_mod == 10:
        new_status = LinkStatus.unread
    elif message_type_value_mod == 20 or message_type_value_mod == 21:
        new_status = LinkStatus.data
    elif message_type_value_mod == 30 or message_type_value_mod == 22:
        new_status = LinkStatus.no_data

    if new_status is None:
        connection.execute(
            ucl_table.update()
            .where(ucl_table.c.id == user_company_link_id)
            .values(last_interaction=message.date)
        )
    else:
        connection.execute(
            ucl_table.update()
            .where(ucl_table.c.id == user_company_link_id)
            .values(last_interaction=message.date, status=new_status)
        )
