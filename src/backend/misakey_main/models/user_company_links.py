# coding=utf-8

import enum
from datetime import datetime

from sqlalchemy import Column, Integer, Enum, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from misakey_main.models import Base


class LinkStatus(enum.Enum):
    disabled = 0
    unread = 10
    read = 20
    data = 30
    no_data = 40


class UserCompanyLink(Base):
    __tablename__ = "user_company_links"

    id = Column(Integer, primary_key=True, autoincrement=True)
    status = Column(Enum(LinkStatus))

    last_interaction = Column(DateTime)

    user_id = Column(Integer, ForeignKey("users.id"))
    user = relationship("User", back_populates="companies")

    company_id = Column(Integer, ForeignKey("companies.id"))
    company = relationship("Company", back_populates="users")

    messages = relationship(
        "Message",
        back_populates="user_company_link",
        order_by="asc(Message.date)",
        passive_deletes=True,
    )
    # TODO: When implementing the message system, add a hook (https://stackoverflow.com/questions/12512662/hooking-into-sqlalchemy-models)
    # To update "last_interaction" on any add of messages

    def __init__(self, user_id=None, company_id=None):
        self.user_id = user_id
        self.company_id = company_id
        self.status = LinkStatus.unread
        self.last_interaction = datetime.utcnow()

    def __repr__(self):
        return "<Link(user={uid}, company={cid}, status={status})>".format(
            uid=self.user_id, cid=self.company_id, status=self.status
        )

    def export(self, side="company", include_messages=False):
        if side == "company":
            to_export = self.company.export()
            to_export["company_status"] = to_export["status"]
        else:
            if self.status != LinkStatus.disabled:
                to_export = self.user.export_for_dpo()

        if to_export:
            to_export["last_interaction"] = (
                self.last_interaction.timestamp() if self.last_interaction else -1.0
            )
            to_export["status"] = self.status.name

            messages = [m for m in self.messages]

            if len(messages) > 0:
                to_export["last_message"] = messages[-1].export()

            if include_messages:
                messages_list = []
                for message in messages:
                    if side == "company" or message.message_type.value % 100 != 0:
                        messages_list.append(message.export())
                to_export["messages"] = messages_list

            return to_export
        return None
