# coding=utf-8

import enum
import uuid

from sqlalchemy import Column, String, Integer, DateTime, Enum, ForeignKey, Text, JSON
from sqlalchemy.orm import relationship
from datetime import datetime
from sqlalchemy.dialects.postgresql import UUID

from misakey_main.models import Base


class MessageType(enum.Enum):
    accessRequestAsked = 100
    accessRequestSent = 110
    accessAcceptData = 121
    accessAcceptNoData = 122
    accessRefusal = 130

    portabilityRequestAsked = 200
    portabilityRequestSent = 210
    portabilityAccept = 220
    portabilityRefusal = 230

    deleteRequestAsked = 300
    deleteRequestSent = 310
    deleteAccept = 320
    deleteRefusal = 330

    welcomeMessage = 500


class Message(Base):
    __tablename__ = "messages"

    id = Column(Integer, primary_key=True, autoincrement=True)
    uuid = Column(UUID(as_uuid=True))  # Maybe not useful ?

    message_type = Column(Enum(MessageType))

    complementary_informations = Column(Text)

    date = Column(DateTime)

    user_company_link_id = Column(
        Integer, ForeignKey("user_company_links.id", ondelete="CASCADE")
    )
    user_company_link = relationship("UserCompanyLink", back_populates="messages")

    files = Column(JSON)

    def __init__(self, message_type=None, user_company_link_id=None):
        self.uuid = uuid.uuid4()
        self.message_type = message_type
        self.user_company_link_id = user_company_link_id
        self.date = datetime.utcnow()
        self.files = {}

    def __repr__(self):
        return "<Message(id={id}, type={type}, date={date})>".format(
            id=self.id, type=self.message_type, date=self.date
        )

    def export(self):
        return {
            "uuid": self.uuid,
            "message_type": self.message_type.name,
            "date": self.date.timestamp() if self.date else -1.0,
            "complementary_informations": self.complementary_informations,
            "files": self.files,
        }
