# coding=utf-8

import enum
import uuid
from datetime import datetime
from sqlalchemy import Column, String, Integer, Boolean, DateTime, Enum
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID

from misakey_main.models import Base
from misakey_main.models.companies import CompanyStatus


class OnboardingStatus(enum.Enum):
    verify_email_pending = 0
    email_double_opted_in = 10
    tos_accepted = 20
    mandate_signed = (
        30
    )  # MAybe merge with TOS. And work with email , wait for it to be verified
    key_generated = 40
    completed = 100


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, autoincrement=True)
    uuid = Column(UUID(as_uuid=True))
    auth0_id = Column(String)  # sub from oAuth provider
    is_active = Column(Boolean)
    email = Column(String)
    onboarding_status = Column(Enum(OnboardingStatus))
    date_creation = Column(DateTime)
    date_ready = Column(DateTime)

    date_accept_tos = Column(DateTime)

    mandates = relationship("Mandate", back_populates="user")
    keys = relationship("Key", back_populates="user")

    companies = relationship(
        "UserCompanyLink",
        back_populates="user",
        order_by="desc(UserCompanyLink.last_interaction)",
    )

    def __init__(self, auth0_id=None, email=None, is_active=True):
        self.uuid = uuid.uuid4()
        self.auth0_id = auth0_id
        self.email = email
        self.is_active = is_active
        self.onboarding_status = OnboardingStatus.verify_email_pending
        self.date_creation = datetime.utcnow()

    def __repr__(self):
        return "<User(id={id}, email={email}, active={active})>".format(
            id=self.id, email=self.email, active=self.is_active
        )

    def export(self):
        if self.onboarding_status == OnboardingStatus.completed:
            return {
                "uuid": str(self.uuid),
                "type": "user",
                "auth0_id": self.auth0_id,
                "email": self.email,
                "is_active": self.is_active,
                "onboarding_status": self.onboarding_status.name,
                "date_creation": self.date_creation.timestamp()
                if self.date_creation
                else -1.0,
                "date_ready": self.date_ready.timestamp() if self.date_ready else -1.0,
                "key": self.keys[0].export(),
            }

        return {
            "uuid": str(self.uuid),
            "type": "user",
            "auth0_id": self.auth0_id,
            "email": self.email,
            "is_active": self.is_active,
            "onboarding_status": self.onboarding_status.name,
            "date_creation": self.date_creation.timestamp()
            if self.date_creation
            else -1.0,
            "date_ready": self.date_ready.timestamp() if self.date_ready else -1.0,
        }

    def export_for_dpo(self):
        if self.is_active:
            return {
                "uuid": str(self.uuid),
                "email": self.email,
                "fullName": "{} {}".format(
                    self.mandates[0].first_name, self.mandates[0].family_name
                ),
            }
        return None

    def export_companies_list(self):
        companies_list = []
        for company_link in self.companies:
            if company_link.company.status != CompanyStatus.init_idle:
                companies_list.append(company_link.export())
        return companies_list
