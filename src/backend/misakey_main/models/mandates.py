# coding=utf-8

from sqlalchemy import Column, String, Integer, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from datetime import datetime

from misakey_main.models import Base


class Mandate(Base):
    __tablename__ = "mandates"

    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey("users.id"))
    user = relationship("User", back_populates="mandates")
    first_name = Column(String)
    family_name = Column(String)
    email = Column(String)
    country = Column(String)
    date_creation = Column(DateTime)

    signature = Column(String)

    def __init__(
        self, user_id=None, first_name=None, family_name=None, email=None, country=None
    ):
        self.user_id = user_id
        self.first_name = first_name
        self.family_name = family_name
        self.country = country
        self.email = email
        self.date_creation = datetime.utcnow()

    def __repr__(self):
        return "<Mandate(id={id}, user={user}, email={email})>".format(
            id=self.id, user=self.user_id, email=self.email
        )

    def export(self):
        return {
            "id": self.id,
            "user_id": self.user_id,
            "email": self.first_name,
            "email": self.family_name,
            "email": self.email,
            "date_creation": self.date_creation.timestamp()
            if self.date_creation
            else -1.0,
        }
