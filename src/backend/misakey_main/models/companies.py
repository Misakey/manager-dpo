# coding=utf-8

import enum
import uuid
import json
from base64 import b64encode

from sqlalchemy import Column, String, Integer, Boolean, DateTime, Enum, Text
from sqlalchemy.orm import relationship
from datetime import datetime
from sqlalchemy.dialects.postgresql import UUID

from misakey_main.models import Base
from misakey_main.models.dpo_company_links import DpoCompanyLink
from misakey_main.models.user_company_links import LinkStatus

from misakey_main.resources.config import config


class CompanyStatus(enum.Enum):
    init_idle = 0
    # Will need to add: ghost_not_onboarded & ghost_onboarded
    not_onboarded = 10
    onboarded = 20
    blacklisted = 30
    disabled = 40
    refused = 50
    not_concerned = 60
    static = 99


class Company(Base):
    __tablename__ = "companies"

    id = Column(Integer, primary_key=True, autoincrement=True)
    uuid = Column(UUID(as_uuid=True))

    name = Column(String)
    logo = Column(String)
    homepage = Column(String)
    domain = Column(String)
    comment = Column(Text)

    dpo_email = Column(String)

    is_active = Column(Boolean)

    status = Column(Enum(CompanyStatus))

    date_creation = Column(DateTime)
    last_weekly_email_date = Column(DateTime)

    sorting_weight = Column(Integer)

    users = relationship(
        "UserCompanyLink",
        back_populates="company",
        order_by="desc(UserCompanyLink.last_interaction)",
    )

    dpos = relationship("DpoCompanyLink", back_populates="company")

    emails = relationship("Email", back_populates="company")

    def __init__(
        self,
        name=None,
        logo=None,
        homepage=None,
        domain=None,
        dpo_email=None,
        is_active=True,
        sorting_weight=None,
    ):
        self.uuid = uuid.uuid4()
        self.name = name
        self.logo = logo
        self.homepage = homepage
        self.domain = domain
        self.dpo_email = dpo_email
        self.is_active = is_active
        self.status = CompanyStatus.init_idle
        self.date_creation = datetime.utcnow()
        self.sorting_weight = sorting_weight

    def __repr__(self):
        return "<Company(id={id}, email={email}, active={active})>".format(
            id=self.id, email=self.dpo_email, active=self.is_active
        )

    def export(self):
        return {
            "uuid": str(self.uuid),
            "name": self.name,
            "logo": self.logo,
            "dpo_email": self.dpo_email,
            "is_active": self.is_active,
            "status": self.status.name,
            "date_creation": self.date_creation.timestamp()
            if self.date_creation
            else -1.0,
        }

    def export_users_list(self):
        users_list = []
        for user_link in self.users:
            if (
                user_link is not None
                and len(user_link.messages) > 0
                and user_link.status != LinkStatus.disabled
            ):
                users_list.append(user_link.export(side="user"))
        return users_list

    def generate_register_link(self):
        base_url = config["DPO_CONSOLE_URL"]

        verification_str = b64encode(
            json.dumps({"email": self.dpo_email, "uuid": str(self.uuid)}).encode("utf8")
        ).decode('utf8')
        return "{}/dpo/{}".format(base_url, verification_str)
