# coding=utf-8

import uuid
from datetime import datetime
from sqlalchemy import Column, String, Integer, Boolean, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID

from misakey_main.models import Base


class Dpo(Base):
    __tablename__ = "dpos"

    id = Column(Integer, primary_key=True, autoincrement=True)
    uuid = Column(UUID(as_uuid=True))
    auth0_id = Column(String)  # sub from oAuth provider
    is_active = Column(Boolean)
    email = Column(String)
    date_creation = Column(DateTime)
    date_onboarding = Column(DateTime)
    date_accept_tos = Column(DateTime)

    companies = relationship("DpoCompanyLink", back_populates="dpo")

    def __init__(self, auth0_id=None, email=None, is_active=True):
        self.uuid = uuid.uuid4()
        self.auth0_id = auth0_id
        self.email = email
        self.is_active = is_active
        self.date_creation = datetime.utcnow()

    def __repr__(self):
        return "<Dpo(id={id}, email={email}, active={active})>".format(
            id=self.id, email=self.email, active=self.is_active
        )

    def export(self):
        if len(self.companies) > 0:
            return {
                "uuid": str(self.uuid),
                "type": "dpo",
                "auth0_id": self.auth0_id,
                "email": self.email,
                "is_active": self.is_active,
                "date_creation": self.date_creation.timestamp()
                if self.date_creation
                else -1.0,
                "date_onboarding": self.date_onboarding.timestamp()
                if self.date_onboarding
                else -1.0,
                "date_accept_tos": self.date_accept_tos.timestamp()
                if self.date_accept_tos
                else -1.0,
                "company": {
                    "uuid": str(self.companies[0].company.uuid),
                    "name": self.companies[0].company.name,
                    "logo": self.companies[0].company.logo,
                },
            }

        return {
            "uuid": str(self.uuid),
            "type": "dpo",
            "auth0_id": self.auth0_id,
            "email": self.email,
            "is_active": self.is_active,
            "date_creation": self.date_creation.timestamp()
            if self.date_creation
            else -1.0,
            "date_onboarding": self.date_onboarding.timestamp()
            if self.date_onboarding
            else -1.0,
            "date_accept_tos": self.date_accept_tos.timestamp()
            if self.date_accept_tos
            else -1.0,
        }
