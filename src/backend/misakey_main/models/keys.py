# coding=utf-8

from sqlalchemy import Column, Integer, Text, ForeignKey
from sqlalchemy.orm import relationship

from misakey_main.models import Base


class Key(Base):
    __tablename__ = "keys"

    id = Column(Integer, primary_key=True, autoincrement=True)
    public_key = Column(Text)
    name = Column(Text)

    user_id = Column(Integer, ForeignKey("users.id"))
    user = relationship("User", back_populates="keys")

    def __init__(self, user_id=None, name=None, public_key=None):
        self.user_id = user_id
        self.name = name
        self.public_key = public_key

    def __repr__(self):
        return "<Key(id={id}, user={user}, name={name}, public_key={public_key})>".format(
            id=self.id, user=self.user_id, name=self.name, public_key=self.public_key
        )

    def export(self):
        return {"name": self.name, "pubkey": self.public_key}
