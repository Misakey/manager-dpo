import json
import requests
import uuid
from datetime import datetime

from flask import request
from flask_api import status
from sqlalchemy import update, and_, delete

from misakey_main.controllers import Controller
from misakey_main.models import User, Company, UserCompanyLink
from misakey_main.resources.errors import ServerError, InvalidInputError
from misakey_main.resources.api_clients.auth0_managment import Auth0ManagmentAPIClient
from misakey_main.resources.api_clients.auth0_auth import Auth0AuthAPIClient

from flask import current_app as app


class CompanyController(Controller):
    def __init__(self):
        Controller.__init__(self)

    def get_companies_list(self):

        user = self.get_user()

        return user.export_companies_list()

    def check_if_company_exists(self, uuid, email):
        self.validate_uuid(uuid)

        company = (
            self.dbSession.query(Company)
            .filter(Company.uuid == uuid)
            .filter(Company.dpo_email == email)
            .one_or_none()
        )
        if company:
            if len(company.dpos) == 0:
                return None, status.HTTP_204_NO_CONTENT
            return {}, status.HTTP_200_OK
        return {}, status.HTTP_404_NOT_FOUND

    def get_company_from_email(self, uuid, email):
        self.validate_uuid(uuid)
        company = (
            self.dbSession.query(Company)
            .filter(Company.uuid == uuid)
            .filter(Company.dpo_email == email)
            .one_or_none()
        )
        if company:
            return {"name": company.name, "logo": company.logo}
        else:
            return {}, status.HTTP_404_NOT_FOUND

    def get_users_list(self, uuid):
        self.validate_uuid(uuid)
        dpo = self.get_dpo()
        company = (
            self.dbSession.query(Company).filter(Company.uuid == uuid).one_or_none()
        )

        if company is None:
            raise InvalidInputError(
                {
                    "code": "company_not_found",
                    "description": "The company doesn't exists",
                }
            )

        doesCompanyBelongToDpo = False
        for company_link in dpo.companies:
            if company.uuid == company_link.company.uuid:
                doesCompanyBelongToDpo = True
        if not doesCompanyBelongToDpo:
            raise InvalidInputError(
                {
                    "code": "company_not_found",
                    "description": "The company doesn't belong to this dpo",
                }
            )

        return company.export_users_list()

    def get_company_details(self, uuid):
        self.validate_uuid(uuid)
        user = self.get_user()
        company = (
            self.dbSession.query(Company).filter(Company.uuid == uuid).one_or_none()
        )
        if company:
            ucl = (
                self.dbSession.query(UserCompanyLink)
                .filter(UserCompanyLink.user_id == user.id)
                .filter(UserCompanyLink.company_id == company.id)
                .one_or_none()
            )
            if ucl:
                return ucl.export(include_messages=True)

        raise InvalidInputError(
            {"code": "company_not_found", "description": "The company doesn't exists"}
        )
