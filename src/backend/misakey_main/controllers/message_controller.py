from datetime import datetime
import json

from flask import request, send_file

from misakey_main.controllers import Controller
from misakey_main.models import Message
from misakey_main.models.messages import MessageType
from misakey_main.resources.errors import ForbidenError

from misakey_main.resources.extract_and_validate.post_message import (
    extract_and_validate_post_message_data,
    host_included_encrypted_files,
)
from misakey_main.resources.api_clients.s3 import S3APIClient


from flask import current_app as app


class MessageController(Controller):
    def __init__(self):
        Controller.__init__(self)
        self.s3 = S3APIClient()

    def create(self):
        message_type, answering_to = extract_and_validate_post_message_data(request)

        authed, authed_type = self.get_authenticated_person()

        original_message = (
            self.dbSession.query(Message)
            .filter(Message.uuid == answering_to)
            .one_or_none()
        )
        if original_message is None:
            raise ForbidenError(
                {
                    "code": "message_not_yours",
                    "description": "You don't own this message",
                }
            )

        if (
            authed_type == "user"
            and original_message.user_company_link.user.uuid == authed.uuid
        ) or (
            authed_type == "dpo"
            and original_message.user_company_link.company.uuid
            == authed.companies[0].company.uuid
        ):
            new_message = Message(message_type, original_message.user_company_link.id)
            if message_type == MessageType.portabilityAccept and request.files:
                new_message.files = host_included_encrypted_files(request)

            self.dbSession.add(new_message)
            self.dbSession.commit()
            return {}

    def get_encrypted_file(self, message_uuid, file_uuid):
        user, authed_type = self.get_authenticated_person()

        db_file = None
        if authed_type == "user":
            message = self.dbSession.query(Message).filter(Message.uuid == message_uuid).one_or_none()
            if message is not None:
                if message.user_company_link.user.uuid == user.uuid:
                    for file in message.files:
                        if file['name'] == file_uuid:
                            db_file = file

        if db_file is None:
            raise ForbidenError(
                {
                    "code": "message_not_yours",
                    "description": "You don't own this file",
                }
            )
        return db_file

    def get_encrypted_file_metadata(self, message_uuid, file_uuid):
        file = self.get_encrypted_file(message_uuid, file_uuid)
        return file

    def download_encrypted_file(self, message_uuid, file_uuid):
        file = self.get_encrypted_file(message_uuid, file_uuid)

        encrypted_file_path = self.s3.getEncryptedFile(file_uuid)

        return send_file(encrypted_file_path)
