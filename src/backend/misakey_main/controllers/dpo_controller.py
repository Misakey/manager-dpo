from datetime import datetime

from flask import request
from flask_api import status


from misakey_main.resources.api_clients.auth0_managment import Auth0ManagmentAPIClient
from misakey_main.controllers import Controller
from misakey_main.models import Dpo, DpoCompanyLink, Company
from misakey_main.models.companies import CompanyStatus
from misakey_main.resources.errors import InvalidInputError, ServerError
from misakey_main.resources.extract_and_validate.signup import (
    extract_and_validate_signup_dpo_data,
)


class DpoController(Controller):
    def __init__(self):
        Controller.__init__(self)
        self.auth0_managment = Auth0ManagmentAPIClient()

    def signup(self):
        sub, email, company_uuid = extract_and_validate_signup_dpo_data(request)

        dpo = self.get_dpo(exception_on_none=False, sub=sub)
        if dpo is not None:
            raise InvalidInputError(
                {
                    "code": "user_exists",
                    "description": "An user already exists with this sub",
                }
            )

        if not self.auth0_managment.user_exists(sub):
            raise InvalidInputError(
                {
                    "code": "user_not_exists",
                    "description": "No user registred with this sub",
                }
            )

        company = (
            self.dbSession.query(Company)
            .filter(Company.uuid == company_uuid)
            .filter(Company.dpo_email == email)
            .one_or_none()
        )
        if company is None:
            raise InvalidInputError(
                {
                    "code": "company_not exists",
                    "description": "No company matching this uuid",
                }
            )
        dpo = Dpo(sub, email)
        dpo.date_creation = datetime.utcnow()
        dpo.date_accept_tos = datetime.utcnow()
        dpo.date_onboarding = datetime.utcnow()

        dcl = DpoCompanyLink()
        dcl.dpo = dpo

        company.dpos.append(dcl)
        company.status = CompanyStatus.onboarded

        self.dbSession.add(dcl)

        self.dbSession.commit()
        return dpo.export()
