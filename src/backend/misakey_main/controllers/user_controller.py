from datetime import datetime
import json

from flask import request, send_file
from flask import current_app as app
from sqlalchemy import update, and_, delete, func, or_

from werkzeug.utils import secure_filename

import boto3

from misakey_main.controllers import Controller
from misakey_main.models import User, Key, Mandate, Company, UserCompanyLink, Message
from misakey_main.models.users import OnboardingStatus
from misakey_main.models.user_company_links import LinkStatus
from misakey_main.models.companies import CompanyStatus
from misakey_main.models.messages import MessageType
from misakey_main.resources.errors import ForbidenError, InvalidInputError, ServerError
from misakey_main.resources.api_clients.auth0_managment import Auth0ManagmentAPIClient
from misakey_main.resources.api_clients.auth0_auth import Auth0AuthAPIClient
from misakey_main.resources.api_clients.s3 import S3APIClient
from misakey_main.resources.tools import get_country_string_from_code
from misakey_main.resources.pdf_generators.mandate import generate_mandate_pdf

from misakey_main.resources.extract_and_validate.signup import (
    extract_and_validate_signup_data,
)

from misakey_main.resources.config import config


class UserController(Controller):
    def __init__(self):
        Controller.__init__(self)
        self.auth0_managment = Auth0ManagmentAPIClient()
        self.auth0_auth = Auth0AuthAPIClient()
        self.s3 = S3APIClient()

    def check_sub_match_with_authenticated_user(self, sub):
        if sub != self.get_sub():
            raise ForbidenError(
                {
                    "code": "invalid_sub",
                    "description": "The sub your asking is not corresponding yours",
                }
            )

    def signup(self):
        sub, first_name, family_name, email, country, signature = extract_and_validate_signup_data(
            request
        )

        user = self.get_user(exception_on_none=False, sub=sub)
        if user is not None:
            raise InvalidInputError(
                {
                    "code": "user_exists",
                    "description": "An user already exists with this sub",
                }
            )

        if not self.auth0_managment.user_exists(sub):
            raise InvalidInputError(
                {
                    "code": "user_not_exists",
                    "description": "No user registred with this sub",
                }
            )

        user = User(sub, email)
        user.date_accept_tos = datetime.utcnow()
        self.dbSession.add(user)

        mandate = Mandate(user.id, first_name, family_name, email, country)
        filename = secure_filename("{}.png".format(user.uuid))
        self.s3.addSignature(signature, filename)
        mandate.signature = filename

        user.mandates.append(mandate)

        # Todo: All that should be done with a job working async
        companies = (
            self.dbSession.query(Company)
            .filter(
                or_(
                    Company.status == CompanyStatus.not_onboarded,
                    Company.status == CompanyStatus.onboarded,
                )
            )
            .order_by(Company.sorting_weight.asc())
            .all()
        )
        for company in companies:
            ucl = UserCompanyLink(company_id=company.id)
            user.companies.append(ucl)

        self.dbSession.commit()

        misakey = (
            self.dbSession.query(Company)
            .filter(Company.name == "Misakey")
            .one_or_none()
        )
        if misakey:
            misakey_ucl = UserCompanyLink(company_id=misakey.id)
            welcome_message = Message(message_type=MessageType.welcomeMessage)
            misakey_ucl.messages.append(welcome_message)
            user.companies.append(misakey_ucl)

        self.dbSession.commit()

        if misakey:
            for message in misakey_ucl.messages:
                if message.message_type == MessageType.accessRequestAsked:
                    self.dbSession.delete(message)
                    self.dbSession.commit()

        return user.export()

    def get_user_as_dpo(self, uuid):
        company = self.get_company()

        for user_company_link in company.users:
            if str(user_company_link.user.uuid) == str(uuid):
                if user_company_link.status == LinkStatus.unread:
                    user_company_link.status = LinkStatus.read
                    self.dbSession.commit()
                return user_company_link.export(side="user", include_messages=True)

        raise InvalidInputError(
            {
                "code": "user_not_exists",
                "description": "No user registred with this uuid",
            }
        )

    def get_authed_user_or_user_linked_to_authed_dpo(self, uuid):
        lower_uuid = uuid.lower()
        authed, authed_type = self.get_authenticated_person()
        user = None
        if authed_type == "user":
            if str(user.uuid).lower() == lower_uuid:
                user = authed
        else:
            for user_company_link in authed.companies[0].company.users:
                if str(user_company_link.user.uuid).lower() == uuid:
                    user = user_company_link.user

        if user is None:
            raise ForbidenError(
                {
                    "code": "dont_have_access",
                    "description": "This company is not linked to user",
                }
            )
        return user

    def get_user_mandate(self, uuid):
        user = self.get_authed_user_or_user_linked_to_authed_dpo(uuid)

        mandate = user.mandates[0]

        fullname = "{} {}".format(mandate.first_name, mandate.family_name)
        email = mandate.email
        country = get_country_string_from_code(mandate.country)
        date = mandate.date_creation.strftime("%d/%m/%Y")
        signature_path = self.s3.getSignature(mandate.signature)

        pdf_path = generate_mandate_pdf(fullname, email, country, date, signature_path)

        return send_file(pdf_path)

        # Read file and prepare the return object
        # Delete signature_path object
        # delete pdf_path object
    
    def get_user_pubkey(self, uuid):
        user = self.get_authed_user_or_user_linked_to_authed_dpo(uuid)
        key = user.keys[0]
        return key.export()


    def get_companies_list(self, uuid):
        user = self.get_user()
        if str(user.uuid) != uuid:
            raise InvalidInputError(
                {
                    "code": "user_doesnt_match",
                    "description": "User doesnt match identified user",
                }
            )

        return user.export_companies_list()

    # TODO: Rate limitation
    def does_user_exist(self, email):
        user = (
            self.dbSession.query(User)
            .filter(func.lower(User.email) == func.lower(email))
            .one_or_none()
        )
        company = (
            self.dbSession.query(Company)
            .filter(func.lower(Company.dpo_email) == func.lower(email))
            .one_or_none()
        )
        if user is None and company is None:
            return {}, 404
        else:
            return {}, 200

    def get_user_profile_from_auth0(self, sub):
        self.check_sub_match_with_authenticated_user(sub)

        user_info = self.auth0_auth.get_user_info()

        user = self.get_user(exception_on_none=False)
        if user is None:
            dpo = self.get_dpo()
            return dpo.export()

        if (
            user_info["email_verified"]
            and user.onboarding_status == OnboardingStatus.verify_email_pending
        ):
            user.onboarding_status = OnboardingStatus.email_double_opted_in
        self.dbSession.commit()

        return user.export()

    def end_onboarding(self):
        user = self.get_user()

        user.onboarding_status = OnboardingStatus.completed
        user.date_ready = datetime.utcnow()

        self.dbSession.commit()

        return user.export()

    def delete_user(self):
        data = request.get_json()
        auth0_id = data.get("auth0_id", "")

        self.check_sub_match_with_authenticated_user(auth0_id)

        user = self.get_user()

        mandates = (
            self.dbSession.query(Mandate).filter(Mandate.user_id == user.id).all()
        )
        conn = boto3.client(
            "s3",
            region_name=config["AWS_S3_REGION"],
            aws_access_key_id=config["AWS_API_KEY"],
            aws_secret_access_key=config["AWS_API_SECRET"],
        )

        for mandate in mandates:
            if mandate.signature:
                try:
                    conn.delete_object(
                        Key=mandate.signature,
                        Bucket=config["AWS_S3_SIGNATURE_BUCKET"],
                    )
                except Exception as e:
                    print(e)
                    raise ServerError(
                        {"code": "s3_delete_error", "description": str(e)}, 500
                    )

        delete_mandates_statement = delete(Mandate).where(Mandate.user_id == user.id)
        self.dbSession.execute(delete_mandates_statement)

        delete_keys_statement = delete(Key).where(Key.user_id == user.id)
        self.dbSession.execute(delete_keys_statement)

        delete_ucl_statement = delete(UserCompanyLink).where(
            UserCompanyLink.user_id == user.id
        )
        self.dbSession.execute(delete_ucl_statement)

        delete_user_statement = delete(User).where(User.id == user.id)
        self.dbSession.execute(delete_user_statement)

        # Delete on Auth0
        res = self.auth0_managment.delete_user(user.auth0_id)
        status = res.status

        if status != 204:
            raise ServerError(
                {
                    "code": "auth0_delete_isssue",
                    "description": str(res.status, res.reason),
                },
                500,
            )

        self.dbSession.commit()

        return {}

    def add_public_key(self):
        data = request.get_json()

        key_name = data.get("keyName", "")
        pubkey = data.get("pubkey", "")

        if len(key_name) == 0 or len(pubkey) == 0:
            raise InvalidInputError(
                {
                    "code": "uncomplete_fieldlist",
                    "description": "Please send public key and name",
                }
            )

        user = self.get_user()

        key = (
            self.dbSession.query(Key)
            .filter(and_(Key.public_key == pubkey, Key.user_id == user.id))
            .one_or_none()
        )

        if key is None:
            key = Key(user.id, key_name, pubkey)
            self.dbSession.add(key)
        else:
            key.name = key_name

        user.onboarding_status = OnboardingStatus.key_generated

        self.dbSession.commit()
        return {}

    # Todo: Return a status code and not a body to know it.
    def check_public_key_ownership(self, key):
        user = self.get_user()

        key = (
            self.dbSession.query(Key)
            .filter(Key.user_id == user.id)
            .filter(Key.public_key == key)
            .one_or_none()
        )
        if key:
            return {"is_owner": True}
        return {"is_owner": False}
