import uuid

from misakey_main.resources.database import session as dbSession
from flask import _request_ctx_stack
from misakey_main.models import User, Dpo
from misakey_main.resources.errors import ServerError, InvalidInputError


class Controller:
    session = None

    def __init__(self):
        self.dbSession = dbSession()

    def get_authenticated_person(self):
        user = self.get_user(exception_on_none=False)
        if user is None:
            dpo = self.get_dpo()
            return dpo, "dpo"
        return user, "user"

    def get_user(self, exception_on_none=True, sub=None):
        if sub is None:
            sub = self.get_sub()
        try:
            user = self.dbSession.query(User).filter(User.auth0_id == sub).one_or_none()
        except:
            raise ServerError(
                {
                    "code": "database_error",
                    "description": "Error while connecting to the database",
                },
                500,
            )

        if user is None:
            if exception_on_none:
                raise ServerError(
                    {"code": "auth_error", "description": "No user found"}, 500
                )
        return user

    def get_dpo(self, exception_on_none=True, sub=None):
        if sub is None:
            sub = self.get_sub()
        try:
            dpo = self.dbSession.query(Dpo).filter(Dpo.auth0_id == sub).one_or_none()
        except Exception as e:
            raise e
            raise ServerError(
                {
                    "code": "database_error",
                    "description": "Error while connecting to the database",
                },
                500,
            )

        if dpo is None:
            if exception_on_none:
                raise ServerError(
                    {"code": "auth_error", "description": "No dpo found"}, 500
                )
        return dpo

    def get_company(self, exception_on_none=True):
        dpo = self.get_dpo(exception_on_none=exception_on_none)
        if len(dpo.companies) > 0:
            return dpo.companies[0].company
        raise ServerError(
            {"code": "auth_error", "description": "No company found"}, 500
        )

    def get_sub(self):
        return _request_ctx_stack.top.current_user["sub"]

    def validate_uuid(self, uuid_string):
        try:
            uuid.UUID(uuid_string)
        except Exception as e:
            raise InvalidInputError(
                {"code": "invalid_uuid", "description": "The given UUID is invalid"}
            )
