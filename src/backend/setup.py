from setuptools import setup, find_packages

name = "misakey_main"

requires = [
    "uuid",
    "blinker",
    "Flask",
    "Flask-API",
    "python-dotenv",
    "simplejson",
    "SQLAlchemy",
    "watchdog",
    "jose",
    "boto3",
]

extras_require = {"dev": ["alembic", "black"]}


setup(
    name=name,
    description="Misakey main package. Entry point for the whole application",
    classifiers=["Programming Language :: Python"],
    author="Misakey",
    author_email="love@misakey.com",
    license="AGPLv3",
    packages=find_packages(),
    include_package_data=True,
    install_requires=requires,
    extras_require=extras_require,
)
