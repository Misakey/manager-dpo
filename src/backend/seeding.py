from datetime import datetime

from misakey_main.resources.database import session as session_builder

from misakey_main.models import Company, User, UserCompanyLink, Mandate, Key, Dpo, DpoCompanyLink, Message
from misakey_main.models.companies import CompanyStatus
from misakey_main.models.user_company_links import LinkStatus
from misakey_main.models.messages import MessageType
from misakey_main.models.users import OnboardingStatus

from time import sleep

print("Will create DB seed")

session = session_builder()

companies_list = [
    {
        "name": "Zalando",
        "logo": "https://static.misakey.com/dashboard/companies-logo/2901709ee6d24faf74b440b938f21344b03ff6406acf7dce5845c6350d80801d.png",
        "email": "misakey_dpo_zalando@yopmail.fr",
        "domain": "zalando.fr",
        "homepage": "https://www.zalando.fr/",
        "sorting_weight": 400,
        "dpos": [
            {
                "auth0": "auth0|5c811d856d3d732e6aa9d842",
                "email": "misakey_dpo_zalando@yopmail.fr",
            }
        ]
    },
    {
        "name": "Mediapart",
        "logo": "https://static.misakey.com/dashboard/companies-logo/665ab0b0a0cc81cad333e01c0e379f342be4bf8a85850b799806affa97066a7b.png",
        "email": "misakey_dpo_mediapart@yopmail.fr",
        "domain": "mediapart.fr",
        "homepage": "https://www.mediapart.fr/",
        "sorting_weight": 200,
    },
    {
        "name": "Dassault Aviation",
        "logo": "https://static.misakey.com/dashboard/companies-logo/fe4b30e3b89efb195d7a3324e51b5ecb6639a96a1a7c684bdb0368bf23f63739.png",
        "email": "misakey_dpo_dassault@yopmail.fr",
        "domain": "dassault-aviation.com",
        "homepage": "https://www.dassault-aviation.com/fr",
        "sorting_weight": 100,
    },
]

users_list = [
    {
        "email": "misakey_user_john_doe@yopmail.fr",
        "auth0": "auth0|5c811b38408fb82ef48ccbd9",
        "first_name": "John",
        "family_name": "Doe",
        "pubkey": "VVzgVF7mWSFJ5cDVszOmgOV48yZjA8HMY24NnbEQDi0=",
        "signature": "7500d14c-948b-4b0d-a3cb-c412333a5212.png",
    },
    {
        "email": "misakey_user_christian_michu@yopmail.fr",
        "auth0": "auth0|5c811c286d3d732e6aa9d7fb",
        "first_name": "Christian",
        "family_name": "Michu",
        "pubkey": "VVzgVF7mWSFJ5cDVszOmgOV48yZjA8HMY24NnbEQDi0=",
        "signature": "7500d14c-948b-4b0d-a3cb-c412333a5212.png",
    },
    {
        "email": "misakey_user_josephine_agd@yopmail.fr",
        "auth0": "auth0|5c811cf30c26462c04c50737",
        "first_name": "Josephine",
        "family_name": "Agd",
        "pubkey": "VVzgVF7mWSFJ5cDVszOmgOV48yZjA8HMY24NnbEQDi0=",
        "signature": "7500d14c-948b-4b0d-a3cb-c412333a5212.png",
    },
]

users = session.query(User).filter(User.email == users_list[0]["email"]).all()
if len(users) == 0:
    print("Creating DB seed")

    db_users_list = []
    for user in users_list:
        db_user = User(user["auth0"], user["email"])
        db_user.onboarding_status = OnboardingStatus.completed
        mandate = Mandate(first_name=user["first_name"], family_name=user["family_name"], country="FR")
        mandate.signature = user["signature"]
        mandate.email = user["email"]
        db_user.mandates.append(mandate)
        key = Key(name="Cle de {}".format(user["email"]), public_key=user["pubkey"])
        db_user.keys.append(key)
        session.add(db_user)
        db_users_list.append(db_user)

    for company in companies_list:
        db_company = Company(
            name=company["name"], 
            logo=company["logo"], 
            homepage=company["homepage"], 
            domain=company["domain"], 
            sorting_weight=company["sorting_weight"],
            dpo_email=company["email"],
        )
        db_company.status = CompanyStatus.not_onboarded
        session.add(db_company)

        if "dpos" in company and company["dpos"]:
            db_company.status = CompanyStatus.onboarded
            for dpo in company["dpos"]:
                db_dpo = Dpo(dpo["auth0"], dpo["email"])
                db_dpo.date_accept_tos = datetime.utcnow()
                db_dpo.onboarding = datetime.utcnow()

                dcl = DpoCompanyLink()
                dcl.company = db_company
                dcl.dpo = db_dpo
                session.add(dcl)

        for db_user in db_users_list:
            ucl = UserCompanyLink()
            ucl.user = db_user
            ucl.company = db_company
            ucl.status = LinkStatus.unread
            session.add(ucl)
        
    session.commit()

    messages = session.query(Message).all()
    for message in messages:
        message.message_type = MessageType.accessRequestSent
    session.commit()

    print("DB populated")

else:
    print("DB not empty, dont seed it")