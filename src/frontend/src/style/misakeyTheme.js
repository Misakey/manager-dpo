import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#e32e72',
    },
    secondary: {
      main: '#FFB400',
    },
    vertdeau: {
      main: '#C6D8D3',
    },
  },
  typography: {
    // Use the system font instead of the default Roboto font.
    useNextVariants: true,
    fontFamily: [
      '-apple-system',
      'Roboto',
      '"Helvetica Neue"',
    ].join(','),
  },

});

export default theme;
