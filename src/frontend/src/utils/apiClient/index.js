import { getHeader, getInit } from './tools';

class APIClient {
  constructor(sync=false) {
    this.endpoint = process.env.REACT_APP_BACKEND_API_ENDPOINT;
    this.sync = sync;
  }

  setSync(sync) {
    this.sync = sync;
  }

  async fetch(path, init, returnType = 'json') {
    const request = new Request(this.endpoint + path, init);

    if (this.sync) {
      const response = await fetch(request);
      let json;
      try {
        json = await response.json();
      } catch {
        json = { };
      }
      return {
        httpStatus: response.status,
        body: json,
      };
    }

    return fetch(request)
      .then((rawResponse) => {
        // TODO: reloader of the token. Maybenot useful because we refresh it automatically with a timeout.
        // if (rawResponse.status === 401 && init.headers.has('Authorization')) {
        //   // TODO: Should renew the token
        //
        //   return auth.renewSession(() => {
        //     this.fetch(path, init, returnType);
        //   });
        // }

        if (rawResponse.status === 204) {
          return { httpStatus: rawResponse.status };
        }
        if (rawResponse.status >= 200 && rawResponse.status < 300) {
          // return rawResponse.json();
          // Todo: try that:

          if (returnType === 'json') {
            return new Promise((resolve, reject) => {
              rawResponse.json().then((response) => {
                resolve({
                  httpStatus: rawResponse.status,
                  body: response,
                });
              });
            });
          }
          if (returnType === 'blob') {
            return new Promise((resolve, reject) => {
              rawResponse.blob().then((response) => {
                resolve({
                  httpStatus: rawResponse.status,
                  body: response,
                });
              });
            });
          }
        }
        return new Promise((resolve, reject) => {
          rawResponse.json().then((response) => {
            const error = new Error(`[ERROR ${rawResponse.status}] - ${response.code}: ${response.description}`);
            error.httpStatus = rawResponse.status;
            error.code = response.code;
            error.description = response.description;
            error.httpText = rawResponse.statusText;
            reject(error);
          });
        });
      });
  }

  async get(path, shouldAuth = true) {
    const headers = getHeader(shouldAuth);
    const init = getInit('GET', headers);

    return this.fetch(path, init);
  }

  async head(path, shouldAuth = true) {
    const headers = getHeader(shouldAuth);
    const init = getInit('HEAD', headers);

    return this.fetch(path, init);
  }

  async download(path, shouldAuth = true) {
    const headers = getHeader(shouldAuth);
    const init = getInit('GET', headers);

    return this.fetch(path, init, 'blob');
  }

  async post(path, payload, shouldAuth = true) {
    const headers = getHeader(shouldAuth, 'application/json');
    const init = getInit('POST', headers, JSON.stringify(payload));

    return this.fetch(path, init);
  }

  async patch(path, payload, shouldAuth = true) {
    const headers = getHeader(shouldAuth, 'application/json');
    const init = getInit('PATCH', headers, JSON.stringify(payload));

    return this.fetch(path, init);
  }

  async delete(path, payload, shouldAuth = true) {
    const headers = getHeader(shouldAuth, 'application/json');
    const init = getInit('DELETE', headers, JSON.stringify(payload));

    return this.fetch(path, init);
  }

  async upload(path, payload, shouldAuth = true) {
    const headers = getHeader(shouldAuth);
    const init = getInit('POST', headers, payload);

    return this.fetch(path, init);
  }
}

export default new APIClient();
