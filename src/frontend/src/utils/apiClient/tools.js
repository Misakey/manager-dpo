export const getHeader = (shouldAuth, contentType = null) => {
  const myHeaders = new Headers();

  if (shouldAuth) {
    const accessToken = localStorage.getItem(`${process.env.REACT_APP_FRONTEND_TYPE}_access_token`) || null;
    if (accessToken) {
      myHeaders.set('Authorization', `Bearer ${accessToken}`);
    }
  }
  if (contentType) {
    myHeaders.set('Content-Type', contentType);
  }
  return myHeaders;
};

export const getInit = (method, headers, body = null) => {
  const init = {
    method,
    headers,
    mode: 'cors',
    credentials: 'same-origin',
    cache: 'default',
  };

  if (body) {
    init.body = body;
  }

  return init;
};
