

export function caesarShift(text, absoluteShift, decrypt = false) {
  let shift = absoluteShift;
  if (decrypt) {
    shift = 26 - absoluteShift;
  }
  let result = '';
  for (let i = 0; i < text.length; i++) {
    const c = text.charCodeAt(i);
    if      (65 <= c && c <=  90) result += String.fromCharCode((c - 65 + shift) % 26 + 65); // Uppercase
    else if (97 <= c && c <= 122) result += String.fromCharCode((c - 97 + shift) % 26 + 97); // Lowercase
    else result += text.charAt(i); // Copy
  }
  return result;
}
