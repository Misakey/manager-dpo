import nacl from 'tweetnacl';
import util from 'tweetnacl-util';
import scrypt from 'scrypt-js';

import { caesarShift } from './misc';

nacl.util = util;


class keys {
  keyPair = null;

  generateKeyPair() {
    this.keyPair = nacl.box.keyPair();
  }

  // Encryption of the Secret Key
  // We scrypt it with the auth0_id as password (TODO: generate UUID on backend, and use it (to be agnostic of auth0))
  // After we use a ceasar cipher just to make some noise on the base64encoded string.
  encryptSecretKey(password, callback) {
    const salt = nacl.util.encodeBase64(nacl.randomBytes(32)).normalize('NFKC');
    const saltBuffered = Buffer.from(salt);
    const passwordBuffer = Buffer.from(password);
    const nonce = new Uint8Array(24);
    const N = 2048;
    const blockSize = 8;
    const p = 1;
    const dkLen = 32;
    scrypt(passwordBuffer, saltBuffered, N, blockSize, p, dkLen, (error, progress, derivedKey) => {
      if (derivedKey) {
        const derivedKeyUintArray = nacl.util.decodeBase64(nacl.util.encodeBase64(derivedKey));
        const encryptedSecretKey = nacl.util.encodeBase64(
          nacl.secretbox(this.keyPair.secretKey, nonce, derivedKeyUintArray),
        );
        const jsonstring = JSON.stringify({
          salt, N, p, dkLen, blockSize, encryptedSecretKey,
        });

        const b64encoded = caesarShift(Buffer.from(jsonstring).toString('base64'), 24);
        callback(b64encoded, nacl.util.encodeBase64(this.keyPair.publicKey));
      }
    });
  }

  decryptSecretKey(password, b64encodedKeyBundle, callback) {
    const encryptedSecretKeyBundle = JSON.parse(Buffer.from(caesarShift(b64encodedKeyBundle, 24, true), 'base64').toString('ascii'));

    const nonce = new Uint8Array(24);
    const saltBuffered = Buffer.from(encryptedSecretKeyBundle.salt);
    const passwordBuffer = Buffer.from(password);
    scrypt(
      passwordBuffer,
      saltBuffered,
      encryptedSecretKeyBundle.N,
      encryptedSecretKeyBundle.blockSize,
      encryptedSecretKeyBundle.p,
      encryptedSecretKeyBundle.dkLen,
      (error, progress, derivedKey) => {
        if (derivedKey) {
          const derivedKeyUintArray = nacl.util.decodeBase64(nacl.util.encodeBase64(derivedKey));
          const secretKey = nacl.secretbox.open(nacl.util.decodeBase64(encryptedSecretKeyBundle.encryptedSecretKey), nonce, derivedKeyUintArray);
          if (secretKey) {
            this.keyPair = nacl.box.keyPair.fromSecretKey(secretKey);
            callback(false);
          } else {
            callback('Error, unable to open the box');
          }
        } else if (error) {
          callback(error);
        }
      },
    );
  }
}

export default keys;
