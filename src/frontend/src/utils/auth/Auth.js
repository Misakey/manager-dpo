import auth0 from 'auth0-js';
import history from '../../history';
import { login, logoutAction } from '../../store/users/actions';

import store from '../../store/store';

class Auth {
  tokenRenewalTimeout;

  accessToken;

  idToken;

  sub;

  expiresAt;

  auth0 = new auth0.WebAuth({
    domain: process.env.REACT_APP_AUTH0_DOMAIN,
    clientID: process.env.REACT_APP_AUTH0_CLIENTID,
    redirectUri: process.env.REACT_APP_AUTH0_CALLBACKURL,
    responseType: 'token id_token',
    scope: 'openid email',
    audience: process.env.REACT_APP_AUTH0_AUDIENCE,
  });

  constructor() {
    this.signup = this.signup.bind(this);
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.handleAuthentication = this.handleAuthentication.bind(this);
    this.isAuthenticated = this.isAuthenticated.bind(this);
    this.getAccessToken = this.getAccessToken.bind(this);
    this.getIdToken = this.getIdToken.bind(this);
    this.renewSession = this.renewSession.bind(this);
  }

  signup() {
    this.auth0.authorize({});
  }

  login() {
    this.auth0.authorize({});
  }

  handleAuthentication() {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
      } else if (err) {
        // history.replace('/');
        console.log(err);
        // alert(`Error: ${err.error}. Check the console for further details.`);
      }
    });
  }

  getAccessToken() {
    return this.accessToken;
  }

  getIdToken() {
    return this.idToken;
  }

  getSub() {
    return this.sub;
  }

  setSession(authResult, callback = null) {
    // Set isLoggedIn flag in localStorage
    localStorage.setItem(`${process.env.REACT_APP_FRONTEND_TYPE}_isLoggedIn`, 'true');


    // Set the time that the access token will expire at
    const expiresAt = (authResult.expiresIn * 1000) + new Date().getTime();
    this.accessToken = authResult.accessToken;
    this.idToken = authResult.idToken;
    this.sub = authResult.idTokenPayload.sub;
    this.expiresAt = expiresAt;
    this.lastRefresh = new Date().getTime();

    localStorage.setItem(`${process.env.REACT_APP_FRONTEND_TYPE}_access_token`, this.accessToken);

    store.dispatch(login(authResult.idTokenPayload.sub));

    console.log(authResult.idTokenPayload.sub);
    console.log(authResult.accessToken);

    // navigate to the home route
    if (history.location.pathname === '/callback') {
      history.replace('/');
    }
    if (callback) {
      callback();
    }
  }

  renewSession(callback) {
    if (!(this.lastRefresh && new Date().getTime() - this.lastRefresh > 2)) {
      this.auth0.checkSession({}, (err, authResult) => {
        if (authResult && authResult.accessToken && authResult.idToken) {
          this.setSession(authResult, callback);
        } else if (err) {
          this.logout();
          console.error(err);
        }
      });
    }
  }

  logout(uuid) {
    // Remove tokens and expiry time
    this.accessToken = null;
    this.idToken = null;
    this.expiresAt = 0;

    // Remove isLoggedIn flag from localStorage
    localStorage.removeItem(`${process.env.REACT_APP_FRONTEND_TYPE}_isLoggedIn`);
    localStorage.removeItem(`encrypted_box_privkey_${uuid}`);
    console.log("LOGGEDOUT");

    store.dispatch(logoutAction());

    // navigate to the home route
    // history.replace('/');
    this.auth0.logout({
      clientID: process.env.REACT_APP_AUTH0_CLIENTID,
      returnTo: process.env.REACT_APP_URL,
    });
  }

  isAuthenticated() {
    // Check whether the current time is past the
    // access token's expiry time
    const expiresAt = this.expiresAt;
    return new Date().getTime() < expiresAt;
  }

  // To finish: https://auth0.com/docs/quickstart/spa/react/05-token-renewal
  scheduleRenewal() {
    const timeout = this.expiresAt - Date.now();
    if (timeout > 0) {
      this.tokenRenewalTimeout = setTimeout(() => {
        this.renewSession();
      }, timeout);
    }
  }

  getExpiryDate() {
    return JSON.stringify(new Date(this.expiresAt));
  }
}

export default Auth;
