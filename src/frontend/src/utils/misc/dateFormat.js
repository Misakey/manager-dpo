import moment from 'moment/min/moment-with-locales';

import i18next from 'i18next';

export const timestampToDigestDate = (timestamp) => {
  moment.locale(i18next.language);
  const date = moment.unix(timestamp);

  // Check if the date is today
  if (date.isSame(moment(), 'd')) {
    return date.format('LT');
  }
  return date.format('L');
};

export const timestampToChatDate = (timestamp) => {
  moment.locale(i18next.language);
  const date = moment.unix(timestamp);
  return date.format('LL');
};

export const timestampToHour = (timestamp) => {
  moment.locale(i18next.language);
  const date = moment.unix(timestamp);

  return date.format('LT');
};

export const nowAsDay = () => {
  moment.locale(i18next.language);
  return moment().format('LL');
};

export const getDaysToAnswerAndProgressValue = (messageDate) => {
  const totalDaysToAnswer = 30;

  const dateNow = moment();
  const dateMessage = moment.unix(messageDate);

  const daysEllapsed = Math.min(30, dateNow.diff(dateMessage, 'days'));

  const progressValue = 100 * daysEllapsed / totalDaysToAnswer;
  const daysToAnswer = totalDaysToAnswer - daysEllapsed;

  return {
    progressValue,
    daysToAnswer,
  };
};
