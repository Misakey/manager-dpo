import React from 'react';
import * as Sentry from '@sentry/browser';

import { Button, Typography } from '@material-ui/core';

// Sentry.init({
//  dsn: "https://0090ecd3b73249f1b03c9ba7a1e3e2c5@sentry.io/1407339"
// });
// should have been called before using it here
// ideally before even rendering your react app 

class AppErrorBoundary extends React.Component {
    constructor(props) {
        super(props);
        this.state = { error: null };
    }

    componentDidCatch(error, errorInfo) {
      this.setState({ error });
      Sentry.withScope(scope => {
        Object.keys(errorInfo).forEach(key => {
          scope.setExtra(key, errorInfo[key]);
        });
        Sentry.captureException(error);
      });
    }

    render() {
        if (this.state.error) {
            //render fallback UI
            return (
              <div class="fullscreenErrorFallbackuiContainer">
                <div class="fullscreenErrorFallbackuiContent">
                  <Typography>
                    L'application a plantée. Nous en avons été informé.<br/>
                    Si vous voulez nous aider vous pouvez nous donner des informations sur ce qui vous a amené à cette erreure en cliquant sur le bouton si dessous.
                  </Typography>

                  <Button onClick={() => Sentry.showReportDialog()} color="primary" variant="contained">Nous dire ce qui s'est passé</Button>
                </div>
              </div>
            );
        } else {
            //when there's not an error, render children untouched
            return this.props.children;
        }
    }
}

export default AppErrorBoundary;
