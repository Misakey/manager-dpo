import React from 'react';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
      error: null,
    };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return {
      hasError: true,
      error,
    };
  }

  componentDidCatch(error, info) {
    // You can also log the error to an error reporting service
    console.log('ERROR Catched. Maybe should send to an external service. (for now only here)');
    console.error(error);
    console.info(info);
  }

  render() {
    const { children, fullScreen } = this.props;
    const { hasError } = this.state;
    if (hasError) {
      if (fullScreen) {
        return (
          <div className="fullscreenErrorFallbackui">
            <h1>Une erreur est survenue. Désolé.</h1>
          </div>
        );
      }
      return <h3>Une erreur est survenue. Désolé.</h3>;
    }
    return children;
  }
}

export default ErrorBoundary;
