import React from 'react';

import { Provider } from 'react-redux';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { I18nextProvider } from 'react-i18next';

import store from './store/store';
import i18n from './i18n'; // initialized i18next instance

import Routes from './Routes';

import AppErrorBoundary from './utils/errorBoundaries/AppErrorBoundary';

import defaultTheme from './style/misakeyTheme';
import './style/App.css';

const App = () => (
  <MuiThemeProvider theme={defaultTheme}>    
    <I18nextProvider i18n={i18n}>
      <AppErrorBoundary>
        <Provider store={store}>
          <Routes />
        </Provider>
      </AppErrorBoundary>
    </I18nextProvider>
  </MuiThemeProvider>
);

export default App;
