import React, { Component } from 'react';
import { connect } from 'react-redux';
import { translate, Trans } from 'react-i18next';
import PropTypes from 'prop-types';

import {
  FormControl, TextField,
  CircularProgress, Typography,
  IconButton, Button,
} from '@material-ui/core';

import {
  Refresh as RefreshIcon,
} from '@material-ui/icons';

import Emoji from '../../components/Emoji';

import { download } from '../../utils/misc/fileDownload';

import Keys from '../../utils/crypto/keys';

class CryptoKeyGeneration extends Component {
  static propTypes = {
    user: PropTypes.object,
    next: PropTypes.func.isRequired,
  };

  static defaultProps = {
    user: null,
  }

  keyGenerationTime = 2000; // milliseconds

  constructor(props) {
    super(props);

    this.state = {
      step: 'key_generation', // key_generation, save_key, saving_validation
      keyName: `Clef Misakey - ${props.user.email}`,
      pubkey: 'pubkeyNotGenerated',
      keyPairToExport: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.nextStep = this.nextStep.bind(this);
    this.downloadKey = this.downloadKey.bind(this);
    this.regenerateKeyPair = this.regenerateKeyPair.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      this.generateKeyPair();
    }, 1000);
  }

  handleChange = name => (event) => {
    this.setState({
      [name]: event.target.value,
    });
  };

  regenerateKeyPair() {
    this.setState({
      step: 'key_generation',
    }, () => {
      this.generateKeyPair();
    });
  }

  generateKeyPair() {
    const { user, t } = this.props;
    const keys = new Keys();
    const t0 = performance.now();
    const keyName = t('keyGeneration.defaultKeyName', 'Clé Misakey - {{email}}', { email: user.email });

    keys.generateKeyPair();
    keys.encryptSecretKey(user.uuid, (encryptedSecretKey, pubkey) => {
      const t1 = performance.now();
      setTimeout(() => {
        this.setState({
          step: 'key_generated',
          pubkey,
          keyName,
          keyPairToExport: JSON.stringify({
            keyName,
            pubkey,
            encryptedSecretKey,
          }),
        });
      }, this.keyGenerationTime - ((t1 - t0) % this.keyGenerationTime));
    });
  }


  downloadKey() {
    const { keyName, keyPairToExport } = this.state;
    download(keyPairToExport, `${keyName}.misakey.txt`, 'application/txt');
  }

  nextStep() {
    this.setState((prevState) => {
      const { next, user } = this.props;
      switch (prevState.step) {
        case 'key_generated':
          // Download file
          this.downloadKey();
          return {
            step: 'key_save_confirm',
          };

        case 'key_save_confirm':
          localStorage.setItem(`encrypted_box_privkey_${user.uuid}`, prevState.keyPairToExport);
          next({
            pubkey: prevState.pubkey,
            keyName: prevState.keyName,
          });
          break;

        default:
          break;
      }
      return {};
    });
  }

  render() {
    const { step, keyName, keyPairToExport } = this.state;
    const { t } = this.props;

    if (step === 'key_generation') {
      return (
        <div className="containerCenter">
          <CircularProgress
            variant="indeterminate"
            size={40}
            thickness={4}
          />
          <Typography variant="h5">
            {t('keyGeneration.generationWaiting', 'Génération de votre clé privé Misakey')}
          </Typography>
        </div>
      );
    }
    if (step === 'key_generated') {
      return (
        <div className="containerCenter">
          <FormControl margin="normal" fullWidth>
            <TextField
              label={t('keyGeneration.keyNameLabel', 'Nom de la clé')}
              value={keyName}
              onChange={this.handleChange('keyName')}
              margin="normal"
              variant="outlined"
            />
          </FormControl>

          <FormControl margin="normal" fullWidth>
            <TextField
              label={t('keyGeneration.keyLabel', 'Clé privée')}
              type="password"
              autoComplete="none"
              margin="normal"
              variant="outlined"
              value={keyPairToExport}
            />
          </FormControl>
          <div className="buttonsBar">
            <div className="grow" />
            <IconButton aria-label="Regénérer une clef" onClick={this.regenerateKeyPair}>
              <RefreshIcon />
            </IconButton>
          </div>

          <Typography variant="h6" align="center">
            <Trans i18nKey="keyGeneration.successMessage">
              Votre clef est générée !<br />
              <br />
              Nous n'avons pas accès à cette clé privée, sauvegardez la précieusement pour rouvrir votre compte plus tard
            </Trans>
          </Typography>
          <Button variant="contained" color="primary" onClick={this.nextStep} className="keyGenerationButton">
            {t('keyGeneration.saveButton', 'Sauvegarder')}
          </Button>
        </div>
      );
    }
    if (step === 'key_save_confirm') {
      return (
        <div className="containerCenter">
          <Emoji emoji="1f510" name="Lock" size={64} />
          <Typography align="center" variant="h6">
            {t('keyGeneration.confirmSaved', 'Votre clé privée n’est pas stockée chez Misakey afin que les données personnelles transmises soient confidentielle. Cette clé sera nécessaire pour rouvrir votre compte si vous vous déconnectez. En cas de perte, nous ne serons pas en mesure de la récupérer.')}
          </Typography>
          <FormControl margin="normal" className="buttonsBarLeft">
            <Button variant="contained" onClick={this.downloadKey}>
              {t('keyGeneration.saveButton', 'Sauvegarder')}
            </Button>
            <Button variant="contained" color="primary" onClick={this.nextStep}>
              {t('keyGeneration.savedButton', 'J\'ai sauvegardé ma clé')}
            </Button>
          </FormControl>
        </div>
      );
    }

    return null;
  }
}

function mapStateToProps(state) {
  return {
    user: state.users.user,
  };
}

export default connect(mapStateToProps)(translate('onboarding')(CryptoKeyGeneration));