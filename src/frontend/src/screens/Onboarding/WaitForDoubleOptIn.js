import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { translate, Trans } from 'react-i18next';

import Typography from '@material-ui/core/Typography';

import Emoji from '../../components/Emoji';

import {
  login as refreshUserAction,
} from '../../store/users/actions';

class WaitForDoubleOptIn extends React.Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    refreshUser: PropTypes.func.isRequired,
  };

  componentDidMount() {
    const { user, refreshUser } = this.props;
    setInterval(() => {
      refreshUser(user.auth0_id);
    }, 15000);
  }

  render() {
    return (
      <div className="containerCenter">
        <Emoji emoji="1f4e5" name="Inbox" size={64} className="" />
        <Typography align="center" variant="h6">
          <Trans i18nKey="waitfordoubleoptin.message">
            Un email vous a été envoyé pour confirmer votre courriel
            <br />
            Cliquer sur le lien pour valider votre email.
          </Trans>
        </Typography>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.users.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    refreshUser: sub => dispatch(refreshUserAction(sub)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(translate('onboarding')(WaitForDoubleOptIn));
