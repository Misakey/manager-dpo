import React from 'react';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import PropTypes from 'prop-types';

import {
  Stepper, Step, StepLabel,
  AppBar, Toolbar,
  IconButton,
} from '@material-ui/core';

import { Menu as MenuIcon } from '@material-ui/icons';

import BaseComponent from '../../components/_BaseComponent';
import SideMenu from '../../components/SideMenu';
import CryptoKeyGeneration from './CryptoKeyGeneration';
import EndOnboarding from './EndOnboarding';
import WaitForDoubleOptIn from './WaitForDoubleOptIn';
import Loading from '../Loading';

import { ROUTES } from '../../Routes';

import { uploadPublicKeyAction, endOnboardingAction } from '../../store/users/actions';

import '../../style/Onboarding.css';

class OnboardingScreen extends BaseComponent {
  static propTypes = {
    user: PropTypes.object,
  };

  static defaultProps = {
    user: null,
  }

  state = {
    isDrawerOpen: false,
  };

  constructor(props) {
    super(props);
    this.nextStep = this.nextStep.bind(this);
    this.toggleDrawer = this.toggleDrawer.bind(this);
  }

  componentDidUpdate() {
    const { user } = this.props;

    if (user) {
      if (user.onboarding_status === 'completed') {
        this.goTo(ROUTES.DASHBOARD_ROUTE);
      }
    }
  }

  toggleDrawer = open => () => {
    this.setState({
      isDrawerOpen: open,
    });
  };

  nextStep(params) {
    const { user } = this.props;

    let onboardingStatus = user.onboarding_status;
    if (onboardingStatus === 'tos_accepted') {
      onboardingStatus = 'email_double_opted_in';
    }

    switch (onboardingStatus) {
      case 'email_double_opted_in':
        this.props.uploadPublicKey({
          pubkey: params.pubkey,
          keyName: params.keyName,
        });
        break;

      case 'key_generated':
        this.props.endOnboarding();
        break;

      default:
        break;
    }
  }

  render() {
    const { user, auth, t } = this.props;
    const steps = [
      t('stepper.email', 'Confirmation courriel'),
      t('stepper.key', 'Création de la clé privée'),
      t('stepper.confirm', 'Récupération des données'),
    ];

    if (user === null) {
      return <Loading />;
    }

    let StepSubScreen = Loading;
    let activeStep = 0;
    let onboardingStatus = user.onboarding_status;
    if (onboardingStatus === 'tos_accepted' || onboardingStatus === 'mandate_signed') {
      onboardingStatus = 'email_double_opted_in';
    }
    switch (onboardingStatus) {
      case 'verify_email_pending':
        StepSubScreen = WaitForDoubleOptIn;
        activeStep = 0;
        break;

      case 'email_double_opted_in':
        StepSubScreen = CryptoKeyGeneration;
        activeStep = 1;
        break;

      case 'key_generated':
        StepSubScreen = EndOnboarding;
        activeStep = 2;
        break;

      default:
        break;
    }
    if (user) {
      return (
        <div>
          <SideMenu
            isDrawerOpen={this.state.isDrawerOpen}
            onClose={this.toggleDrawer(false)}
            auth={auth}
          />

          <AppBar position="static" color="inherit" className="headerBar">
            <Toolbar>
              <IconButton color="inherit" aria-label="Menu" onClick={this.toggleDrawer(true)}>
                <MenuIcon />
              </IconButton>
              <div className="onboardingLogoWrapper">
                <img src="/img/logo-typo.png" alt="Misakey" className="onboardingLogo" />
              </div>

              <div className="emptyIconPlaceholder" />
            </Toolbar>
          </AppBar>

          <Stepper activeStep={activeStep} alternativeLabel>
            {steps.map(label => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>

          <br />
          <StepSubScreen next={this.nextStep} />
        </div>
      );
    }
    return <Loading />;
  }
}

function mapStateToProps(state) {
  return {
    user: state.users.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    uploadPublicKey: publicKey => dispatch(uploadPublicKeyAction(publicKey)),
    endOnboarding: () => dispatch(endOnboardingAction()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(translate('onboarding')(OnboardingScreen));
