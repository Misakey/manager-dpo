import React from 'react';
import { translate } from 'react-i18next';

import { Typography, Button } from '@material-ui/core';

import Emoji from '../../components/Emoji';

const EndOnboarding = props => (
  <div className="containerCenter">
    <Emoji emoji="2705" name="Check" size={64} />
    <Typography align="center" variant="h6">
      {props.t('end.message', 'Tout est en place pour que nous puissions commencer à récupérer vos données personnelles de manière sécurisée')}
    </Typography>
    <Button onClick={props.next} color="primary" variant="contained">
      {props.t('end.startButton', 'Commencer')}
    </Button>
  </div>
);

export default translate('onboarding')(EndOnboarding);
