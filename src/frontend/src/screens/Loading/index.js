import React from 'react';
import { translate } from 'react-i18next';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Emoji from '../../components/Emoji';

import '../../style/Loading.css';


class Loading extends React.Component {
  state = {
    isTooLong: false,
  }

  componentDidMount() {
    this.timeout = setTimeout(() => {
      this.setState({
        isTooLong: true,
      });
    }, 15000);
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  render() {
    const { t } = this.props;
    const { isTooLong } = this.state;

    let tooLongText = null;
    if (isTooLong) {
      tooLongText = (
        <Typography align="center">
          Le temps de chargement est anormalement long. Vous pouvez essayer de rafraichir la page. <br/>
          Si le problème persiste, contactez-nous ! 
        </Typography>
      );
    }

    return (
      <div className="loadingPage">
        <CircularProgress
          variant="indeterminate"
          size={40}
          thickness={4}
        />
        <Typography variant="h5">
          { t('loading.message', 'La page est en cours de chargement. Merci de patienter !')}
        </Typography>
        <Emoji emoji="2665" name="Love" size={64} />
        {tooLongText}
      </div>
    );
  }
}

export default translate('common')(Loading);
