import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import PropTypes from 'prop-types';


import { withTheme } from '@material-ui/core/styles';
import {
  Typography, CircularProgress, Button,
  AppBar, Toolbar, IconButton, FormHelperText,
  FormControl, TextField,
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import FolderIcon from '@material-ui/icons/Folder';

import BaseComponent from '../../components/_BaseComponent';
import SideMenu from '../../components/SideMenu';
import Emoji from '../../components/Emoji';
import Loading from '../Loading';

import { loadPrivKeyAction } from '../../store/users/actions';

import '../../style/UnlockSeal.css';

import APIClient from '../../utils/apiClient';

import { ROUTES } from '../../Routes';


class UnlockSealScreen extends BaseComponent {
  static propTypes = {
    user: PropTypes.object,
  };

  static defaultProps = {
    user: null,
  }

  state = {
    keyStatus: 'waiting', // or 'loading'
    errorMessage: '',
    isDrawerOpen: false,
    exportedKeyInput: '',
  }

  constructor(props) {
    super(props);

    this.fileSelect = React.createRef();

    this.handleChange = this.handleChange.bind(this);
    this.toggleDrawer = this.toggleDrawer.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.loadKey = this.loadKey.bind(this);
    this.handleFileSelect = this.handleFileSelect.bind(this);
  }

  componentDidUpdate() {
    const { cryptoKey, user } = this.props;
    if (cryptoKey) {
      if (cryptoKey.isValid) {
        this.props.history.replace(ROUTES.DASHBOARD_ROUTE);
      } else {
        this.setState((prevState) => {
          if (prevState.keyStatus !== 'waiting') {
            return {
              keyStatus: 'waiting',
              errorMessage: cryptoKey.error,
            };
          }
        });
      }
    }

    if (user) {
      if (user.onboarding_status !== 'completed') {
        this.props.history.push(ROUTES.ONBOARDING_ROUTE);
      }

      const exportedKeyPair = localStorage.getItem(`encrypted_box_privkey_${user.uuid}`)
      if (exportedKeyPair) {
        try {
          const key = JSON.parse(exportedKeyPair);
          if (key && key.pubkey) {
            // Check (API) if the user owns this key
            APIClient.get(`/users/is-owner/publicKey/${key.pubkey}`, true).then((response) => {
              if (response.body.is_owner) {
                this.props.history.replace(ROUTES.DASHBOARD_ROUTE);
              } else {
                localStorage.removeItem(`encrypted_box_privkey_${user.uuid}`);
              }
            });
          }
        } catch (e) {
          console.log('Invalid key format');
          localStorage.removeItem(`encrypted_box_privkey_${user.uuid}`);
        }
      }
    }
  }

  handleChange = name => (event) => {
    this.setState({
      [name]: event.target.value,
    });
  };

  toggleDrawer = open => () => {
    this.setState({
      isDrawerOpen: open,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const { exportedKeyInput } = this.state;
    const { t } = this.props;

    if (exportedKeyInput.length > 0) {
      this.setState({
        keyStatus: 'loading',
        errorMessage: '',
      }, () => {
        setTimeout(() => {
          this.loadKey(exportedKeyInput);
        }, 1000);
      });
    } else {
      this.setState({
        errorMessage: t('errors.nokey', 'Veuillez entrer ou selectionner votre clé'),
      });
    }
  }

  handleFileSelect() {
    if (this.fileSelect.current.files.length > 0) {
      const file = this.fileSelect.current.files[0];
      this.setState({
        keyStatus: 'loading',
        errorMessage: '',
      }, () => {
        // Change interface to show we're loading the file !
        const read = new FileReader();
        read.readAsBinaryString(file);
        read.onloadend = () => {
          setTimeout(() => {
            this.loadKey(read.result);            
          }, 1000);
        };
      });
    }
  }

  loadKey(exportedKeyPair) {
    const { user, loadPrivKey, t } = this.props;
    try {
      const key = JSON.parse(exportedKeyPair);
      if (key && key.pubkey) {
        // Check (API) if the user owns this key
        APIClient.get(`/users/is-owner/publicKey/${key.pubkey}`, true).then((response) => {
          if (response.body.is_owner) {
            loadPrivKey(key.encryptedSecretKey, user.uuid, () => {
              localStorage.setItem(`encrypted_box_privkey_${user.uuid}`, exportedKeyPair);
            });
          } else {
            this.setState({
              keyStatus: 'waiting',
              errorMessage: t('errors.owner', 'Vous n`êtes pas propriétaire de cette clé'),
            });
          }
        }, (error) => {
          console.error(error);
          this.setState({
            keyStatus: 'waiting',
            errorMessage: t('errors.unknown', 'Une erreur est survenue. Veuillez réessayer.'),
          });
        });
      } else {
        console.log('ERROR');
        console.log(key);
        console.log(key.pubkey);
      }
    } catch (e) {
      this.setState({
        keyStatus: 'waiting',
        errorMessage: t('errors.format', 'Format de la clé invalide. Veuillez vérifier que vous avez bien selectionné votre clé.'),
      });
    }
  }

  render() {
    const { user, auth, t } = this.props;
    const { keyStatus, errorMessage } = this.state;

    if (user && user.key) {
      if (keyStatus === 'waiting') {
        return (
          <div>
            <SideMenu
              isDrawerOpen={this.state.isDrawerOpen}
              onClose={this.toggleDrawer(false)}
              auth={auth}
            />
            <div className="drop-area">
              <AppBar position="static" color="inherit" className="headerBar">
                <Toolbar>
                  <IconButton color="inherit" aria-label="Menu" onClick={this.toggleDrawer(true)}>
                    <MenuIcon />
                  </IconButton>
                  <div className="unlockLogoWrapper">
                    <img src="/img/logo-typo.png" alt="Misakey" className="unlockLogo" />
                  </div>
                </Toolbar>
              </AppBar>
              <div className="sealContainer">
                <form className="sealContainerContent" onSubmit={this.handleSubmit}>
                  <Emoji emoji="1f511" name="Key" size={64} />
                  <Typography variant="h6">{t('enterKey', 'Veuillez saisir ou importer votre clef privée pour accéder à vos données')}</Typography>
                  <div className="inputsKey">
                    <FormControl
                      error={errorMessage.length > 0}
                      aria-describedby="key-error"
                      fullWidth
                      variant="outlined"
                    >
                      <TextField
                        variant="outlined"
                        label={t('keyLabel', 'Clé privée')}
                        value={this.state.exportedKeyInput}
                        type="password"
                        onChange={this.handleChange('exportedKeyInput')}
                      />
                      <FormHelperText id="key-error">
                        {
                          (errorMessage.length > 0) ?
                            errorMessage :
                            t('keyHint', 'Astuce: le fichier s\'appelle probablement {{keyName}}', { keyName: user.key.name })
                        }
                      </FormHelperText>
                    </FormControl>
                    <label htmlFor="upload-key-input">
                      <input
                        accept=".misakey,.txt"
                        style={{ display: 'none' }}
                        id="upload-key-input"
                        type="file"
                        onChange={this.handleFileSelect}
                        ref={this.fileSelect}
                      />
                      <Button
                        variant="contained"
                        component="span"
                        className="openFileExplorerButton"
                        disableRipple
                        disableFocusRipple
                      >
                        <FolderIcon />
                      </Button>
                    </label>
                  </div>
                  <div className="buttonValidate">
                    <Button variant="contained" type="submit" color="primary">
                      {t('validateButton', 'Valider')}
                    </Button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        );
      }
      return (
        <div className="drop-area">
          <AppBar position="static" color="default" className="headerBar">
            <Toolbar>
              <IconButton color="inherit" aria-label="Menu" onClick={this.toggleDrawer(true)}>
                <MenuIcon />
              </IconButton>
              <div className="unlockLogoWrapper">
                <img src="/img/logo-typo.png" alt="Misakey" className="unlockLogo" />
              </div>
            </Toolbar>
          </AppBar>
          <div className="sealContainer">
            <div className="sealContainerContent">
              <Emoji emoji="1f511" name="Key" size={64} />
              <Typography variant="h6">
                {t('waitUnlock', 'Veuillez patienter, nous dévérouillons votre conteneur')}
              </Typography>
              <CircularProgress
                variant="indeterminate"
                size={40}
                thickness={4}
              />
            </div>
          </div>
        </div>
      );
    }
    return <Loading />;
  }
}


function mapStateToProps(state) {
  return {
    user: state.users.user,
    cryptoKey: state.users.key,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loadPrivKey: (privkey, password, successCallback) => dispatch(loadPrivKeyAction(privkey, password, successCallback)),
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTheme()(translate('unlock')(UnlockSealScreen))));
