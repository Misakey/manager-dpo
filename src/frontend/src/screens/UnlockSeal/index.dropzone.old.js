import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Dropzone from 'react-dropzone';

import { withTheme } from '@material-ui/core/styles';
import {
  Typography, Paper, CircularProgress, Button,
  AppBar, Toolbar, IconButton, FormHelperText,
  FormControl, TextField,
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import FolderIcon from '@material-ui/icons/Folder';

import BaseComponent from '../../components/_BaseComponent';
import SideMenu from '../../components/SideMenu';
import Emoji from '../../components/Emoji';
import Loading from '../Loading';

import { loadPrivKeyAction } from '../../store/users/actions';

import '../../style/UnlockSeal.css';

import APIClient from '../../utils/apiClient';


class UnlockSealScreen extends BaseComponent {
  static propTypes = {
    user: PropTypes.object,
  };

  static defaultProps = {
    user: null,
  }

  state = {
    keyStatus: 'waiting', // or 'loading'
    errorMessage: '',
    isDrawerOpen: false,
    exportedKeyInput: '',
  }

  constructor(props) {
    super(props);

    this.dropArea = React.createRef();
    this.dropZone = React.createRef();

    this.highlight = this.highlight.bind(this);
    this.unhighlight = this.unhighlight.bind(this);

    this.handleChange = this.handleChange.bind(this);
    this.toggleDrawer = this.toggleDrawer.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.loadKey = this.loadKey.bind(this);
    this.onDrop = this.onDrop.bind(this);
  }

  componentDidUpdate() {
    const { cryptoKey } = this.props;
    if (cryptoKey) {
      this.props.history.goBack();
    }
  }

  handleChange = name => (event) => {
    this.setState({
      [name]: event.target.value,
    });
  };

  toggleDrawer = open => () => {
    this.setState({
      isDrawerOpen: open,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const { exportedKeyInput } = this.state;

    if (exportedKeyInput.length > 0) {
      this.loadKey(exportedKeyInput);
    } else {
      this.setState({
        errorMessage: 'La clef ne correspond pas.',
      })
    }   
  }

  onDrop = (acceptedFiles, rejectedFiles) => {
    const { user, loadPrivKey } = this.props;
    this.unhighlight();
    // Do something with files
    if (acceptedFiles.length > 0) {
      this.setState({
        keyStatus: 'loading',
        errorMessage: '',
      }, () => {
        // Change interface to show we're loading the file !
        const file = acceptedFiles[0];
        const read = new FileReader();
        read.readAsBinaryString(file);
        read.onloadend = () => {
          this.loadKey(read.result);
        };
      });
    } else if (rejectedFiles.length > 0) {
      this.setState({
        keyStatus: 'waiting',
        errorMessage: 'Format de la clef invalide. Votre fichier doit être en ".misakey"',
      });
    }
  }

  loadKey(exportedKey) {
    const { user, loadPrivKey } = this.props;
    try {
      const key = JSON.parse(exportedKey);
      if (key.key && key.key.pubkey) {
        // Check (API) if the user owns this key
        APIClient.get(`/users/is-owner/publicKey/${key.key.pubkey}`, true).then((response) => {
          if (response.is_owner) {
            loadPrivKey(key.key.privkey, user.uuid);
            localStorage.setItem(`encrypted_box_privkey_${user.uuid}`, key.key.privkey);
          } else {
            this.setState({
              keyStatus: 'waiting',
              errorMessage: 'Vous n`êtes pas propriétaire de cette clef.',
            });
          }
        }, (error) => {
          console.error(error);
          this.setState({
            keyStatus: 'waiting',
            errorMessage: 'Une erreur est survenue. Veuillez réessayer.',
          });
        });
      } else {
        console.log('ERROR');
        console.log(key.key);
        console.log(key.key.pubkey);
      }
    } catch (e) {
      this.setState({
        keyStatus: 'waiting',
        errorMessage: 'Format de la clef invalide. Veuillez vérifier que vous avez bien selectionné votre clef.',
      });
    }
  }

  highlight() {
    this.dropArea.current.classList.add('highlight');
  }

  unhighlight() {
    this.dropArea.current.classList.remove('highlight');
  }

  render() {
    const { user, auth } = this.props;
    const { keyStatus, errorMessage } = this.state;

    if (user) {
      if (keyStatus === 'waiting') {
        return (
          <div>
            <SideMenu
              isDrawerOpen={this.state.isDrawerOpen}
              onClose={this.toggleDrawer(false)}
              auth={auth}
            />
            <Dropzone
              onDrop={this.onDrop}
              accept=".misakey"
              multiple={false}
              disableClick
              ref={this.dropZone}
            >
              {({ getRootProps, getInputProps }) => (
                <div
                  className="drop-area"
                  {...getRootProps()}
                  ref={this.dropArea}
                >
                  <AppBar position="static" color="default" className="headerBar">
                    <Toolbar>
                      <IconButton color="inherit" aria-label="Menu" onClick={this.toggleDrawer(true)}>
                        <MenuIcon />
                      </IconButton>
                      <Typography variant="h5" color="primary" align="center" className="headerTitle">
                        Misakey
                      </Typography>
                    </Toolbar>
                  </AppBar>
                  <input {...getInputProps()} />
                  <div className="sealContainer">
                    <form className="sealContainerContent" onSubmit={this.handleSubmit}>
                      <Emoji emoji="1f511" name="Key" size={64} />
                      <Typography variant="h6">Veuillez saisir ou importer votre clef privée pour accéder à vos données</Typography>
                      <div className="inputsKey">
                        <FormControl
                          error={errorMessage.length > 0}
                          aria-describedby="key-error"
                          fullWidth
                          variant="outlined"
                        >
                          <TextField
                            variant="outlined"
                            label="Clé privée"
                            value={this.state.exportedKeyInput}
                            type="password"
                            onChange={this.handleChange('exportedKeyInput')}
                          />
                          <FormHelperText id="key-error">
                            {
                              (errorMessage.length > 0) ? errorMessage : `Hint: It should be named "${user.key.name}.misakey"`
                            }
                          </FormHelperText>
                        </FormControl>
                        <Button
                          variant="contained"
                          onClick={() => this.dropZone.current.open()}
                          className="openFileExplorerButton"
                          disableRipple
                          disableFocusRipple
                        >
                          <FolderIcon />
                        </Button>
                      </div>
                      <div className="buttonValidate">
                        <Button variant="contained" type="submit" color="primary">Valider</Button>
                      </div>
                    </form>
                  </div>
                </div>
              )}
            </Dropzone>
          </div>
        );
      }
      return (
        <div className="drop-area">
          <AppBar position="static" color="default" className="headerBar">
            <Toolbar>
              <IconButton color="inherit" aria-label="Menu" onClick={this.toggleDrawer(true)}>
                <MenuIcon />
              </IconButton>
              <Typography variant="h5" color="primary" align="center" className="headerTitle">
                Misakey
              </Typography>
            </Toolbar>
          </AppBar>
          <div className="sealContainer">
            <div className="sealContainerContent">
              <Emoji emoji="1f511" name="Key" size={64} />
              <Typography variant="h6">Veuillez patienter, nous dévérouillons votre conteneur</Typography>
              <CircularProgress
                variant="indeterminate"
                size={40}
                thickness={4}
              />
            </div>
          </div>
        </div>
      );
    }
    return <Loading />;
  }
}


function mapStateToProps(state) {
  return {
    user: state.users.user,
    cryptoKey: state.users.key,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loadPrivKey: (privkey, password) => dispatch(loadPrivKeyAction(privkey, password)),
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTheme()(UnlockSealScreen)));
