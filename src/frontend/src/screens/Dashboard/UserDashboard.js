import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { updateCompaniesListAction } from '../../store/dashboard/actions';

import BaseComponent from '../../components/_BaseComponent';

import '../../style/Dashboard.css';

import Dashboard from './Dashboard';
import Loading from '../Loading';

class UserDashboardScreen extends BaseComponent {
  static propTypes = {
    updateCompaniesList: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
  };

  render() {
    const {
      auth, history, user, updateCompaniesList,
    } = this.props;

    if (user) {
      return (
        <Dashboard
          history={history}
          auth={auth}
          updateList={() => updateCompaniesList(user.uuid)}
        />
      );
    }
    return <Loading />;
  }
}

function mapStateToProps(state) {
  return {
    user: state.users.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    updateCompaniesList: uuid => dispatch(updateCompaniesListAction(uuid)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(UserDashboardScreen);
