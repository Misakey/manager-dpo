import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import withWidth, { isWidthDown } from '@material-ui/core/withWidth';

import BaseComponent from '../../components/_BaseComponent';
import SideMenu from '../../components/SideMenu';
import ChatView from '../../components/ChatView';
import ItemsList from '../../components/ItemsList';

import Loading from '../Loading';

import {
  fetchItemAction, unselectItemAction,
} from '../../store/dashboard/actions';


import '../../style/Dashboard.css';

class DashboardScreen extends BaseComponent {
  leftListMinWidth = '300px';

  refreshIntervalDuration = 60000;

  state = {
    isMenuOpen: false,
    activeScreen: 'list',
  }

  componentDidMount() {
    const { updateList } = this.props;
    updateList();
    this.listRefreshIntervalId = setInterval(updateList, this.refreshIntervalDuration);
  }

  componentWillUnmount() {
    clearInterval(this.listRefreshIntervalId);
    clearInterval(this.itemRefreshIntervalId);
  }

  toggleMenu = isOpen => () => {
    this.setState({
      isMenuOpen: isOpen,
    });
  }

  selectItem = itemUUID => () => {
    const { fetchItem, variant } = this.props;
    fetchItem(variant, itemUUID);
    clearInterval(this.itemRefreshIntervalId);
    this.itemRefreshIntervalId = setInterval(() => fetchItem(variant, itemUUID), this.refreshIntervalDuration);
  }

  goBackToList = () => {
    const { unselectItem } = this.props;
    unselectItem();
    clearInterval(this.itemRefreshIntervalId);
  }

  render() {
    const {
      user, width, auth, activeItem, variant, fetchItem,
    } = this.props;
    const {
      isMenuOpen,
    } = this.state;

    const isMobile = isWidthDown('xs', width);

    if (user) {
      let mainScreen;
      if (isMobile) {
        if (Object.entries(activeItem).length === 0) {
          mainScreen = (
            <ItemsList
              style={{ width: '100vw' }}
              width="100vw"
              toggleMenu={this.toggleMenu}
              clickOnLink={this.selectItem}
            />
          );
        } else {
          mainScreen = (
            <ChatView
              style={{ width: '100%' }}
              goBackToList={this.goBackToList}
              isFullScreen
              reload={() => fetchItem(variant, activeItem.uuid)}
            />
          );
        }
      } else {
        mainScreen = (
          <div>
            <ItemsList
              style={{ width: this.leftListMinWidth }}
              width={this.leftListMinWidth}
              toggleMenu={this.toggleMenu}
              clickOnLink={this.selectItem}
            />
            <ChatView
              style={{ width: `calc(100% - ${this.leftListMinWidth})`, marginLeft: this.leftListMinWidth }}
              goBackToList={this.goBackToList}
              reload={() => fetchItem(variant, activeItem.uuid)}
            />
          </div>
        );
      }

      return (
        <div>
          {mainScreen}
          <SideMenu
            isDrawerOpen={isMenuOpen}
            onClose={this.toggleMenu(false)}
            auth={auth}
          />
        </div>
      );
    }
    return <Loading />;
  }
}

DashboardScreen.propTypes = {
  user: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    user: state.users.user,
    activeItem: state.dashboard.active,
    variant: state.dashboard.variant,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchItem: (variant, uuid) => dispatch(fetchItemAction(variant, uuid)),
    unselectItem: () => dispatch(unselectItemAction()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withWidth()(DashboardScreen));
