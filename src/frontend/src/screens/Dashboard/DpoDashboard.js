import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { updateUsersListAction } from '../../store/dashboard/actions';

import Loading from '../Loading';
import BaseComponent from '../../components/_BaseComponent';

import '../../style/Dashboard.css';

import Dashboard from './Dashboard';

class DpoDashboardScreen extends BaseComponent {
  static propTypes = {
    updateUsersList: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
  };

  render() {
    const {
      auth, history, updateUsersList, user,
    } = this.props;

    if (user) {
      return (
        <Dashboard
          history={history}
          auth={auth}
          updateList={() => updateUsersList(user.company.uuid)}
        />
      );
    }
    return <Loading />;
  }
}

function mapStateToProps(state) {
  return {
    itemsList: state.dashboard.list,
    user: state.users.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    updateUsersList: uuid => dispatch(updateUsersListAction(uuid)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DpoDashboardScreen);
