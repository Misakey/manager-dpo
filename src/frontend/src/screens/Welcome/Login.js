import React from 'react';
import PropTypes from 'prop-types';
import { Link as RouterLink, withRouter } from 'react-router-dom';
import { translate, Trans } from 'react-i18next';
import { withTheme } from '@material-ui/core/styles';
import {
  Paper, TextField, FormControl, Link, IconButton,
  Button, FormHelperText, Typography, Modal,
} from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';

import BaseComponent from '../../components/_BaseComponent';

import { validateEmail } from '../../utils/misc/regex';

import { ROUTES } from '../../Routes';

import '../../style/Welcome.css';

class Login extends BaseComponent {
  static propTypes = {
    isLogin: PropTypes.bool.isRequired,
  }

  static defaultProps = {
    isLogin: false,
  }

  state = {
    email: '',
    emailError: '',
    password: '',
    repeatPassword: '',
    passwordError: '',

    isDpoCreateAccountModalOpen: false,
  }

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.toggleDpoCreateAccountModal = this.toggleDpoCreateAccountModal.bind(this);
  }

  componentDidMount() {
    const { history } = this.props;
    if (localStorage.getItem(`${process.env.REACT_APP_FRONTEND_TYPE}_isLoggedIn`) === 'true') {
      history.replace(ROUTES.DASHBOARD_ROUTE);
    }
  }

  toggleDpoCreateAccountModal() {
    this.setState(prevState => ({
      isDpoCreateAccountModalOpen: !prevState.isDpoCreateAccountModalOpen,
    }));
  }

  handleChange = name => (event) => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handleSubmit(e) {
    e.preventDefault();

    const { email, password } = this.state;
    const { t } = this.props;

    let emailError = '';
    let passwordError = '';
    if (!validateEmail(email)) {
      emailError = t('errors.invalidEmail', 'Courriel invalide.');
    }
    if (password.length === 0) {
      passwordError = t('errors.emptyPassword', 'Merci de saisir un mot de passe.');
    }
    if (emailError.length > 0 || passwordError.length > 0) {
      this.setState({
        emailError,
        passwordError,
      });
    } else {
      this.props.auth.auth0.login({
        email,
        password,
        realm: process.env.REACT_APP_AUTH0_REALM,
      }, (err) => {
        console.log(err);
        this.setState({
          emailError: t(`errors.${err.code}`, ' Le mot de passe ne correspond pas.'),
        });
      });
    }
    return false;
  }

  render() {
    const { theme, t, dpo } = this.props;
    const {
      email, password, emailError, passwordError, isDpoCreateAccountModalOpen,
    } = this.state;

    return (
      <div className="welcomeContainer" style={{ backgroundColor: theme.palette.vertdeau.main }}>
        <div className="contentContainer">
          <Paper
            className="idFormContainer"
            elevation={0}
            component="form"
            onSubmit={this.handleSubmit}
          >
            <img src="/img/logo-typo.png" alt="Misakey Logo" className="loginLogo" />
            <Typography variant="h6">
              {t('connexionTitle', 'Connection')}
            </Typography>
            <Typography>
              {
                (dpo)
                  ? t('dpoConnexionSubtitle', 'Utilisez votre compte DPO')
                  : t('connexionSubtitle', 'Utilisez votre compte Misakey')
              }
            </Typography>

            <FormControl fullWidth error={emailError.length > 0} aria-describedby="email-error">
              <TextField
                error={emailError.length > 0}
                id="outlined-name"
                label={t('email', 'Courriel')}
                margin="normal"
                variant="outlined"
                onChange={this.handleChange('email')}
                value={email}
                InputProps={{ inputProps: { "data-matomo-mask": true } }}
              />
              {
                (emailError.length === 0) ? null : (
                  <FormHelperText id="email-error">{emailError}</FormHelperText>
                )
              }
            </FormControl>
            <FormControl fullWidth error={passwordError.length > 0} aria-describedby="password-error">
              <TextField
                error={passwordError.length > 0}
                id="outlined-password-input"
                label={t('password', 'Mot de passe!')}
                type="password"
                autoComplete="current-password"
                margin="normal"
                variant="outlined"
                onChange={this.handleChange('password')}
                value={password}
                InputProps={{ inputProps: { "data-matomo-mask": true } }}
              />
              {
                (passwordError.length === 0) ? null : (
                  <FormHelperText id="password-error">{passwordError}</FormHelperText>
                )
              }
            </FormControl>

            <div className="loginButtonGroup">
              {
                (dpo) ? (
                  <Button
                    color="primary"
                    onClick={this.toggleDpoCreateAccountModal}
                  >
                    {t('noAccountButton', 'Créer un compte')}
                  </Button>
                ) : (
                  <Button
                    color="primary"
                    component={RouterLink}
                    to={ROUTES.REGISTER_ROUTE}
                  >
                    {t('noAccountButton', 'Créer un compte')}
                  </Button>
                )
              }
              <Button variant="contained" color="primary" type="submit">
                {t('loginButton', 'Se connecter')}
              </Button>
            </div>
          </Paper>
          <Typography variant="body2" align="center" className="infoLinks">
            <Link
              href={t('common:sideMenu.privacyLink')}
              color="textSecondary"
              target="_blank"
              rel="noopener noreferrer"
            >
              {t('common:sideMenu.privacyLabel', 'Règles de confidentialité')}
            </Link>
            {' - '}
            <Link
              href={t('common:sideMenu.tosLink')}
              color="textSecondary"
              target="_blank"
              rel="noopener noreferrer"
            >
              {t('common:sideMenu.tosLabel', 'Condition d’utilisation')}
            </Link>
          </Typography>
        </div>
        {
          (dpo) ? (
            <Modal
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
              open={isDpoCreateAccountModalOpen}
              onClose={this.toggleDpoCreateAccountModal}
              className="dpoCreateAccountModalWrapper"
            >
              <Paper className="dpoCreateAccountModal">
                <div className="modalHeader">
                  <div className="placeHolder48px"/>
                  <Typography align="center" variant="h5">
                    {t('registerTitle', 'Inscription')}
                  </Typography>
                  <IconButton aria-label="Close" onClick={this.toggleDpoCreateAccountModal} className="closeModalButton">
                    <CloseIcon />
                  </IconButton>
                </div>
                <Typography variant="subtitle1" id="simple-modal-description">
                  <Trans i18nKey="dpoNoOpenRegistration">
                    {'Pour créer un compte DPO, contactez-nous directement à '}
                    <Link color="primary" href="mailto:arthur@love.misakey.com">arthur@love.misakey.com</Link>                    
                    {' afin que nous puissions valider votre profil organisation'}
                  </Trans>
                </Typography>
              </Paper>
            </Modal>
          ) : null
        }
      </div>
    );
  }
}

export default withTheme()(translate('welcome')(withRouter(Login)));
