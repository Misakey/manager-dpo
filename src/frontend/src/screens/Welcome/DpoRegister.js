import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { translate, Trans } from 'react-i18next';
import { withTheme } from '@material-ui/core/styles';

import {
  Typography, Paper, Button, Avatar,
  TextField, FormControl, Link,
  Divider, FormControlLabel, Checkbox,
  FormGroup, FormHelperText, CircularProgress,
} from '@material-ui/core';

import SwapHorizIcon from '@material-ui/icons/SwapHoriz';

import BaseComponent from '../../components/_BaseComponent';

import {
  getCompanyInformationsWithEmailAction,
  createDpoAccountAction,
} from '../../store/welcome/actions';

import { ROUTES } from '../../Routes';

import APIClient from '../../utils/apiClient';

import '../../style/Welcome.css';


// TODO: Onload check if uuid & email are matching, if not redirect to connect (or 404)
class DpoRegister extends BaseComponent {
  static propTypes = {
    t: PropTypes.func.isRequired,
  }

  static defaultProps = {
  }

  state = {
    step: 'password',
    password: '',
    passwordError: '',
    areTOSChecked: false,
    isPrivacyChecked: false,
  }

  constructor(props) {
    super(props);
    this.handleCheck = this.handleCheck.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.nextStep = this.nextStep.bind(this);
  }


  async componentDidMount() {
    const {
      history, email, uuid,
      getCompanyInformationsWithEmail,
    } = this.props;
    if (localStorage.getItem(`${process.env.REACT_APP_FRONTEND_TYPE}_isLoggedIn`) === 'true') {
      history.replace(ROUTES.DASHBOARD_ROUTE);
    } else {
      getCompanyInformationsWithEmail(uuid, email);
    }
  }

  componentDidUpdate() {
    const {
      isRegistrationDone, email, shouldRedirectToLoginPage, history,
    } = this.props;
    const { password } = this.state;
    if (shouldRedirectToLoginPage) {
      history.replace(ROUTES.DASHBOARD_ROUTE);
    }
    if (isRegistrationDone) {
      this.props.auth.auth0.login({
        email,
        password,
        realm: process.env.REACT_APP_AUTH0_REALM,
      });
    }
  }

  handleChange = name => (event) => {
    this.setState({
      [name]: event.target.value,
    });
  }

  handleCheck = name => () => {
    this.setState(prevState => ({
      [name]: !prevState[name],
    }));
  }

  async nextStep(e) {
    e.preventDefault();

    const { step, password } = this.state;

    const {
      createDpoAccount, uuid, email, history,
    } = this.props;

    switch (step) {
      case 'password':
        if (this.checkCredentialsValidity()) {
          this.setState({
            step: 'validate_tos',
          });
        }
        break;

      case 'validate_tos':
        this.setState({
          step: 'account_creation',
        }, async () => {
          APIClient.setSync(true);
          // Check if UUID / email are matching
          const companyExists = await APIClient.head(`/companies/${uuid}/${email}`, false);
          APIClient.setSync(false);
          if (companyExists.httpStatus === 204) {
            this.props.auth.auth0.signup({
              connection: process.env.REACT_APP_AUTH0_REALM,
              email,
              password,
            }, (err, params) => {
              if (err) {
                console.log(err);
              } else {
                const sub = `auth0|${params.Id}`;
                createDpoAccount(uuid, sub, email);
              }
            });
          } else {
            history.replace(ROUTES.ROUTE_LOGIN);
          }
        });
        break;

      default:
        break;
    }
  }

  checkCredentialsValidity() {
    const { password } = this.state;
    const { t } = this.props;
    let passwordError = '';

    if (password.length < 6) {
      passwordError = t('errors.passwordStrength', 'Mot de passe trop faible.');
    }

    if (passwordError.length > 0) {
      this.setState({
        passwordError,
      });
      return false;
    }
    return true;
  }

  render() {
    const { theme, t, email, company } = this.props;
    const {
      password, passwordError,
      step, areTOSChecked, isPrivacyChecked,
    } = this.state;

    let modifyButton = (
      <Button onClick={this.backToModify}>
        {t('mandate.backButton', 'Retour')}
      </Button>
    );

    let logo = (<img src="/img/logo-typo.png" alt="Misakey Logo" className="loginLogo" />);
    let title;
    let content;


    if (company === null) {
      title = t('registerDpoLoading', 'Chargement...');
      content = (
        <CircularProgress
          variant="indeterminate"
          size={40}
          thickness={4}
        />
      );
    } else {
      switch (step) {
        case 'password':
          title = t('registerDpoTitle', 'Inscription');
          title = email
          modifyButton = null;
          logo = (
            <div className="logosGroup">
              <img src="/img/logo-typo.png" alt="Misakey Logo" className="loginLogo" />
              <SwapHorizIcon />
              <Avatar alt={company.name} src={company.logo} />
            </div>
          );
          content = (
            <div>
              <Typography align="center">
                {t('registerDpoSubtitle', 'Créer le mot de passe de votre compte responsable de données Misakey pour {{companyName}}.', { companyName: company.name})}
              </Typography>
              <FormControl fullWidth error={passwordError.length > 0} aria-describedby="password-error">
                <TextField
                  error={passwordError.length > 0}
                  id="outlined-password-input"
                  label={t('password', 'Mot de passe!')}
                  type="password"
                  autoComplete="current-password"
                  margin="normal"
                  variant="outlined"
                  onChange={this.handleChange('password')}
                  value={password}
                />
                {
                  (passwordError.length === 0) ? null : (
                    <FormHelperText id="password-error">{passwordError}</FormHelperText>
                  )
                }
              </FormControl>
              <div className="DpoRegisterButtonGroup">
                <Button variant="contained" color="primary" type="submit">
                  {t('registerNextStepButton', 'Suivant')}
                </Button>
              </div>
            </div>
          );
          break;


        case 'validate_tos':
          title = t('mandate.tosTitle', 'Règles de confidentialité et conditions d\'utilisation');
          content = (
            <div className="mandateContainer">
              <Trans i18nKey="mandate.tos">
                <Typography>
                  Pour créer un compte Misakey, vous devez accepter les <Link href="https://www.misakey.com/#mk-legal-tos">Conditions d'utilisation</Link> ci-dessous.<br />
                  De plus, lorsque vous créez un compte, nous traitons vos informations comme décrit dans nos <Link href="https://www.misakey.com/#mk-legal-privacy">Règles de confidentialité</Link>, y compris les points clés suivants :<br />
                  <br />
                  <strong>Données que nous traitons lorsque vous utilisez Misakey</strong>
                </Typography>
                <ul>
                  <li><Typography>Lorsque vous configurez un compte Misakey, nous enregistrons les informations que vous nous fournissez, telles que votre nom, votre adresse e-mail et votre pays de résidence.</Typography></li>
                  <li><Typography>Lorsque vous échangez des données personnelles au travers des services Misakey, nous chiffrons les informations de bout en bout afin que nous ne soyons pas en mesure d’y accéder.</Typography></li>
                  <li><Typography>Lorsque vous utilisez les services Misakey, nous enregistrons des informations liées à l’usage de nos services, telles que votre IP, votre navigateur et votre appareil.</Typography></li>
                </ul>
                <Typography>
                  <strong>Pourquoi nous traitons les données</strong>
                  Nous traitons ces données conformément à <Link href="https://www.misakey.com/#mk-legal-tos">notre règlement</Link>, notamment aux fins suivantes :
                </Typography>
                <ul>
                  <li><Typography>Améliorer la qualité de nos services et en développer de nouveaux</Typography></li>
                  <li><Typography>Renforcer la sécurité en vous protégeant contre la fraude et les abus</Typography></li>
                  <li><Typography>Effectuer des analyses et des mesures afin de comprendre comment nos services sont utilisés. </Typography></li>
                </ul>
                <Typography>
                  <strong>Vous contrôlez vos données</strong>

                  Selon les paramètres de votre compte, certaines de ces données peuvent être associées à votre compte Misakey et traitées comme des informations personnelles. Vous pouvez contrôler dès maintenant la façon dont nous collectons et utilisons ces données en cliquant sur "Plus d'options" ci-dessous. Vous pourrez à tout moment ajuster les paramètres ou retirer votre consentement pour l'avenir en accédant à la page Mon compte (my.misakey.com).
                </Typography>
              </Trans>
              <Divider variant="middle" />

              <FormControl component="fieldset">
                <FormGroup>
                  <FormControlLabel
                    control={(
                      <Checkbox
                        checked={areTOSChecked}
                        onChange={this.handleCheck('areTOSChecked')}
                        value="tos"
                        color="primary"
                      />
                    )}
                    label={t('mandate.tosValidate', 'J\'accepte les conditions d\'utilisation de Misakey')}
                    color="primary"
                  />
                  <FormControlLabel
                    control={(
                      <Checkbox
                        checked={isPrivacyChecked}
                        onChange={this.handleCheck('isPrivacyChecked')}
                        value="privacy"
                        color="primary"
                      />
                    )}
                    label={t('mandate.privacyValidate', 'J\'accepte que mes informations soient utilisées tel que décrit ci-dessus et détaillé dans les règles de confidentialité')}
                  />
                </FormGroup>
              </FormControl>
              <div className="loginButtonGroup">
                {modifyButton}
                <Button
                  variant="contained"
                  color="primary"
                  disabled={!(isPrivacyChecked && areTOSChecked)}
                  onClick={this.nextStep}
                >
                  {t('mandate.createAccountButton', 'Créer un compte')}
                </Button>
              </div>
            </div>
          );
          break;

        case 'account_creation':
          title = t('mandate.accountCreation', 'Création du compte en cours');
          content = (
            <CircularProgress
              variant="indeterminate"
              size={40}
              thickness={4}
            />
          );
          break;

        default:
          break;
      }
    }

    return (
      <div className="welcomeContainer" style={{ backgroundColor: theme.palette.vertdeau.main }}>
        <div
          className={`contentContainer${(step === 'validate_tos')
            ? ' contentContainerLarge'
            : ''}`}
        >
          <Paper
            className="idFormContainer"
            elevation={0}
            component="form"
            onSubmit={this.nextStep}
          >
            {logo}
            <Typography variant="h6">
              {title}
            </Typography>
            {content}
          </Paper>
          {
            (step === 'password') ? (
              <Typography variant="body2" align="center" className="infoLinks">
                <Link
                  href={t('common:sideMenu.privacyLink')}
                  color="textSecondary"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {t('common:sideMenu.privacyLabel', 'Règles de confidentialité')}
                </Link>
                {' - '}
                <Link
                  href={t('common:sideMenu.tosLink')}
                  color="textSecondary"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {t('common:sideMenu.tosLabel', 'Condition d’utilisation')}
                </Link>
              </Typography>
            ) : null
          }
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isRegistrationDone: state.welcome.isRegistrationDone,
    company: state.welcome.company,
    shouldRedirectToLoginPage: state.welcome.shouldRedirectToLoginPage,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    createDpoAccount: (uuid, sub, email) => dispatch(createDpoAccountAction(uuid, sub, email)),
    getCompanyInformationsWithEmail: (uuid, email) => dispatch(getCompanyInformationsWithEmailAction(uuid, email)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme()(translate('welcome')(withRouter(DpoRegister))));
