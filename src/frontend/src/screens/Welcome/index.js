import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import Login from './Login';
import Register from './Register';
import DpoRegister from './DpoRegister';

import { ROUTES } from '../../Routes';

import '../../style/Welcome.css';

class WelcomeScreen extends React.Component {
  static propTypes = {
    isLogin: PropTypes.bool,
    history: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    dpo: PropTypes.bool,
    match: PropTypes.object,
  }

  static defaultProps = {
    match: { params: {} },
    isLogin: false,
    dpo: false,
  }

  shouldRender = true

  constructor(props) {
    super(props);

    if (props.dpo && !props.isLogin) {
      const { match, history } = props;
      const { linkKey } = match.params;

      try {
        JSON.parse(window.atob(linkKey));
      } catch (e) {
        this.shouldRender = false;
        history.replace(ROUTES.DASHBOARD_ROUTE);
      }
    }
  }

  componentDidMount() {
    const { history } = this.props;
    if (localStorage.getItem(`${process.env.REACT_APP_FRONTEND_TYPE}_isLoggedIn`) === 'true') {
      history.replace(ROUTES.DASHBOARD_ROUTE);
    }
  }

  render() {
    const {
      isLogin, auth, dpo, match,
    } = this.props;

    if (!this.shouldRender) {
      return null;
    }
    
    if (isLogin) {
      return <Login auth={auth} isLogin dpo={dpo} />;
    }
    if (dpo) {
      const { linkKey } = match.params;

      const { email, uuid } = JSON.parse(window.atob(linkKey));
      return <DpoRegister auth={auth} dpo email={email} uuid={uuid} />;
    }
    return <Register auth={auth} />;
  }
}

export default withRouter(WelcomeScreen);
