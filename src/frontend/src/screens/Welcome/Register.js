import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link as RouterLink, withRouter } from 'react-router-dom';
import { translate, Trans } from 'react-i18next';
import { withTheme } from '@material-ui/core/styles';

import SignatureCanvas from 'react-signature-canvas';
import {
  Typography, Paper, Button,
  TextField, FormControl, InputLabel,
  Select, OutlinedInput, List, ListItem,
  ListItemText, ListItemIcon, Link,
  Divider, FormControlLabel, Checkbox,
  FormGroup, FormHelperText, CircularProgress,
} from '@material-ui/core';

import Emoji from '../../components/Emoji';

import BaseComponent from '../../components/_BaseComponent';

import { validateEmail } from '../../utils/misc/regex';
import { nowAsDay } from '../../utils/misc/dateFormat';

import { createAccountAction } from '../../store/users/actions';

import { ROUTES } from '../../Routes';
import { countries } from '../../data/countries';
import { dataURLToBlob } from '../../utils/misc/fileDownload';

import APIClient from '../../utils/apiClient';

import '../../style/Welcome.css';

class Register extends BaseComponent {
  static propTypes = {
    t: PropTypes.func.isRequired,
  }

  static defaultProps = {
  }

  state = {
    step: 'email_password',
    email: '',
    emailError: '',
    password: '',
    passwordError: '',
    firstName: '',
    firstNameError: '',
    familyName: '',
    familyNameError: '',
    country: 'FR',
    countryLabelWidth: 35,
    areTOSChecked: false,
    isPrivacyChecked: false,
    signatureBlob: null,
    signatureError: '',
  }

  constructor(props) {
    super(props);
    this.handleCheck = this.handleCheck.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.selectCountry = React.createRef();
    this.signatureCanvas = React.createRef();

    this.nextStep = this.nextStep.bind(this);
    this.backToModify = this.backToModify.bind(this);
  }

  componentDidMount() {
    const { history } = this.props;
    if (localStorage.getItem(`${process.env.REACT_APP_FRONTEND_TYPE}_isLoggedIn`) === 'true') {
      history.replace(ROUTES.DASHBOARD_ROUTE);
    }
    if (this.selectCountry.current) { // NOT WORKING
      this.setState({
        countryLabelWidth: this.selectCountry.current.offsetWidth,
      });
    }
  }

  componentDidUpdate() {
    const { isRegistrationDone } = this.props;
    const { email, password } = this.state;
    if (isRegistrationDone) {
      this.props.auth.auth0.login({
        email,
        password,
        realm: process.env.REACT_APP_AUTH0_REALM,
      });
    }
  }

  backToModify = () => {
    this.setState((prevState) => {
      let newStep = prevState.step;
      switch (prevState.step) {
        case 'enter_data':
          newStep = 'email_password';
          break;

        case 'acknowledge_mandate':
          newStep = 'enter_data';
          break;

        case 'sign_mandate':
          newStep = 'acknowledge_mandate';
          break;

        case 'validate_tos':
          newStep = 'sign_mandate';
          break;

        default:
          break;
      }
      if (newStep !== prevState.step) {
        return {
          step: newStep,
        };
      }
    });
  }

  handleChange = name => (event) => {
    this.setState({
      [name]: event.target.value,
    });
  }

  handleCheck = name => () => {
    this.setState(prevState => ({
      [name]: !prevState[name],
    }));
  }

  async checkCredentialsValidity() {
    const { email, password } = this.state;
    const { t } = this.props;
    let emailError = '';
    let passwordError = '';

    if (!validateEmail(email)) {
      emailError = t('errors.invalidEmail', 'Courriel invalide.');
    }
    if (password.length < 6) {
      passwordError = t('errors.passwordStrength', 'Mot de passe trop faible.');
    }

    APIClient.setSync(true);
    const userExists = await APIClient.head(`/users/${email}`, false);
    APIClient.setSync(false);

    if (userExists.httpStatus === 200) {
      emailError = t('errors.existingEmail', 'Un compte existe déjà avec cet email.');
    }

    if (emailError.length > 0 || passwordError.length > 0) {
      this.setState({
        emailError,
        passwordError,
      });
      return false;
    }
    return true;
  }

  checkMandateDataValidity() {
    const { firstName, familyName } = this.state;
    const { t } = this.props;
    let firstNameError = '';
    let familyNameError = '';

    if (firstName.length < 2) {
      firstNameError = t('errors.firstNameRequired', 'Veuillez entrer un prénom');
    }
    if (familyName.length < 2) {
      familyNameError = t('errors.familyNameRequired', 'Veuillez entrer un nom');
    }


    if (firstNameError.length > 0 || familyNameError.length > 0) {
      this.setState({
        familyNameError,
        firstNameError,
      });
      return false;
    }
    return true;
  }

  checkSignatureValidity() {
    const { t } = this.props;
    if (this.signatureCanvas.current.isEmpty()) {
      this.setState({
        signatureError: t('errors.signatureRequired', 'Veuillez apposer votre signature pour le mandat'),
      });
      return false;
    }
    return true;
  }

  async nextStep(e) {
    e.preventDefault();
    const {
      step, firstName, familyName, country, email, password, signatureBlob,
    } = this.state;
    const { createAccount} = this.props;

    switch (step) {
      case 'email_password':
        if (await this.checkCredentialsValidity()) {
          this.setState({
            step: 'enter_data',
          });
        }
        break;

      case 'enter_data':
        if (this.checkMandateDataValidity()) {
          this.setState({
            step: 'acknowledge_mandate',
          });
        }
        break;

      case 'acknowledge_mandate':
        this.setState({
          step: 'sign_mandate',
        });
        break;

      case 'sign_mandate':
        if (this.checkSignatureValidity()) {
          this.setState({
            signatureBlob: dataURLToBlob(this.signatureCanvas.current.toDataURL('image/png')),
            step: 'validate_tos',
          });
        }
        break;

      case 'validate_tos':
        // Create account
        this.setState({
          step: 'account_creation',
        }, () => {
          this.props.auth.auth0.signup({
            connection: process.env.REACT_APP_AUTH0_REALM,
            email,
            password,
          }, (err, params) => {
            if (err) {
              console.log(err);
            } else {
              const sub = `auth0|${params.Id}`;
              createAccount({
                sub,
                email,
                firstName,
                familyName,
                country,
                signature: signatureBlob,
              });
            }
          });
        });
        break;

      default:
        break;
    }
    return false;
  }

  render() {
    const { theme, t } = this.props;
    const {
      email, password, emailError, passwordError,
      step, firstName, familyName, country, countryLabelWidth,
      areTOSChecked, isPrivacyChecked, firstNameError, familyNameError, signatureError,
    } = this.state;

    const countryName = countries.find((countryElement => countryElement.value === country)).label;

    let modifyButton = (
      <Button onClick={this.backToModify} color="primary">
        {t('mandate.backButton', 'Retour')}
      </Button>
    );

    let title;
    let content;
    switch (step) {
      case 'email_password':
        title = t('registerTitle', 'Inscription');
        modifyButton = null;
        content = (
          <div>
            <Typography align="center">
              {t('registerSubtitle', 'Créez votre compte Misakey')}
            </Typography>

            <FormControl fullWidth error={emailError.length > 0} aria-describedby="email-error">
              <TextField
                error={emailError.length > 0}
                id="outlined-name"
                label={t('email', 'Courriel')}
                margin="normal"
                variant="outlined"
                onChange={this.handleChange('email')}
                value={email}
                InputProps={{ inputProps: { "data-matomo-mask": true } }}
              />
              {
                (emailError.length === 0) ? null : (
                  <FormHelperText id="email-error">{emailError}</FormHelperText>
                )
              }
            </FormControl>
            <FormControl fullWidth error={passwordError.length > 0} aria-describedby="password-error">
              <TextField
                error={passwordError.length > 0}
                id="outlined-password-input"
                label={t('password', 'Mot de passe!')}
                type="password"
                autoComplete="current-password"
                margin="normal"
                variant="outlined"
                onChange={this.handleChange('password')}
                value={password}
                InputProps={{ inputProps: { "data-matomo-mask": true } }}
              />
              {
                (passwordError.length === 0) ? null : (
                  <FormHelperText id="password-error">{passwordError}</FormHelperText>
                )
              }
            </FormControl>
            <div className="loginButtonGroup">
              <Button color="primary" component={RouterLink} to={ROUTES.LOGIN_ROUTE}>{t('hasAccountButton', 'J\'ai déjà un compte.')}</Button>
              <Button variant="contained" color="primary" type="submit">
                {t('registerNextStepButton', 'Suivant')}
              </Button>
            </div>
          </div>
        );
        break;

      case 'enter_data':
        title = t('mandate.nameCountryTitle', 'Créer mon compte Misakey');
        content = (
          <div className="formMandate mandateContainer">
            <FormControl fullWidth aria-describedby="firstname-error">
              <TextField
                margin="normal"
                value={firstName}
                label={t('mandate.firstName', 'Prénom')}
                variant="outlined"
                onChange={this.handleChange('firstName')}
                InputProps={{ inputProps: { "data-matomo-mask": true } }}
              />
              {
                (firstNameError.length === 0) ? null : (
                  <FormHelperText id="firstname-error">{firstNameError}</FormHelperText>
                )
              }
            </FormControl>

            <FormControl fullWidth aria-describedby="familyname-error">
              <TextField
                margin="normal"
                value={familyName}
                label={t('mandate.lastName', 'Nom')}
                variant="outlined"
                onChange={this.handleChange('familyName')}
                InputProps={{ inputProps: { "data-matomo-mask": true } }}
              />
              {
                (familyNameError.length === 0) ? null : (
                  <FormHelperText id="familyname-error">{familyNameError}</FormHelperText>
                )
              }
            </FormControl>
            <FormControl margin="normal" variant="outlined">
              <InputLabel
                htmlFor="country"
                ref={this.selectCountry}
              >
                {t('mandate.country', 'Pays')}
              </InputLabel>
              <Select
                native
                defaultValue={country}
                onChange={this.handleChange('country')}
                input={(
                  <OutlinedInput
                    name="country"
                    labelWidth={countryLabelWidth}
                    id="country"
                  />
                )}
              >
                {
                  countries.map(countryElement => (
                    <option value={countryElement.value} key={countryElement.value}>{countryElement.label}</option>
                  ))
                }
              </Select>
            </FormControl>
            <div className="loginButtonGroup">
              {modifyButton}
              <Button variant="contained" color="primary" type="submit">
                {t('mandate.validateButton', 'Valider')}
              </Button>
            </div>
          </div>
        );
        break;

      case 'acknowledge_mandate':
        title = t('mandate.acknowledgeMandateTitle', 'Valider le mandat de mission');
        content = (
          <div className="mandateContainer">
            <Trans i18nKey="mandate.recap.intro" firstName={firstName} familyName={familyName} countryName={countryName}>
              <Typography variant="body2">
                <strong>Je soussigné : </strong> {{firstName}} {{familyName}}, résidant en {{countryName}},
              </Typography>
              <Typography  variant="body2">
                <strong>donne pouvoir et mandate : </strong> Misakey SAS, 66 Avenue des Champs-Elysées 75008 PARIS, RCS PARIS 845 272 053
              </Typography>
              <Typography  variant="body2">
                <strong>pour :</strong>
              </Typography>
            </Trans>
            <List>
              <ListItem>
                <ListItemIcon>
                  <Emoji emoji="270d" name="Hand writing" size={32} />
                </ListItemIcon>
                <ListItemText
                  primary={t('mandate.recap.access', 'Demander auprès de toutes organisations l’accès et la portabilité de mes données personnelles')}
                  primaryTypographyProps={{ variant: 'body2' }}
                />
              </ListItem>
              <ListItem>
                <ListItemIcon>
                  <Emoji emoji="1f510" name="Lock" size={32} />
                </ListItemIcon>
                <ListItemText
                  primary={t('mandate.recap.portability', 'Effectuer le transfert de mes données personnelles de manière chiffrée')}
                  primaryTypographyProps={{ variant: 'body2' }}
                />
              </ListItem>
              <ListItem>
                <ListItemIcon>
                  <Emoji emoji="2705" name="Check" size={32} />
                </ListItemIcon>
                <ListItemText
                  primary={t('mandate.recap.delete', 'Faire valoir mes droits de suppression, modification et limitation de mes données personnelles')}
                  primaryTypographyProps={{ variant: 'body2' }}
                />
              </ListItem>
            </List>
            <Typography variant="body2">
              <Trans i18nKey="mandate.recap.email" email={email}>
                <strong>associées à l’email : </strong> {{email}} dont je suis propriétaire.
              </Trans>
            </Typography>
            <div className="loginButtonGroup">
              {modifyButton}
              <Button variant="contained" color="primary" onClick={this.nextStep}>
                {t('mandate.recap.signButton', 'Signer')}
              </Button>
            </div>
          </div>
        );
        break;

      case 'sign_mandate':
        title = t('mandate.signMandateTitle', 'Signer le mandat de mission');
        content = (
          <div className="mandateContainer">
            <Typography>
              {t('mandate.signature.intro', 'Le mandant, {{firstName}} {{familyName}}', { firstName, familyName })}
            </Typography>
            <Typography className="signatureLabel">
              {t('mandate.signature.label', 'e-signature, le {{date}}', { date: nowAsDay() })}
            </Typography>
            <Typography color="textSecondary" className="signaturePlaceholder">
              {t('mandate.signature.tip', 'Dessiner votre signature ici')}
            </Typography>
            <SignatureCanvas
              canvasProps={{ className: 'signatureCanvas' }}
              clearOnResize={false}
              ref={this.signatureCanvas}
            />

            <FormControl fullWidth error={signatureError.length > 0} aria-describedby="signature-error">
              {
                (signatureError.length === 0) ? null : (
                  <FormHelperText id="signature-error">{signatureError}</FormHelperText>
                )
              }
            </FormControl>

            <Button onClick={() => this.signatureCanvas.current.clear()}>
              {t('mandate.signature.restartButton', 'Recommencer')}
            </Button>

            <Typography variant="body2" color="textSecondary">
              {t('mandate.signature.removable', 'Vous pourrez à tout moment retirer votre consentement en accédant à la page Mon compte (app.misakey.com).')}
            </Typography>
            <div className="loginButtonGroup">
              {modifyButton}
              <Button variant="contained" color="primary" onClick={this.nextStep}>
                {t('mandate.signature.acceptButton', 'Bon pour acceptation')}
              </Button>
            </div>
          </div>
        );
        break;

      case 'validate_tos':
        title = t('mandate.tosTitle', 'Règles de confidentialité et conditions d\'utilisation');
        content = (
          <div className="mandateContainer">
            <Trans i18nKey="mandate.tos">
              <Typography>
                Pour créer un compte Misakey, vous devez accepter les <Link href="https://www.misakey.com/#mk-legal-tos" target="_blank" rel="noopener noreferrer">Conditions d'utilisation</Link> ci-dessous.<br />
                De plus, lorsque vous créez un compte, nous traitons vos informations comme décrit dans nos <Link href="https://www.misakey.com/#mk-legal-privacy" target="_blank" rel="noopener noreferrer">Règles de confidentialité</Link>, y compris les points clés suivants :<br />
                <br />
                <strong>Données que nous traitons lorsque vous utilisez Misakey</strong>
              </Typography>
              <ul>
                <li><Typography>Lorsque vous configurez un compte Misakey, nous enregistrons les informations que vous nous fournissez, telles que votre nom, votre adresse e-mail et votre pays de résidence.</Typography></li>
                <li><Typography>Lorsque vous échangez des données personnelles au travers des services Misakey, nous chiffrons les informations de bout en bout afin que nous ne soyons pas en mesure d’y accéder.</Typography></li>
                <li><Typography>Lorsque vous utilisez les services Misakey, nous enregistrons des informations liées à l’usage de nos services, telles que votre IP, votre navigateur et votre appareil.</Typography></li>
              </ul>
              <Typography>
                <strong>Pourquoi nous traitons les données</strong><br />
                Nous traitons ces données conformément à <Link href="https://www.misakey.com/#mk-legal-tos" target="_blank" rel="noopener noreferrer">notre règlement</Link>, notamment aux fins suivantes :
              </Typography>
              <ul>
                <li><Typography>Améliorer la qualité de nos services et en développer de nouveaux</Typography></li>
                <li><Typography>Renforcer la sécurité en vous protégeant contre la fraude et les abus</Typography></li>
                <li><Typography>Effectuer des analyses et des mesures afin de comprendre comment nos services sont utilisés. </Typography></li>
              </ul>
              <Typography>
                <strong>Vous contrôlez vos données</strong>

                Selon les paramètres de votre compte, certaines de ces données peuvent être associées à votre compte Misakey et traitées comme des informations personnelles. Vous pourrez à tout moment ajuster les paramètres ou retirer votre consentement pour l'avenir en accédant à la page Mon compte (app.misakey.com).
              </Typography>
            </Trans>
            <Divider variant="middle" />

            <FormControl component="fieldset">
              <FormGroup>
                <FormControlLabel
                  control={(
                    <Checkbox
                      checked={areTOSChecked}
                      onChange={this.handleCheck('areTOSChecked')}
                      value="tos"
                      color="primary"
                    />
                  )}
                  label={t('mandate.tosValidate', 'J\'accepte les conditions d\'utilisation de Misakey')}
                  color="primary"
                />
                <FormControlLabel
                  control={(
                    <Checkbox
                      checked={isPrivacyChecked}
                      onChange={this.handleCheck('isPrivacyChecked')}
                      value="privacy"
                      color="primary"
                    />
                  )}
                  label={t('mandate.privacyValidate', 'J\'accepte que mes informations soient utilisées tel que décrit ci-dessus et détaillé dans les règles de confidentialité')}
                />
              </FormGroup>
            </FormControl>
            <div className="loginButtonGroup">
              {modifyButton}
              <Button
                variant="contained"
                color="primary"
                disabled={!(isPrivacyChecked && areTOSChecked)}
                onClick={this.nextStep}
              >
                {t('mandate.createAccountButton', 'Créer un compte')}
              </Button>
            </div>
          </div>
        );
        break;

      case 'account_creation':
        title = t('mandate.accountCreation', 'Création du compte en cours');
        content = (
          <CircularProgress
            variant="indeterminate"
            size={40}
            thickness={4}
          />
        );
        break;

      default:
        break;
    }

    return (
      <div className="welcomeContainer" style={{ backgroundColor: theme.palette.vertdeau.main }}>
        <div
          className={`contentContainer${(step === 'acknowledge_mandate' || step === 'validate_tos' || step === 'sign_mandate')
            ? ' contentContainerLarge'
            : ''}`}
        >
          <Paper
            className="idFormContainer"
            elevation={0}
            component="form"
            onSubmit={this.nextStep}
          >
            <img src="/img/logo-typo.png" alt="Misakey Logo" className="loginLogo" />
            <Typography variant="h6">
              {title}
            </Typography>
            {content}
          </Paper>
          {
            (step === 'email_password') ? (
              <Typography variant="body2" align="center" className="infoLinks">
                <Link
                  href={t('common:sideMenu.privacyLink')}
                  color="textSecondary"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {t('common:sideMenu.privacyLabel', 'Règles de confidentialité')}
                </Link>
                {' - '}
                <Link
                  href={t('common:sideMenu.tosLink')}
                  color="textSecondary"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {t('common:sideMenu.tosLabel', 'Condition d’utilisation')}
                </Link>
              </Typography>
            ) : null
          }
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isRegistrationDone: state.users.isRegistrationDone,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    createAccount: accountInformations => dispatch(createAccountAction(accountInformations)),
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(withTheme()(translate('welcome')(withRouter(Register))));
