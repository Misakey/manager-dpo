import React from 'react';

import { Avatar } from '@material-ui/core';

import { stringToRGB } from '../../utils/misc/colorGenerator';

const LetterAvatar = (props) => {
  const { text, className } = props;


  if (text) {
    const avatarColor = stringToRGB(text);

    return (
      <Avatar
        style={{ backgroundColor: `#${avatarColor}` }}
        className={className}
      >
        { text.charAt(0).toUpperCase() }
      </Avatar>
    );
  }
  return null;
};

export default LetterAvatar;
