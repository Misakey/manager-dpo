import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { translate } from 'react-i18next';

import {
  Drawer, Toolbar,
  Divider, List, Typography,
  IconButton, CircularProgress,
} from '@material-ui/core';

import MenuIcon from '@material-ui/icons/Menu';

import ItemsListItem from '../ItemsListItem';

class ItemsList extends React.PureComponent {
  static propTypes = {
    width: PropTypes.string,
    toggleMenu: PropTypes.func.isRequired,
    clickOnLink: PropTypes.func.isRequired,
    style: PropTypes.object,
    itemsList: PropTypes.arrayOf(PropTypes.object),
    variant: PropTypes.string.isRequired,
    t: PropTypes.func.isRequired,
  };

  static defaultProps = {
    width: '100%',
    style: {},
    itemsList: null,
  };

  state = {}

  render() {
    const {
      toggleMenu, clickOnLink, style, width, itemsList, activeItem, variant, t,
    } = this.props;

    let itemsListBlock = (
      <div style={{ width }} className="companiesListLoadingContainer">
        <CircularProgress
          variant="indeterminate"
          size={40}
          thickness={4}
        />
      </div>
    );
    if (itemsList !== null) {
      if (itemsList.length > 0) {
        itemsListBlock = (
          <List style={{ width }} className="companiesListContainer">
            {itemsList.map(item => (
              <ItemsListItem
                isActive={item.uuid == activeItem.uuid}
                key={item.uuid}
                clickOnLink={clickOnLink(item.uuid)}
                item={item}
              />
            ))}
          </List>
        );
      } else {
        itemsListBlock = (
          <div style={{ width }} className="companiesListLoadingContainer">
            <Typography align="center" variant="h6" color="textSecondary">
              {t(`contactList.noConversation.${variant}`)}
            </Typography>
          </div>
        );
      }
    }

    return (
      <Drawer
        variant="permanent"
        anchor="left"
        style={style}
      >
        <Toolbar disableGutters className="itemListToolbar">
          <IconButton color="inherit" aria-label="Menu" onClick={toggleMenu(true)}>
            <MenuIcon />
          </IconButton>
          <img src="/img/logo-typo.png" alt="Misakey" className="companiesListLogo" />
        </Toolbar>
        <Divider />
        {/* <Typography>
          Search field
        </Typography>
        <Divider /> */}
        {itemsListBlock}
      </Drawer>
    );
  }
}


function mapStateToProps(state) {
  return {
    user: state.users.user,
    itemsList: state.dashboard.list,
    activeItem: state.dashboard.active,
    variant: state.dashboard.variant,
  };
}

export default connect(mapStateToProps)(translate('dashboard')(ItemsList));