import React from 'react';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import PropTypes from 'prop-types';

import {
  AppBar, Toolbar, Typography,
  IconButton, Avatar, Paper,
  ListItemText, Button, CircularProgress,
} from '@material-ui/core';

import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import moment from 'moment/min/moment-with-locales';

import ChatDate from '../ChatDate';
import Message from '../Message';
import LetterAvatar from '../LetterAvatar';

import APIClient from '../../utils/apiClient';
import { download } from '../../utils/misc/fileDownload';

class ChatView extends React.PureComponent {
  static propTypes = {
    item: PropTypes.object,
    isFullScreen: PropTypes.bool,
    style: PropTypes.object,
    goBackToList: PropTypes.func.isRequired,
    user: PropTypes.object,
    t: PropTypes.func.isRequired,
    variant: PropTypes.string.isRequired,
  };

  static defaultProps = {
    item: {},
    isFullScreen: false,
    style: {},
    user: {},
  };

  state = {
    isMandateDownloading: false,
  }

  constructor(props) {
    super(props);
    this.downloadMandate = this.downloadMandate.bind(this);
  }

  downloadMandate() {
    const { item, user, variant } = this.props;

    this.setState({
      isMandateDownloading: true,
    }, () => {
      let uuid;
      if (variant === 'dpo') {
        uuid = item.uuid;
      } else {
        uuid = user.uuid;
      }
      APIClient.download(`/users/${uuid}/mandate`, true).then((blob) => {
        download(blob.body, 'Mandat.pdf', 'application/pdf');
        setTimeout(() => {
          this.setState({
            isMandateDownloading: false,
          });
        }, 1000);
      });
    });
  }

  render() {
    const {
      style, item, goBackToList, isFullScreen, user, t, variant, reload,
    } = this.props;

    const { isMandateDownloading } = this.state;

    if (Object.entries(item).length === 0 && item.constructor === Object) {
      return (
        <div style={style} className="emptyItem">
          <Typography align="center" variant="h6" color="textSecondary">
            {t(`chat.selectMessage.${variant}`)}
          </Typography>
        </div>
      );
    }

    let arrowBackButton;
    if (isFullScreen) {
      arrowBackButton = (
        <IconButton color="inherit" aria-label="Back" onClick={goBackToList}>
          <ArrowBackIcon />
        </IconButton>
      );
    }

    let lastDate = moment(0);

    let email = (variant === 'dpo') ? item.email : user.email;

    let topAvatar;
    let name;
    let bottomAvatar;
    if (variant === 'dpo') {
      topAvatar = (<LetterAvatar text={item.fullName} />);
      name = (
        <ListItemText
          primary={item.fullName}
          secondary={item.email}
        />
      );
      bottomAvatar = (
        <Avatar
          alt={user.company.name}
          src={user.company.logo}
          style={{ backgroundColor: 'white' }}
          className="tooltipAvatar"
        />);
    }
    if (variant === 'user') {
      topAvatar = (<Avatar alt={item.name} src={item.logo} style={{ backgroundColor: 'white' }} />);
      name = (
        <ListItemText
          primary={item.name}
        />
      );
      bottomAvatar = (<LetterAvatar text={user.email} className="tooltipAvatar" />);
    }

    let downloadMandateBlock = null
    if (variant === 'dpo') {
      downloadMandateBlock = (isMandateDownloading) ? (
        <CircularProgress
          variant="indeterminate"
          size={20}
          thickness={2}
        />
      ) : (
        <Button onClick={this.downloadMandate}>
          Télécharger le  mandat
        </Button>
      )
    }

    return (
      <div style={style}>
        <AppBar
          style={style}
          position="fixed"
          color="inherit"
          elevation={0}
        >
          <Toolbar>
            {arrowBackButton}
            {topAvatar}
            {name}
            {downloadMandateBlock}
          </Toolbar>
        </AppBar>
        <main className="contentAfterAppbar itemDetails">
          <div className="container">
            <div className="dialogFlow">
              {
                item.messages.map((message, index) => {
                  const currentDate = moment.unix(message.date);
                  const messageElement = (
                    <Message
                      message={message}
                      isLastOne={index === (item.messages.length - 1)}
                      variant={variant}
                      email={email}
                      reload={reload}
                    />
                  );

                  const isSameDate = currentDate.isSame(lastDate, 'd');
                  if (
                    !isSameDate
                    && (
                      variant !== 'dpo'
                      || !(
                        message.message_type === 'accessRequestAsked'
                        || message.message_type === 'portabilityRequestAsked'
                        || message.message_type === 'deleteRequestAsked'
                      )
                    )
                  ) {
                    lastDate = currentDate;
                    return (
                      <div key={message.date}>
                        <ChatDate timestamp={message.date} />
                        {messageElement}
                      </div>
                    );
                  }
                  return (
                    <div key={message.date}>
                      {messageElement}
                    </div>
                  );
                })
              }
            </div>

            {
              (item.company_status === 'static') ? null : (
                <div className="informationMessageContainer">
                  <div className="informationMessage">
                    {bottomAvatar}
                    <Paper
                      className="tooltipMessage"
                      elevation={0}
                    >
                      <Typography component="p">
                        {t(`chat.tooltip.${variant}.${item.status}`)}
                      </Typography>
                    </Paper>
                  </div>
                </div>
              )
            }
          </div>
        </main>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.users.user,
    variant: state.dashboard.variant,
    item: state.dashboard.active,
  };
}

export default connect(mapStateToProps)(translate('dashboard')(ChatView));
