import React from 'react';
import { translate } from 'react-i18next';
import { connect } from 'react-redux';
import { withTheme } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import {
  Avatar, ListItem, ListItemText, ListItemAvatar,
  Typography, ListItemSecondaryAction,
} from '@material-ui/core';

import DoneIcon from '@material-ui/icons/Done';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import CloseIcon from '@material-ui/icons/Close';

import { timestampToDigestDate } from '../../utils/misc/dateFormat';
import LetterAvatar from '../LetterAvatar';

class ItemsListItem extends React.PureComponent {
  static propTypes = {
    clickOnLink: PropTypes.func.isRequired,
    item: PropTypes.object.isRequired,
    t: PropTypes.func.isRequired,
    variant: PropTypes.string.isRequired,
  };

  state = {}

  render() {
    const {
      clickOnLink, item, t, variant, theme, isActive,
    } = this.props;

    let name;
    let avatar;

    if (variant === 'dpo') {
      name = item.fullName;
      avatar = (<LetterAvatar text={item.fullName} />);
    } else {
      name = item.name;
      avatar = (<Avatar alt={item.name} src={item.logo} />);
    }

    let icon;
    if (item.status === 'read') {
      icon = (
        <DoneIcon
          className="statusIcon"
          style={{ backgroundColor: theme.palette.secondary.main }}
        />
      );
    } else if (item.status === 'data') {
      icon = (
        <DoneAllIcon
          className="statusIcon"
          style={{ backgroundColor: theme.palette.primary.main }}
        />
      );
    } else if (item.status === 'no_data') {
      icon = (
        <CloseIcon
          className="statusIcon"
          style={{ backgroundColor: theme.palette.grey[500] }}
        />
      );
    }

    let lastMessageText;
    if (item.last_message && item.last_message.message_type === 'custom_message') {
      lastMessageText = item.last_message.complementary_informations;
    } else if (item.last_message) {
      lastMessageText = t(`chat.messages.${item.last_message.message_type}`).replace(/(<([^>]+)>)/ig, '');
    }

    return (
      <ListItem button onClick={clickOnLink} selected={isActive}>
        <ListItemAvatar>
          {avatar}
        </ListItemAvatar>
        {icon}
        <ListItemText
          primary={name}
          secondary={lastMessageText}
          primaryTypographyProps={{ noWrap: true }}
          secondaryTypographyProps={{ noWrap: true }}
        />
        <div className="itemsListItemDate">
          <Typography color="textSecondary">
            {timestampToDigestDate(item.last_interaction)}
          </Typography>
        </div>
      </ListItem>
    );
  }
}


function mapStateToProps(state) {
  return {
    variant: state.dashboard.variant,
  };
}

export default connect(mapStateToProps)(withTheme()(translate('dashboard')(ItemsListItem)));
