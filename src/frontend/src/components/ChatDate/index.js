import React from 'react';

import { Typography } from '@material-ui/core';

import { timestampToChatDate } from '../../utils/misc/dateFormat';

const ChatDate = (props) => {
  const { timestamp, text } = props;

  if (timestamp) {
    return (
      <Typography align="center" color="textSecondary" className="date">
        {timestampToChatDate(timestamp)}
      </Typography>
    );
  }
  if (text) {
    return (
      <Typography align="center" color="textSecondary" className="date">
        {text}
      </Typography>
    );
  }
  return null;
};

export default ChatDate;
