import { Component } from 'react';
import PropTypes from 'prop-types';


class BaseComponent extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.goTo = this.goTo.bind(this);
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this)
  }

  componentDidMount() {
    const { renewSession } = this.props.auth;

    if (localStorage.getItem(`${process.env.REACT_APP_FRONTEND_TYPE}_isLoggedIn`) === 'true') {
      renewSession();
    }
  }

  goTo(route) {
    this.props.history.replace(`${route}`);
  }

  login() {
    this.props.auth.login();
  }

  logout(uuid) {
    this.props.auth.logout(uuid);
  }
}

export default BaseComponent;
