import React from 'react';

import { Paper, LinearProgress, Typography } from '@material-ui/core';

const ProgressMessage = (props) => {
  const progressValue = 100 * props.currentValue / props.total;

  return (
    <Paper
      className="message"
      elevation={0}
    >
      {props.children}
      <LinearProgress variant="determinate" value={progressValue} />
      <Typography align="right">
        {props.dateText}
      </Typography>
    </Paper>
  );
};

export default ProgressMessage;