import React from 'react';
import { translate, Trans } from 'react-i18next';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { withTheme } from '@material-ui/core/styles';

import {
  Paper, LinearProgress, Typography, Button,
  CircularProgress,
} from '@material-ui/core';

import { timestampToHour, getDaysToAnswerAndProgressValue } from '../../utils/misc/dateFormat';
import APIClient from '../../utils/apiClient';


class Message extends React.PureComponent {
  state = {
    isOngoingAction: false,
  }

  sendMessage(messageType) {
    const { reload, message } = this.props;
    this.setState({
      isOngoingAction: true,
    }, () => {
      console.log(messageType);
      // Send information to the backend
      // On done, update message list

      const newMessage = {
        message_type: messageType,
        answering_to: message.uuid,
      };

      APIClient.post('/messages', newMessage, true).then((response) => {
        if (response.httpStatus === 200) {
          reload();
          this.setState({
            isOngoingAction: false,
          });
        } else {
          console.log("ERROR");
          console.log(response);
        }
      });
    });
  }

  render() {
    const {
      message, isLastOne, theme, t, variant, email,
    } = this.props;
    const { isOngoingAction } = this.state;
    const messageType = message.message_type;

    let messageContent;
    let actions;
    let isFromUser = true;
    let withProgressBar = false;


    const { daysToAnswer, progressValue } = getDaysToAnswerAndProgressValue(message.date);

    if (messageType === 'accessRequestAsked' && variant === 'user') {
      messageContent = t('chat.messages.accessRequestAsked', '[Misakey] Nous avons pris en compte cette demande mais ne l\'avons pas encore transmise à l\'entreprise. Ça ne saurait tarder');
    }

    if (messageType === 'accessRequestSent') {
      if (isLastOne && variant === 'dpo') {
        actions = (
          <div className="chatActionGroup">
            <Button
              variant="contained"
              className="chatActionButton"
              onClick={() => this.sendMessage('accessAcceptNoData')}
            >
              {t('chat.buttons.no', 'Non')}
            </Button>
            <Button
              variant="contained"
              color="primary"
              className="chatActionButton"
              onClick={() => this.sendMessage('accessAcceptData')}
            >
              {t('chat.buttons.yes', 'Oui')}
            </Button>
          </div>
        );
      }

      messageContent = t('chat.messages.accessRequestSent', 'Je souhaites savoir si vous disposez de données personnelles associées à mon email : {{email}} parmi les données dont vous êtes responsable du traitement.', { email });

      if (isLastOne) {
        withProgressBar = true;
      }
    }

    if (messageType === 'accessAcceptData') {
      if (isLastOne && variant === 'user') { // TODO: Remove the false when enabling user responses
        actions = (
          <div className="chatActionGroup">
            <Button
              variant="contained"
              className="chatActionButton"
              onClick={() => this.sendMessage('deleteRequestAsked')}
            >
              {t('chat.buttons.delete', 'Supprimer ces données')}
            </Button>
            <Button
              variant="contained"
              color="primary"
              className="chatActionButton"
              onClick={() => this.sendMessage('portabilityRequestAsked')}
            >
              {t('chat.buttons.portability', 'Récupérer mes données')}
            </Button>
          </div>
        );
      }

      isFromUser = false;

      messageContent = t('chat.message.accessAcceptData', 'Nous avons des données liées à l’email {{email}}', { email });
    }

    if (messageType === 'accessAcceptNoData') {
      messageContent = t('chat.message.accessAcceptNoData', 'Nous n’avons pas de données concernant l’email {{email}}', { email });

      isFromUser = false;
    }

    if (messageType === 'portabilityRequestAsked') {
      messageContent = t('chat.messages.portabilityRequestAsked', 'Demander à récuperer mes données');
    }

    if (messageType === 'portabilityRequestSent') {
      messageContent = t('chat.messages.portabilityRequestSent', 'Votre demande de portabilité  est en cours. Le délai légal maximum est de 30 jours. Il peut être prolongé dans les cas complexes et vous devrez être informé.');
      if (isLastOne) {
        withProgressBar = true;
      }
    }

    if (messageType === 'portabilityAccept') {
      if (isLastOne && variant === 'user') {
        actions = (
          <div className="chatActionGroup">
            <Button
              variant="contained"
              onClick={() => this.sendMessage('deleteRequestAsked')}
            >
              {t('chat.buttons.delete', 'Supprimer ces données')}
            </Button>
            <Button
              variant="contained"
              color="primary"
            >
              {t('chat.buttons.show', 'Voir mes données')}
            </Button>
          </div>
        );
      }

      messageContent = t('portabilityAccept.message', 'Le transfert de vos données est terminé. Vous pouvez les consulter, les mettre à jour ou demander leur suppression.');
    }

    if (messageType === 'welcomeMessage') {
      messageContent = t('chat.messages.welcomeMessage', 'Hello ! Bienvenue sur ton compte Misakey, nous on a que les données de ton mandat (nom, prénom, email), les boites avec qui on te met en relation, et les typologies de messages que vous vous envoyez (demande d acces, ...). Les données qu ils te ramènent te restent privés !');
    }

    if (isOngoingAction) {
      actions = (
        <div className="chatActionGroup">
          <CircularProgress
            variant="indeterminate"
            size={25}
            thickness={2}
          />
        </div>
      );
    }

    if (messageContent) {
      let style;
      if ((variant === 'dpo' && isFromUser) || (variant === 'user' && !isFromUser)) {
        style = { backgroundColor: theme.palette.vertdeau.main };
      }
      return (
        <div>
          <div className="messageContainer">
            <Paper
              className="message"
              elevation={0}
              style={style}
            >
              <Typography inline>
                {messageContent}
              </Typography>
              {
                (withProgressBar) ? (
                  <div>
                    <Trans i18nKey="chat.messages.overall.remainingTime" daysToAnswer={daysToAnswer}>
                      <Typography inline>J’espère obtenir une réponse dans moins de</Typography>
                      <Typography color="primary" inline> {{daysToAnswer}} jours.</Typography>
                    </Trans>
                    <LinearProgress className="messageProgressBar" variant="determinate" value={progressValue} />
                  </div>
                ) : null
              }
              <Typography align="right" className="messageDate">
                { timestampToHour(message.date) }
              </Typography>
              <div className="clear" />
            </Paper>
          </div>
          {actions}
        </div>
      );
    }

    return null;

    /*
      accessRequestAsked
      accessRequestSent
      accessAcceptData
      accessAcceptNoData
      accessRefusal

      portabilityRequestAsked
      portabilityRequestSent
      portabilityAccept
      portabilityRefusal

      deleteRequestAsked
      deleteRequestSent
      deleteAccept
      deleteRefusal
    */
  }
}

Message.propTypes = {
  message: PropTypes.object.isRequired,
  isLastOne: PropTypes.bool,
  t: PropTypes.func.isRequired,
  theme: PropTypes.object.isRequired,
  variant: PropTypes.string.isRequired,
};

Message.defaultProps = {
  isLastOne: false,
};


function mapStateToProps(state) {
  return {
    variant: state.dashboard.variant,
  };
}

export default connect(mapStateToProps)(translate('dashboard')(withTheme()(Message)));
