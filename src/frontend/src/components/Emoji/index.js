import React from 'react';

import PropTypes from 'prop-types';


class Emoji extends React.PureComponent {
  static propTypes = {
    size: PropTypes.number,
    lib: PropTypes.string,
    libVersion: PropTypes.string,
    emoji: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  };

  static defaultProps = {
    size: 32,
    lib: 'EmojiOne',
    libVersion: '4.0',
  };

  render() {
    const {
      size, lib, libVersion, emoji, name, inline, ...other
    } = this.props;

    let sizeSubFolder = '';
    let emojiFile = '';

    let fileSize = size;
    if (size > 64) {
      fileSize = 128
    } else if (size > 32) {
      fileSize = 64
    } else {
      fileSize = 32
    }

    if (lib === 'EmojiOne') {
      sizeSubFolder = `${fileSize}x${fileSize}_png`;
      emojiFile = `${emoji}.png`;
    }

    const style = {}

    return (
      <img {...other} src={`https://static.misakey.com/img/${lib}/${libVersion}/${sizeSubFolder}/${emojiFile}`} alt={`Emoji - ${name}`} height={size} width={size} style={style} />
    );
  }
}


export default Emoji;
