import React from 'react';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { withRouter } from 'react-router-dom';

import {
  Drawer, List, ListItem, ListItemIcon, ListItemText,
  Divider, Typography, Link, Avatar,
} from '@material-ui/core';

import {
  FormatListNumbered as FormatListNumberedIcon,
  Delete as DeleteIcon, PowerSettingsNew as PowerSettingsNewIcon,
  Schedule as ScheduleIcon, Settings as SettingsIcon,
  /* Home as HomeIcon, ShoppingCart as ShoppingCartIcon,
  LibraryBooks as LibraryBooksIcon, AddCircle as AddCircleIcon,
  Favorite as FavoriteIcon, Laptop as LaptopIcon,
  VideogameAsset as VideogameAssetIcon, People as PeopleIcon, */
} from '@material-ui/icons';

import BaseComponent from '../_BaseComponent';
import LetterAvatar from '../LetterAvatar';

import DeleteAccountModal from '../DeleteAccountModal';

class SideMenu extends BaseComponent {
  state = {
    isDeleteAccountModalOpen: false,
  }

  constructor(props) {
    super(props);
    this.toggleDeleteAccountModal = this.toggleDeleteAccountModal.bind(this);
  }

  toggleDeleteAccountModal = isOpen => () => {
    this.setState({
      isDeleteAccountModalOpen: isOpen,
    });
  };

  render() {
    const {
      isDrawerOpen, onClose, user, t, variant,
    } = this.props;
    const { isDeleteAccountModalOpen } = this.state;
    const email = user.email;

    let avatar = (<LetterAvatar text={email} />);

    if (variant === 'dpo') {
      avatar = (<Avatar alt={user.company.name} src={user.company.logo} style={{ backgroundColor: 'white' }} />);
    }

    // Default actions, during onboarding
    let scopeActions = (
      <div>
        <ListItem button selected>
          <ListItemIcon>
            <FormatListNumberedIcon />
          </ListItemIcon>
          <ListItemText primary={t('sideMenu.register', 'Inscription')} />
        </ListItem>
        <ListItem button onClick={this.toggleDeleteAccountModal(true)}>
          <ListItemIcon>
            <DeleteIcon />
          </ListItemIcon>
          <ListItemText primary={t('sideMenu.deleteAccount', 'Supprimer mon compte')} />
        </ListItem>
        <ListItem button onClick={() => this.logout(user.uuid)}>
          <ListItemIcon>
            <PowerSettingsNewIcon />
          </ListItemIcon>
          <ListItemText primary={t('sideMenu.logout', 'Déconnexion')} />
        </ListItem>
      </div>
    );

    if (user.onboarding_status === 'completed') {
      scopeActions = (
        <div>
          <ListItem button selected>
            <ListItemIcon>
              <ScheduleIcon />
            </ListItemIcon>
            <ListItemText primary={t('sideMenu.recent', 'Récent')} />
          </ListItem>
          { /* <ListItem button>
            <ListItemIcon>
              <HomeIcon />
            </ListItemIcon>
            <ListItemText primary={t('sideMenu.home', 'Maison')} />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <ShoppingCartIcon />
            </ListItemIcon>
            <ListItemText primary={t('sideMenu.consumption', 'Consommation')} />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <LibraryBooksIcon />
            </ListItemIcon>
            <ListItemText primary={t('sideMenu.media', 'Média')} />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <FavoriteIcon />
            </ListItemIcon>
            <ListItemText primary={t('sideMenu.me', 'Moi')} />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <LaptopIcon />
            </ListItemIcon>
            <ListItemText primary={t('sideMenu.utils', 'Utilitaire')} />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <VideogameAssetIcon />
            </ListItemIcon>
            <ListItemText primary={t('sideMenu.games', 'Jeux')} />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <PeopleIcon />
            </ListItemIcon>
            <ListItemText primary={t('sideMenu.civic', 'Collectivité')} />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <AddCircleIcon />
            </ListItemIcon>
            <ListItemText primary={t('sideMenu.misc', 'Autres')} />
          </ListItem> */}


          <Divider className="menuDivider" />

          <ListItem button>
            <ListItemIcon>
              <SettingsIcon />
            </ListItemIcon>
            <ListItemText primary={t('sideMenu.settings', 'Paramètres')} />
          </ListItem>
          <ListItem button onClick={this.toggleDeleteAccountModal(true)}>
            <ListItemIcon>
              <DeleteIcon />
            </ListItemIcon>
            <ListItemText primary={t('sideMenu.deleteAccount', 'Supprimer mon compte')} />
          </ListItem>
          <ListItem button onClick={() => this.logout(user.uuid)}>
            <ListItemIcon>
              <PowerSettingsNewIcon />
            </ListItemIcon>
            <ListItemText primary={t('sideMenu.logout', 'Déconnexion')} />
          </ListItem>
        </div>
      );
    }

    if (variant === 'dpo') {
      scopeActions = (
        <div>
          <ListItem button selected>
            <ListItemIcon>
              <ScheduleIcon />
            </ListItemIcon>
            <ListItemText primary={t('sideMenu.ongoing', 'En cours')} />
          </ListItem>

          <Divider className="menuDivider" />

          <ListItem button>
            <ListItemIcon>
              <SettingsIcon />
            </ListItemIcon>
            <ListItemText primary={t('sideMenu.settings', 'Paramètres')} />
          </ListItem>
          <ListItem button onClick={() => this.logout(user.uuid)}>
            <ListItemIcon>
              <PowerSettingsNewIcon />
            </ListItemIcon>
            <ListItemText primary={t('sideMenu.logout', 'Déconnexion')} />
          </ListItem>
        </div>
      );
    }

    return (
      <div>
        <Drawer open={isDrawerOpen} onClose={onClose}>
          <div className="menuContainer">
            <List className="menuList">
              <ListItem>
                {avatar}
                <ListItemText primary={email} />
              </ListItem>

              <Divider className="menuDivider" />

              {scopeActions}
            </List>
            <div className="menuFooter">
              <Typography variant="body2" align="center">
                <Link
                  href={t('sideMenu.privacyLink', 'https://www.misakey.com/#mk-legal-privacy')}
                  color="textSecondary"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {t('sideMenu.privacyLabel', 'Règles de confidentialité')}
                </Link>
                {' - '}
                <Link
                  href={t('sideMenu.tosLink', 'https://www.misakey.com/#mk-legal-privacy')}
                  color="textSecondary"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {t('sideMenu.tosLabel', 'Condition d’utilisation')}
                </Link>
              </Typography>
            </div>
          </div>
        </Drawer>
        <DeleteAccountModal
          isOpen={isDeleteAccountModalOpen}
          toggleModal={this.toggleDeleteAccountModal}
          email={email}
          logout={() => this.logout(user.uuid)} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    variant: state.dashboard.variant,
    user: state.users.user,
  };
}

export default withRouter(connect(mapStateToProps)(translate('common')(SideMenu)));
