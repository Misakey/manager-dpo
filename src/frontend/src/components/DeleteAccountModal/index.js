import React from 'react';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';

import PropTypes from 'prop-types';

import {
  Typography, Modal, Paper, Avatar, ListItem, ListItemText,
  FormControl, TextField, Button,
} from '@material-ui/core';

import { stringToRGB } from '../../utils/misc/colorGenerator';

import { deleteAccountAction } from '../../store/users/actions';


class DeleteAccountModal extends React.PureComponent {
  static propTypes = {
    isOpen: PropTypes.bool.isRequired,
    toggleModal: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    deleteAccount: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired,
  };

  static defaultProps = {
  };

  state = {
    deleteConfirmation: '',
    isDeleted: false,
  }

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.deleteAccount = this.deleteAccount.bind(this);
  }

  handleChange = name => (event) => {
    this.setState({
      [name]: event.target.value,
    });
  };

  deleteAccount() {
    const { deleteAccount, user } = this.props;
    deleteAccount(user.auth0_id).then(() => {
      this.setState({
        isDeleted: true,
      });
    });
  }

  render() {
    const { isOpen, toggleModal, user, t, logout } = this.props;
    const { deleteConfirmation, isDeleted } = this.state;
    const email = user.email;

    const avatarColor = stringToRGB(email);

    if (isDeleted) {
      return (
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open
          className="modalContainer"
        >
          <Paper elevation={1} className="deleteAccountModal">
            <Typography align="center" variant="h6">
              {t('deleteAccount.confirmMessage', 'Votre compte Misakey à été supprimé')}
            </Typography>
            <br />
            <div className="buttonBar">
              <div/>
              <Button onClick={logout} variant="contained" color="primary">OK</Button>
            </div>
          </Paper>
        </Modal>
      );
    }

    return (
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={isOpen}
        onClose={toggleModal(false)}
        className="modalContainer"
      >
        <Paper elevation={1} className="deleteAccountModal">
          <Typography>
            {t('deleteAccount.deleteMessage', 'Pour confirmer la suppression de votre compte Misakey.')}
          </Typography>
          <ListItem>
            <Avatar style={{ backgroundColor: `#${avatarColor}` }}>
              { email.charAt(0).toUpperCase() }
            </Avatar>
            <ListItemText primary={email} />
          </ListItem>
          <Typography>
            {t('deleteAccount.actionMessage', 'Veuillez taper "DELETE" et valider')}
          </Typography>

          <FormControl margin="normal" fullWidth>
            <TextField
              required
              value={deleteConfirmation}
              label="Confirmation"
              variant="outlined"
              onChange={this.handleChange('deleteConfirmation')}
            />
          </FormControl>
          <div className="buttonBar">
            <Button onClick={toggleModal(false)}>{t('deleteAccount.cancelButton', 'Annuler')}</Button>
            <Button onClick={this.deleteAccount} variant="contained" color="primary" disabled={deleteConfirmation !== 'DELETE'}>
              {t('deleteAccount.validateButton', 'Valider')}
            </Button>
          </div>
        </Paper>
      </Modal>
    );
  }
}


function mapStateToProps(state) {
  return {
    user: state.users.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    deleteAccount: (auth0Id) => dispatch(deleteAccountAction(auth0Id)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(translate('common')(DeleteAccountModal));
