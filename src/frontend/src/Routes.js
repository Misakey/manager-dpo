import React, { Component, Suspense, lazy } from 'react';
import {
  Router, // BrowserRouter ??
  Route,
  Switch,
} from 'react-router-dom';
import Auth from './utils/auth/Auth';
import history from './history';

import LoadingScreen from './screens/Loading';

import store from './store/store';

// Screen laey imports (for code splitting)
const WelcomeScreen = lazy(() => import('./screens/Welcome'));
const DpoDashboardScreen = lazy(() => import('./screens/Dashboard/DpoDashboard'));
const UserDashboardScreen = lazy(() => import('./screens/Dashboard/UserDashboard'));
const OnboardingScreen = lazy(() => import('./screens/Onboarding'));
const UnlockSealScreen = lazy(() => import('./screens/UnlockSeal'));


// ROUTES DEFINITION
const DASHBOARD_ROUTE = '/';
const ONBOARDING_ROUTE = '/onboarding';
const REGISTER_ROUTE = '/signup';
const REGISTER_DPO_ROUTE = '/dpo/:linkKey';
const LOGIN_ROUTE = '/login';
const LOGOUT_ROUTE = '/logout';
const UNLOCK_ROUTE = '/unlock';

export const ROUTES = {
  DASHBOARD_ROUTE,
  ONBOARDING_ROUTE,
  REGISTER_ROUTE,
  REGISTER_DPO_ROUTE,
  LOGIN_ROUTE,
  LOGOUT_ROUTE,
  UNLOCK_ROUTE,
};


const auth = new Auth();

// Handle Authentication for the callback URL
const handleAuthentication = ({ location }) => {
  if (/access_token|id_token|error/.test(location.hash)) {
    auth.handleAuthentication();
  }
};

// HOC to secure a screen with login
const withLogin = (screenComponentToRender) => {
  if (auth.isAuthenticated()) {
    return screenComponentToRender;
  }
  if (localStorage.getItem(`${process.env.REACT_APP_FRONTEND_TYPE}_isLoggedIn`) !== 'true') {
    history.push(LOGIN_ROUTE);
  }
  return null;
};

const withPrivKey = (screenComponentToRender) => {
  const user = store.getState().users.user;
  if (user && user.uuid) {
    const encryptedPrivkey = localStorage.getItem(`encrypted_box_privkey_${user.uuid}`);
    if (encryptedPrivkey) {
      return screenComponentToRender;
    }
  }
  history.push(UNLOCK_ROUTE);

  return null;
};


class Routes extends Component {
  state = {
    isAuthProcessDone: false,
  }

  // Onmount, reload auth0 session
  componentDidMount() {
    if (localStorage.getItem(`${process.env.REACT_APP_FRONTEND_TYPE}_isLoggedIn`) === 'true') {
      auth.renewSession(() => {
        this.setState({
          isAuthProcessDone: true,
        });
      });
    }
    // To be sure the renew process is finish before rendering router
    // It's a fix for the HOC (if not, it's not considered as loggedin)
    this.setState({
      isAuthProcessDone: true,
    });
  }

  render() {
    const { isAuthProcessDone } = this.state;

    const frontendType = process.env.REACT_APP_FRONTEND_TYPE;

    if (isAuthProcessDone && frontendType === 'user') {
      return (
        <Router history={history}>
          <Suspense fallback={<LoadingScreen />}>
            <Switch>
              <Route exact path={DASHBOARD_ROUTE} render={props => withLogin(<UserDashboardScreen auth={auth} {...props} />)} />
              <Route exact path={ONBOARDING_ROUTE} render={props => withLogin(<OnboardingScreen auth={auth} {...props} />)} />
              <Route exact path={REGISTER_ROUTE} render={props => <WelcomeScreen auth={auth} {...props} />} />
              <Route exact path={LOGIN_ROUTE} render={props => <WelcomeScreen auth={auth} isLogin {...props} />} />
              <Route exact path={UNLOCK_ROUTE} render={props => withLogin(<UnlockSealScreen auth={auth} {...props} />)} />
              <Route
                path="/callback"
                render={(props) => {
                  handleAuthentication(props);
                  return <LoadingScreen {...props} />;
                }}
              />
            </Switch>
          </Suspense>
        </Router>
      );
    }
    if (isAuthProcessDone && frontendType === 'dpo') {
      return (
        <Router history={history}>
          <Suspense fallback={<LoadingScreen />}>
            <Switch>
              <Route exact path={DASHBOARD_ROUTE} render={props => withLogin(<DpoDashboardScreen auth={auth} {...props} />)} />
              <Route exact path={REGISTER_DPO_ROUTE} render={props => <WelcomeScreen auth={auth} dpo {...props} />} />
              <Route exact path={LOGIN_ROUTE} render={props => <WelcomeScreen auth={auth} isLogin dpo {...props} />} />
              <Route
                path="/callback"
                render={(props) => {
                  handleAuthentication(props);
                  return <LoadingScreen {...props} />;
                }}
              />
            </Switch>
          </Suspense>
        </Router>
      );
    }
    return <LoadingScreen />;
  }
}

export default Routes;
