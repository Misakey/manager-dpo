import i18n from 'i18next';

import LanguageDetector from 'i18next-browser-languagedetector';
import { reactI18nextModule } from 'react-i18next';
import XHR from 'i18next-xhr-backend';

const defaultLanguage = 'fr';

const missingKeys = [];

i18n
  .use(XHR)
  .use(LanguageDetector)
  .use(reactI18nextModule) // if not using I18nextProvider
  .init({
    debug: false,

    interpolation: {
      escapeValue: false, // not needed for react!!
    },

    // When a translation is missing, it's written in the console
    saveMissing: true,
    missingKeyHandler: (lng, ns, key, fallbackValue) => {
      lng.forEach((lang) => {
        const keyIdentifier = `${lng}-${ns}--${key}`;
        if (missingKeys.indexOf(keyIdentifier) === -1) {
          const filename = `/locales/${lang}/${ns}.json`;
          console.log(`[I18N] Edit ${filename}, to add ${key}: "${fallbackValue}"`);
          missingKeys.push(keyIdentifier);
        }
      });
    },

    fallbackLng: defaultLanguage,
    lng: defaultLanguage, // 'fr', // language to use
    backend: {
      loadPath: '/locales/{{lng}}/{{ns}}.json',
    },

    // react i18next special options (optional)
    react: {
      wait: false,
      bindI18n: 'languageChanged loaded',
      bindStore: 'added removed',
      nsMode: 'default',
      omitBoundRerender: false,
    },
  });

export default i18n;
