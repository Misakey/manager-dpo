import { initialState as welcomeInitialState } from './welcome/reducers'
 
export function clearState(completeState) {
    let clearedState = completeState;
    if (clearedState && clearedState.welcome) {
        clearedState.welcome = welcomeInitialState;
    }
    return clearedState
}