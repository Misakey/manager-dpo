import {
  GET_COMPANIES_LIST_SUCCESS,
  GET_COMPANIES_LIST_ERROR,

  GET_USERS_LIST_SUCCESS,
  GET_USERS_LIST_ERROR,

  UNSELECT_ITEM,
  GET_ITEM_SUCCESS,
  GET_ITEM_ERROR,
} from './types';
import { CALL_API } from '../../_middlewares/api';

export const updateCompaniesListAction = (uuid) => (dispatch) => {
  dispatch({
    [CALL_API]: {
      endpoint: `/users/${uuid}/companies`,
      shouldAuth: true,
      types: [GET_COMPANIES_LIST_SUCCESS, GET_COMPANIES_LIST_ERROR],
    },
  });
};

export const updateUsersListAction = (uuid) => (dispatch) => {
  dispatch({
    [CALL_API]: {
      endpoint: `/companies/${uuid}/users`,
      shouldAuth: true,
      types: [GET_USERS_LIST_SUCCESS, GET_USERS_LIST_ERROR],
    },
  });
};

export const fetchItemAction = (variant, uuid) => (dispatch) => {
  switch (variant) {
    case 'user':
      dispatch({
        [CALL_API]: {
          endpoint: `/companies/${uuid}`,
          shouldAuth: true,
          types: [GET_ITEM_SUCCESS, GET_ITEM_ERROR],
        },
      });
      break;

    case 'dpo':
      dispatch({
        [CALL_API]: {
          endpoint: `/users/${uuid}`,
          shouldAuth: true,
          types: [GET_ITEM_SUCCESS, GET_ITEM_ERROR],
        },
      });
      break;

    default:
      break;
  }
};

export const unselectItemAction = () => (dispatch) => {
  dispatch({
    type: UNSELECT_ITEM,
  });
};
