import {
  GET_COMPANIES_LIST_SUCCESS,
  GET_COMPANIES_LIST_ERROR,
  GET_USERS_LIST_SUCCESS,
  GET_USERS_LIST_ERROR,
  UNSELECT_ITEM,
  GET_ITEM_SUCCESS,
} from '../actions/types';

const initialState = {
  list: null,
  active: {},
  variant: 'none',
};

// TODO: Manager the ERROR cases, with an error managment system (maybe alert the user, tell to reload, ... ?)
export default function reducer(state = initialState, action) {
  switch (action.type) {
    case GET_COMPANIES_LIST_SUCCESS:
      return { ...state, list: action.response.body, variant: 'user' };

    case GET_COMPANIES_LIST_ERROR:
      return { ...state, variant: 'user' };

    case GET_USERS_LIST_ERROR:
      return { ...state, variant: 'dpo' };

    case GET_USERS_LIST_SUCCESS:
      return { ...state, list: action.response.body, variant: 'dpo' };

    case GET_ITEM_SUCCESS:
      return { ...state, active: action.response.body };

    case UNSELECT_ITEM:
      return { ...state, active: {} };

    default:
      return state;
  }
}
