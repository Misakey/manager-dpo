import { combineReducers } from 'redux';
import users from './users/reducers';
import welcome from './welcome/reducers';
import dashboard from './dashboard/reducers';

const appReducer = combineReducers({
  users,
  welcome,
  dashboard,
});

const rootReducer = (state, action) => {
  let newState = state;
  if (action.type === 'LOGOUT') {
    newState = undefined;
  }

  return appReducer(newState, action);
};

export default rootReducer;

// export default combineReducers({
//  users,
//  welcome,
//  dashboard,
// });
