import APIClient from '../../utils/apiClient';

export const CALL_API = Symbol('Call API');

export default () => next => (action) => {
  const callAPI = action[CALL_API];

  // So the middleware doesn't get applied to every single action
  if (typeof callAPI === 'undefined') {
    return next(action);
  }

  const { endpoint, types, shouldAuth } = callAPI;

  const [successType, errorType] = types;

  let responsePromise;
  if (callAPI.method && callAPI.method === 'POST') {
    responsePromise = APIClient.post(endpoint, callAPI.payload, shouldAuth);
  } else if (callAPI.method && callAPI.method === 'DELETE') {
    responsePromise = APIClient.delete(endpoint, callAPI.payload, shouldAuth);
  } else if (callAPI.method && callAPI.method === 'UPLOAD') {
    responsePromise = APIClient.upload(endpoint, callAPI.payload, shouldAuth);
  } else if (callAPI.method && callAPI.method === 'PATCH') {
    responsePromise = APIClient.patch(endpoint, callAPI.payload, shouldAuth);
  } else if (callAPI.method && callAPI.method === 'HEAD') {
    responsePromise = APIClient.head(endpoint, shouldAuth);
  } else {
    responsePromise = APIClient.get(endpoint, shouldAuth);
  }

  return responsePromise.then(
    response => next({
      type: successType,
      response,
    }),
    error => next({
      errorMessage: error.message || 'There was an error.',
      error,
      type: errorType,
    }),
  );
};
