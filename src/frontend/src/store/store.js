import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise';

import reducers from './reducers';

import apiMiddleware from './_middlewares/api';

import { loadState, saveState } from './localStorage';

import { clearState } from './utils';

// const erraseState = process.env.REACT_APP_ENVIRONMENT === 'dev';

const erraseState = false;

let store;
if (erraseState) {
  store = createStore(reducers, applyMiddleware(thunk, promiseMiddleware, apiMiddleware));
} else {
  const persistedState = loadState();
  const clearedPreviousState = clearState(persistedState)
  store = createStore(reducers, clearedPreviousState, applyMiddleware(thunk, promiseMiddleware, apiMiddleware));
}

store.subscribe(() => {
  saveState(store.getState());
});

export default store;
