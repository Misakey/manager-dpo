import {
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  REFRESH_SUCCESS,
  CREATE_ACCOUNT_SUCCESS,
  UPLOAD_PUBKEY_SUCCESS,
  // UPLOAD_PUBKEY_ERROR,
  END_ONBOARDING_SUCCESS,
  // END_ONBOARDING_ERROR,
  UPDATE_ONBOARDING_STATUS,
  LOAD_PRIVKEY_SUCCESS,
  LOAD_PRIVKEY_ERROR,
} from '../actions/types';

const initialState = {
  user: null,
  isLogged: false,
  key: null,
  isRegistrationDone: false,
};

// TODO: Manager the ERROR cases, with an error managment system (maybe alert the user, tell to reload, ... ?)

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return { ...state, isLogged: true, user: action.response.body };

    case LOGIN_ERROR:
      return { ...state, isLogged: false, user: null };

    case CREATE_ACCOUNT_SUCCESS:
      return { ...state, isRegistrationDone: true };

    case REFRESH_SUCCESS:
      return {
        ...state, isLogged: true, user: action.response.body, isRegistrationDone: true,
      };

    case UPDATE_ONBOARDING_STATUS:
      return { ...state, user: { ...state.user, onboarding_status: action.newStatus } };

    case UPLOAD_PUBKEY_SUCCESS:
      return { ...state, user: { ...state.user, onboarding_status: 'key_generated' } };

    case END_ONBOARDING_SUCCESS:
      return { ...state, user: action.response.body };

    case LOAD_PRIVKEY_SUCCESS:
      return { ...state, key: action.key };

    case LOAD_PRIVKEY_ERROR:
      return { ...state, key: action.key };

    default:
      return state;
  }
}
