import {
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  REFRESH_SUCCESS,
  REFRESH_ERROR,
  CREATE_ACCOUNT_SUCCESS,
  CREATE_ACCOUNT_ERROR,
  UPLOAD_PUBKEY_SUCCESS,
  UPLOAD_PUBKEY_ERROR,
  END_ONBOARDING_SUCCESS,
  END_ONBOARDING_ERROR,
  DELETE_USER_SUCCESS,
  DELETE_USER_ERROR,
  LOAD_PRIVKEY_SUCCESS,
  LOAD_PRIVKEY_ERROR,
  LOGOUT,
} from './types';
import { CALL_API } from '../../_middlewares/api';
import Keys from '../../../utils/crypto/keys';

export const login = sub => (dispatch) => {
  // Todo a switch case between DPO / user
  // Or maybe in backend manage "user" profile, with 2 types: dpo / user, and unify here
  dispatch({
    [CALL_API]: {
      endpoint: `/users/auth0/${sub}`,
      shouldAuth: true,
      types: [LOGIN_SUCCESS, LOGIN_ERROR],
    },
  });
};

export const logoutAction = () => (dispatch) => {
  return dispatch({
      type: LOGOUT,
    });
};

export const refreshUserAction = sub => (dispatch) => {
  dispatch({
    [CALL_API]: {
      endpoint: `/users/auth0/${sub}`,
      shouldAuth: true,
      types: [REFRESH_SUCCESS, REFRESH_ERROR],
    },
  });
};

export const createAccountAction = accountInformations => (dispatch) => {
  const formData = new FormData();

  formData.append('signature', accountInformations.signature, 'signature.png');
  formData.append('sub', accountInformations.sub);
  formData.append('email', accountInformations.email);
  formData.append('firstName', accountInformations.firstName);
  formData.append('familyName', accountInformations.familyName);
  formData.append('country', accountInformations.country);

  return dispatch({
    [CALL_API]: {
      endpoint: '/users',
      shouldAuth: false,
      method: 'UPLOAD',
      payload: formData,
      types: [CREATE_ACCOUNT_SUCCESS, CREATE_ACCOUNT_ERROR],
    },
  });
};

export const uploadPublicKeyAction = publicKey => (dispatch) => {
  dispatch({
    [CALL_API]: {
      endpoint: '/users/publicKey',
      shouldAuth: true,
      method: 'POST',
      payload: publicKey,
      types: [UPLOAD_PUBKEY_SUCCESS, UPLOAD_PUBKEY_ERROR],
    },
  });
};

export const endOnboardingAction = () => (dispatch) => {
  return dispatch({
    [CALL_API]: {
      endpoint: '/users/end-onboarding',
      shouldAuth: true,
      types: [END_ONBOARDING_SUCCESS, END_ONBOARDING_ERROR],
    },
  });
};

export const deleteAccountAction = auth0Id => (dispatch) => {
  return dispatch({
    [CALL_API]: {
      endpoint: '/users',
      shouldAuth: true,
      method: 'DELETE',
      payload: { auth0_id: auth0Id },
      types: [DELETE_USER_SUCCESS, DELETE_USER_ERROR],
    },
  });
};

export const loadPrivKeyAction = (encryptedSecretKey, password, successCallback) => (dispatch) => {
  // Get full key from privkey
  const keys = new Keys();
  keys.decryptSecretKey(password, encryptedSecretKey, (error) => {
    if (!error) {
      successCallback();
      return dispatch({
        type: LOAD_PRIVKEY_SUCCESS,
        key: {
          isValid: true,
          pubkey: keys.publicKey,
          privkey: keys.secretKey,
          encryptedPrivkey: encryptedSecretKey,
        },
      });
    }
    console.log(error);
    return dispatch({
      type: LOAD_PRIVKEY_ERROR,
      key: {
        isValid: false,
        error,
      },
    });
  });
};
