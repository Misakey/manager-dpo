export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';
export const REFRESH_SUCCESS = 'REFRESH_SUCCESS';
export const REFRESH_ERROR = 'REFRESH_ERROR';
export const UPLOAD_PUBKEY_SUCCESS = 'UPLOAD_PUBKEY_SUCCESS';
export const UPLOAD_PUBKEY_ERROR = 'UPLOAD_PUBKEY_ERROR';
export const END_ONBOARDING_SUCCESS = 'END_ONBOARDING_SUCCESS';
export const END_ONBOARDING_ERROR = 'END_ONBOARDING_ERROR';
export const UPDATE_ONBOARDING_STATUS = 'UPDATE_ONBOARDING_STATUS';
export const DELETE_USER_SUCCESS = 'DELETE_USER_SUCCESS';
export const DELETE_USER_ERROR = 'DELETE_USER_ERROR';
export const LOAD_PRIVKEY_SUCCESS = 'LOAD_PRIVKEY_SUCCESS';
export const LOAD_PRIVKEY_ERROR = 'LOAD_PRIVKEY_ERROR';
export const CREATE_ACCOUNT_SUCCESS = 'CREATE_ACCOUNT_SUCCESS';
export const CREATE_ACCOUNT_ERROR = 'CREATE_ACCOUNT_ERROR';
export const LOGOUT = 'LOGOUT';
