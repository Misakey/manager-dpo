import {
  GET_COMPANY_INFORMATIONS_SUCCESS,
  GET_COMPANY_INFORMATIONS_ERROR,
  CREATE_DPO_ACCOUNT_SUCCESS,
  CREATE_DPO_ACCOUNT_ERROR,
} from './types';
import { CALL_API } from '../../_middlewares/api';

export const getCompanyInformationsWithEmailAction = (uuid, email) => (dispatch) => {
  dispatch({
    [CALL_API]: {
      endpoint: `/companies/${uuid}/${email}`,
      shouldAuth: false,
      method: 'GET',
      types: [GET_COMPANY_INFORMATIONS_SUCCESS, GET_COMPANY_INFORMATIONS_ERROR],
    },
  });
};

export const createDpoAccountAction = (uuid, sub, email) => dispatch => dispatch({
  [CALL_API]: {
    endpoint: '/dpos',
    shouldAuth: false,
    method: 'POST',
    payload: { sub, email, company_uuid: uuid },
    types: [CREATE_DPO_ACCOUNT_SUCCESS, CREATE_DPO_ACCOUNT_ERROR],
  },
});
