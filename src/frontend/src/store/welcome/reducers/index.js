import {
  GET_COMPANY_INFORMATIONS_SUCCESS,
  GET_COMPANY_INFORMATIONS_ERROR,
  CREATE_DPO_ACCOUNT_SUCCESS,
} from '../actions/types';

const initialState = {
  company: null,
  shouldRedirectToLoginPage: false,
  isRegistrationDone: false,
};
export { initialState };

// TODO: Manager the ERROR cases, with an error managment system (maybe alert the user, tell to reload, ... ?)

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case GET_COMPANY_INFORMATIONS_SUCCESS:
      if (action.response.httpStatus === 200) {
        return { ...state, company: action.response.body };
      }
      return state;

    case GET_COMPANY_INFORMATIONS_ERROR:
      return { ...state, shouldRedirectToLoginPage: true };

    case CREATE_DPO_ACCOUNT_SUCCESS:
      return { ...state, isRegistrationDone: true };

    default:
      return state;
  }
}
