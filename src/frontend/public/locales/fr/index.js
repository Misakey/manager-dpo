import translationWelcomeFR from './welcome.json';
import translationDashboardFR from './dashboard.json';

export default {
  welcome: translationWelcomeFR,
  dashboard: translationDashboardFR,
};
