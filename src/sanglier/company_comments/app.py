from flask import Flask
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView

from misakey_main.models import Company
from misakey_main.resources.database import session

dbSession = session()

app = Flask(__name__)
app.secret_key = '80f14dc9290ecd46a31854ccac0b99899dcf0ef7db2d90d70c6dab7aac7285ebb0bb8fbad5455a68de03f45119379b9b9e23b3ff8eed9d8b15231ae99c39831ca116f24a7387a3455c7300a9e86b41bd1248d655ca74ff96cb3b7d576ab4b41a76797c75107292646366693137f4b60a4bf330c2d65033e1315d0a0002460332'

# set optional bootswatch theme
app.config['FLASK_ADMIN_SWATCH'] = 'superhero'

class CompanyView(ModelView):
    def __init__(self, model, session, *args, **kwargs):
        super(CompanyView, self).__init__(model, session, *args, **kwargs)
        self.static_folder = 'static'
        self.endpoint = 'admin'

    column_list = ['name', 'homepage', 'dpo_email', 'comment', 'status']
    form_excluded_columns = ['uuid', 'users', 'dpos', 'emails', 'date_creation', 'last_weekly_email_date']
    can_delete = False  # disable model deletion
    can_edit = False
    page_size = 25  # the number of entries to display on the list view
    can_view_details = False
    column_searchable_list = ['name', 'comment', 'dpo_email']
    column_filters = ['status']
    column_editable_list = ['comment', 'status']
    edit_modal = True
    form_columns = []
    column_default_sort = ('name', False)
    form_widget_args = {
        'status':{
            'disabled':True
        }
    }

    def create_form(self):
        form = super(CompanyView, self).create_form()
        form.status.data = "init_idle"
        return form

admin = Admin(
    app,
    name='companies', 
    template_mode='bootstrap3',
    url="/company-comments/",
    index_view=CompanyView(Company, dbSession, url="/company-comments/"),
    endpoint='companies'
)

if __name__ == "__main__":
    app.run(port=3333, host="0.0.0.0", debug=True)
