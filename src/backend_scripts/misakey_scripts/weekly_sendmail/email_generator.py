import os.path

from datetime import datetime 

from email.message import EmailMessage
from email.headerregistry import Address

from misakey_main.models.user_company_links import LinkStatus
from misakey_main.models.emails import EmailType
from misakey_main.resources.tools.date import is_today, is_last_n_days
from misakey_main.resources.config import config


def generate_email_object(email_to, email_type, company, is_test, is_spamtest):
    email_object = EmailMessage()
    if is_spamtest:
        email_object["To"] = "ins-dgepyya6@isnotspam.com, test-6o5zm@mail-tester.com, st-3-630dpq4vvo@glockapps.com, ajay.silicomm@gmail.com, ajaygoel999@gmail.com, ajay@parttimesnob.com, test@chromecompete.com, ajay@ajaygoel.net, ajay@reiaofmacomb.com, ajay@chabadslo.com, ajay@ocluke.com, ajay@theisopros.com, ajay@the5disciplines.org, ajay@gmailgenius.com, test@ajaygoel.org, me@dropboxslideshow.com, test@wordzen.com, rajgoel8477@gmail.com, briansmith8477@gmail.com, ajay@butterclaw.com"
    elif is_test:
        email_object["To"] = "DEBUG <antoine.vadot@misakey.com>"
    else:
        recipients = ["DPO de {} <{}>".format(company.name, to) for to in email_to]
        email_object["To"] = ", ".join(recipients)
    email_object["From"] = "Arthur Blanchon <arthur@love.misakey.com>"
    email_object["Subject"] = "À l'attention de Madame, Monsieur, DPO de {}".format(
        company.name
    )
    email_object.add_header(
        "X-SES-CONFIGURATION-SET", "misakey_dashboard_notifications_confset"
    )

    plaintext_content, html_content = generate_email_content(email_type, company)

    email_object.set_content(plaintext_content, subtype="plain", charset="utf-8")

    email_object.add_alternative(html_content, subtype="html", charset="utf-8")

    return email_object


def generate_email_content(email_type, company):
    plaintext_content = ""
    html_content = ""

    ## TODO: Add Matomo tracker campain to emails links

    if email_type == EmailType.not_onboarded_new_requests:
        registration_link = "{base_link}?pk_campaign={campaing_name}&pk_kwd={sub_campaing}".format(
            base_link=company.generate_register_link(),
            campaing_name="DPORegistrationEmail",
            sub_campaing=datetime.utcnow().strftime("%Y-%m-%d"),
        )

        not_onboarded_text_file_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "email_templates/not_onboarded.txt",
        )
        with open(not_onboarded_text_file_path, "r") as template_file:
            plaintext_content = template_file.read()

        plaintext_content = plaintext_content.format(
            company_name=company.name,
            company_homepage=company.homepage,
            dpo_email=company.dpo_email,
            registration_link=registration_link,
            company_logo=company.logo,
        )

        not_onboarded_html_file_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "email_templates/not_onboarded.html",
        )
        with open(not_onboarded_html_file_path, "r") as template_file:
            html_content = template_file.read()

        html_content = html_content.format(
            company_name=company.name,
            company_homepage=company.homepage,
            dpo_email=company.dpo_email,
            registration_link=registration_link,
            company_logo=company.logo,
        )

    return plaintext_content, html_content
