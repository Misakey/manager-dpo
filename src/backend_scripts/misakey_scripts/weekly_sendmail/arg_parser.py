import argparse

default_new_users_number = 10
default_days_between_last_email = 6


def get_cli_args():

    arg_parser = argparse.ArgumentParser(
        description="Send weekly email to DPO to ask them to answer the requests"
    )

    arg_parser.add_argument(
        "--days",
        dest="days_between_last_email",
        help="How much days since the last email was sent to resend an email",
        required=False,
        default=default_days_between_last_email,
        type=int,
    )

    arg_parser.add_argument(
        "--non-interactive-input",
        dest="is_input_interactive",
        help="Don't ask anything to user, use default values or args",
        action="store_false",
    )

    arg_parser.add_argument(
        "--force",
        dest="should_ask_before_doing_anything",
        help="Ask user before anything that will touch the database or send emails",
        action="store_false",
    )

    arg_parser.add_argument(
        "--dry",
        dest="dry_run",
        help="Don't actually do anything, just doing a step by step simulation",
        action="store_true",
    )

    arg_parser.add_argument(
        "--test",
        dest="test",
        help="Will send one email (first company) to administrator (antoine.vadot@misakey.com)",
        action="store_true",
    )

    arg_parser.add_argument(
        "--spam-test",
        dest="spam",
        help="Will send one email (first company) to spam tests addresses",
        action="store_true",
    )

    return arg_parser.parse_args()


def get_interactive_args():
    print("If you want to use the CLI with args just type `python index.py --help`")
    print("We'll now ask you interactively the different data needed")
    print("Warning: Dryrun is not available in interactive mode \n")

    days_between_last_email = input(
        "Send a mail if last email is older then how much days ? [default: {}] ".format(
            default_days_between_last_email
        )
    )
    try:
        days_between_last_email = max(0, int(users_number))
    except:
        days_between_last_email = default_days_between_last_email

    print("")
    return days_between_last_email
