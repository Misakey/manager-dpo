# Reflexion, faire une queue ? (SQS / Rabbit)

from os import environ
from datetime import datetime, timedelta
from pprint import pprint
from email.message import EmailMessage

from sqlalchemy import or_, and_

from misakey_main.resources.database import session as session_generator
from misakey_main.models import Company, User, UserCompanyLink, Message, Email
from misakey_main.models.companies import CompanyStatus
from misakey_main.models.users import OnboardingStatus
from misakey_main.models.messages import MessageType
from misakey_main.models.user_company_links import LinkStatus
from misakey_main.models.emails import EmailType

from misakey_main.services.email.default import email_service

from misakey_scripts.weekly_sendmail.email_generator import generate_email_object
from misakey_scripts.weekly_sendmail.arg_parser import (
    get_cli_args,
    get_interactive_args,
)
from misakey_scripts.weekly_sendmail.cli_tools import cli_confirm

try:
    session = session_generator()

    args = get_cli_args()

    days_between_last_email = args.days_between_last_email

    if args.is_input_interactive:
        days_between_last_email = get_interactive_args()

    print("Will work with those data:")
    print(
        "Days between last email: {}, dry run: {}, ask before doing anything: {}\n".format(
            days_between_last_email, args.dry_run, args.should_ask_before_doing_anything
        )
    )

    n_days_ago = datetime.today() - timedelta(days=days_between_last_email)

    companies = (
        session.query(Company)
        .filter(
            or_(
                Company.status == CompanyStatus.not_onboarded,
                Company.status == CompanyStatus.onboarded,
            )
        )
        .filter(
            or_(
                Company.last_weekly_email_date == None,
                Company.last_weekly_email_date <= n_days_ago,
            )
        )
        .limit(1 if args.test or args.spam else None)
        .all()
    )

    emails_to_send = []

    n = len(companies)
    print("First 5 companies:")
    for companie in companies[:5]:
        print("{} - {}".format(companie.name, companie.dpo_email))

    cli_confirm(
        args.should_ask_before_doing_anything,
        "We will now send emails to {}".format(len(companies)),
    )

    emails = []
    i = 0
    for company in companies:
        i += 1
        if i % 10 == 0:
            print("{} / {}".format(i, n))
        # Get data to create the email
        # Send email
        # Update database (create message)

        if company.status == CompanyStatus.not_onboarded:
            email_to = [company.dpo_email]
            email_type = EmailType.not_onboarded_new_requests

        elif company.status == CompanyStatus.onboarded:
            email_to = []
            email_type = EmailType.not_onboarded_new_requests
            for dcl in company.dpos:
                email_to.append(dcl.dpo.email)

        email_object = generate_email_object(email_to, email_type, company, args.test, args.spam)

        if not args.dry_run:

            email_well_sent, infos = email_service.send_email(email_object)
            if email_well_sent:
                company.last_weekly_email_date = datetime.utcnow()
                email_id = infos["MessageId"]

                if email_well_sent:
                    # Add email to database
                    email_db_object = Email(email_id)
                    email_db_object.company_id = company.id
                    email_db_object.email_to = ", ".join(email_to)
                    email_db_object.email_type = email_type
                    session.add(email_db_object)
                    session.commit()

                    # Adding messages to ucls
                    for ucl in company.users:
                        if ucl.messages:
                            last_message = ucl.messages[-1]
                            last_message_type_value = last_message.message_type.value
                            if last_message_type_value % 100 == 0:
                                last_message_type_value_mod_100 = (
                                    last_message_type_value // 100
                                )
                                if last_message_type_value_mod_100 == 1:
                                    last_message.message_type = (
                                        MessageType.accessRequestSent
                                    )
                                    last_message.date = datetime.utcnow()
                                    ucl.last_interaction = last_message.date
                                elif last_message_type_value_mod_100 == 2:
                                    last_message.message_type = (
                                        MessageType.portabilityRequestAsked
                                    )
                                    last_message.date = datetime.utcnow()
                                    ucl.last_interaction = last_message.date
                                elif last_message_type_value_mod_100 == 3:
                                    last_message.message_type = (
                                        MessageType.deleteRequestAsked,
                                        ucl.id,
                                    )
                                    last_message.date = datetime.utcnow()
                                    ucl.last_interaction = last_message.date

                    session.commit()
            else:
                print(infos)
                print(company.dpo_email)
                session.rollback()
        else:
            print("Email dry sent to {} - {}".format(company.name, email_object["To"]))

except Exception as e:
    raise e
finally:
    session_generator.remove()
