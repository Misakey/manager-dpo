def cli_confirm(should_confirm, message):
    print(message)
    if should_confirm:
        print("Presse enter to continue, Ctrl+C to quit")
        input()
