import json
from datetime import datetime

from db import session as dbSession, Email
from sqlalchemy.orm.attributes import flag_modified

def lambda_handler(event, context):
    local_session = dbSession()
    message = json.loads(event['Records'][0]['Sns']['Message'])
    message_id = message['mail']['messageId']
    print("New Message")
    print(message)

    email = local_session.query(Email).filter(Email.email_provider_message_id == message_id).one_or_none()
    if email:
        notification_type = message['eventType']
        handlers.get(notification_type, handle_unknown_type)(message, email, local_session)
    else:
        print("No mail found for ID {}".format(message_id))
        raise Exception("No email found for ID {}".format(message_id))


def handle_bounce(message, email, local_session):
    if not email.is_bounced:
        timestamp = message['bounce']['timestamp']
        message_datetime = datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")
        email.is_bounced = True
        email.date_bounce = message_datetime
        bounce_informations = []
    else:
        bounce_informations = email.bounce_informations
    bounce_data = message['complaint']
    bounce_informations.append(bounce_data)
    email.bounce_informations = bounce_informations

    flag_modified(email, "bounce_informations")
    local_session.add(email)
    local_session.commit()


def handle_complaint(message, email, local_session):
    if not email.is_complaint:
        timestamp = message['complaint']['timestamp']
        message_datetime = datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")
        email.is_complaint = True
        email.date_complaint = message_datetime
        complaint_informations = []
    else:
        complaint_informations = email.complaint_informations
    complaint_data = message['complaint']
    complaint_informations.append(complaint_data)
    email.complaint_informations = complaint_informations

    flag_modified(email, "complaint_informations")
    local_session.add(email)
    local_session.commit()


def handle_delivery(message, email, local_session):
    timestamp = message['delivery']['timestamp']
    message_datetime = datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")
    email.is_delivered = True
    email.date_deliver = message_datetime
    local_session.commit()

def handle_click(message, email, local_session):
    if not email.is_clicked:
        timestamp = message['click']['timestamp']
        message_datetime = datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")
        email.is_clicked = True
        email.date_first_click = message_datetime
        click_informations = []
    else:
        click_informations = email.click_informations
    click_data = { 'timestamp': message['click']['timestamp'], 'link': message['click']['link'] }
    click_informations.append(click_data)
    email.click_informations = click_informations

    flag_modified(email, "click_informations")
    local_session.add(email)
    local_session.commit()


def handle_open(message, email, local_session):
    if not email.is_opened:
        timestamp = message['open']['timestamp']
        message_datetime = datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")
        email.is_open = True
        email.date_first_open = message_datetime
        local_session.commit()

def handle_unknown_type(message, email, local_session):
    print("Unknown message type:\n%s" % json.dumps(message))
    raise Exception("Invalid message type received: {}".format(message['eventType']))

def handle_send(message, email, local_session):
    pass


handlers = {
    "Bounce": handle_bounce,
    "Complaint": handle_complaint,
    "Delivery": handle_delivery,
    "Click": handle_click,
    "Open": handle_open,
    "Send": handle_send,
}
