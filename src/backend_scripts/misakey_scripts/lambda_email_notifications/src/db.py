# coding=utf-8
from datetime import datetime
import enum
import os

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

from sqlalchemy import (
    Column,
    Integer,
    DateTime,
    JSON,
    String,
    Enum,
    Boolean,
)

engine = create_engine(
    os.environ["SQLALCHEMY_URL"], client_encoding="utf8", pool_size=10, max_overflow=20
)
session = scoped_session(sessionmaker(bind=engine))


class EmailType(enum.Enum):
    not_onboarded_new_requests = 10
    onboarded_new_requests = 20
    # Will probably need ghost types


class Email(Base):
    __tablename__ = "emails"

    id = Column(Integer, primary_key=True, autoincrement=True)

    email_provider_message_id = Column(String)

    email_to = Column(String)

    email_type = Column(Enum(EmailType))

    date_sent = Column(DateTime)

    is_delivered = Column(Boolean, default=False)
    date_deliver = Column(DateTime)

    is_bounced = Column(Boolean, default=False)
    date_bounce = Column(DateTime)
    bounce_informations = Column(JSON)

    is_complaint = Column(Boolean, default=False)
    date_complaint = Column(DateTime)
    complaint_informations = Column(JSON)

    is_opened = Column(Boolean, default=False)
    date_first_open = Column(DateTime)

    is_clicked = Column(Boolean, default=False)
    date_first_click = Column(DateTime)
    click_informations = Column(JSON)

    company_id = Column(Integer)

    def __init__(self, email_provider_message_id=None):
        self.email_provider_message_id = email_provider_message_id
        self.date_sent = datetime.utcnow()

    def __repr__(self):
        return "<Email(id={id}, to={email_to}, date={date})>".format(
            id=self.id, email_to=self.email_to, date=self.date_sent
        )

