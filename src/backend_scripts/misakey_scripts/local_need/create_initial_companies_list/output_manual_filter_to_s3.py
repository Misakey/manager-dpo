import csv
import os
from hashlib import sha256

from misakey_main.services.file_store.default import file_store_service

csv_manual_orgs = "ressources/orgs.csv"

output_csv_file = "output.csv"

first_batch_domains = []
with open(csv_manual_orgs, newline="") as csvfile:
    reader = csv.DictReader(csvfile)
    for domain in reader:
        if len(domain["should_import"]) > 0:
            if len(domain["logo"]) == 0:
                image_path = "ressources/logos/{}.png".format(domain["domain_name"])
                if os.path.isfile(image_path):
                    # Push it to S3
                    domain_hash = sha256(
                        domain["domain_name"].encode("utf-8")
                    ).hexdigest()
                    image_name = "{}.png".format(domain_hash)
                    is_sent, infos = file_store_service.send_file(
                        "dashboard/companies-logo/{}".format(image_name),
                        "static.misakey.com",
                        image_path,
                    )
                    if is_sent:
                        # print("Image sent for {}".format(image_name))
                        os.remove(image_path)
                        image_url = "https://static.misakey.com/dashboard/companies-logo/{}".format(
                            image_name
                        )
                        domain["logo"] = image_url
                    else:
                        print("Issue with S3 {}".format(infos))
                else:
                    print(
                        "No logo found for {} - {}".format(
                            domain["domain_name"], domain["should_import"]
                        )
                    )

            if (
                len(domain["logo"]) > 0
                and len(domain["email"]) > 0
                and len(domain["name"]) > 0
                and len(domain["domain_name"]) > 0
                and len(domain["homepage"]) > 0
                and len(domain["should_import"]) > 0
            ):
                first_batch_domains.append(
                    {
                        "email": domain["email"],
                        "name": domain["name"],
                        "logo": domain["logo"],
                        "homepage": domain["homepage"],
                        "domain_name": domain["domain_name"],
                        "score": domain["should_import"],
                    }
                )

with open(output_csv_file, "w", newline="") as csvfile:
    fieldnames = ["domain_name", "name", "homepage", "logo", "email", "score"]
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    for domain in first_batch_domains:
        writer.writerow(domain)
