import csv

from misakey_scripts.insert_new_companies.data_fetcher import DataFetcher
from misakey_scripts.logger import logger

csv_golden = "ressources/golden_ones.csv"
csv_manual = "ressources/manual_entries_treated.csv"
csv_ok = "ressources/ok_no_data.csv"

output_csv_file = "output.csv"

golden_domains = []
with open(csv_golden, newline="") as csvfile:
    reader = csv.DictReader(csvfile)
    for domain in reader:
        if (
            len(domain["golden_email"]) > 0
            and len(domain["organization_name"]) > 0
            and len(domain["organization_id"]) > 0
        ):
            golden_domains.append(
                {
                    "org_id": domain["organization_id"],
                    "domain_name": domain["organization_name"],
                    "email": domain["golden_email"],
                }
            )

manual_domains = []

with open(csv_manual, newline="") as csvfile:
    reader = csv.DictReader(csvfile)
    for domain in reader:
        if (
            len(domain["golden_email"]) > 0
            and len(domain["org_id"]) > 0
            and len(domain["golden_email"]) > 0
        ):
            manual_domains.append(
                {
                    "org_id": domain["org_id"],
                    "domain_name": domain["domain"],
                    "email": domain["golden_email"],
                }
            )

ok_domains = []
with open(csv_ok, newline="") as csvfile:
    reader = csv.DictReader(csvfile)
    for domain in reader:
        if (
            len(domain["golden_email"]) > 0
            and len(domain["organization_name"]) > 0
            and len(domain["organization_id"]) > 0
        ):
            ok_domains.append(
                {
                    "org_id": domain["organization_id"],
                    "domain_name": domain["organization_name"],
                    "email": domain["golden_email"],
                }
            )

agregated_domains = []
accepted_domains_name = []
accepted_email = []

for domain in ok_domains:
    domain["from"] = "OkNoData"
    domain["should_import"] = ""
    if (
        domain["domain_name"] not in accepted_domains_name
        and domain["email"] not in accepted_email
    ):
        agregated_domains.append(domain)
        accepted_email.append(domain["email"])
        accepted_domains_name.append(domain["domain_name"])

for domain in manual_domains:
    domain["from"] = "Manual"
    domain["should_import"] = ""
    if (
        domain["domain_name"] not in accepted_domains_name
        and domain["email"] not in accepted_email
    ):
        agregated_domains.append(domain)
        accepted_email.append(domain["email"])
        accepted_domains_name.append(domain["domain_name"])

for domain in golden_domains:
    domain["from"] = "DPO@"
    domain["should_import"] = ""
    if (
        domain["domain_name"] not in accepted_domains_name
        and domain["email"] not in accepted_email
    ):
        agregated_domains.append(domain)
        accepted_email.append(domain["email"])
        accepted_domains_name.append(domain["domain_name"])

logger.info(len(agregated_domains))
logger.info(
    "{} - {} - {}".format(len(manual_domains), len(ok_domains), len(golden_domains))
)

fetcher = DataFetcher(domains=agregated_domains)

logger.info("Fetching names")
fetcher.fetch_name()
logger.info("Fetching logos")
fetcher.fetch_logos()
logger.info("Saving")

with open(output_csv_file, "w", newline="") as csvfile:
    fieldnames = [
        "domain_name",
        "name",
        "homepage",
        "logo",
        "email",
        "org_id",
        "from",
        "should_import",
    ]
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    for domain in fetcher.domains:
        writer.writerow(domain)
