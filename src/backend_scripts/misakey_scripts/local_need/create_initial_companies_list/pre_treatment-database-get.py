import csv
import psycopg2

scrappy_db_connection_string = (
    "dbname='scrappy' user='user' host='172.17.0.3' password='password'"
)

csv_manual = "ressources/manual_entries.csv"
csv_manual_treated = "ressources/manual_entries_treated.csv"

manual_domains = []
try:
    conn = psycopg2.connect(scrappy_db_connection_string)
    cur = conn.cursor()
    domains = []
    with open(csv_manual, newline="") as csvfile:
        reader = csv.DictReader(csvfile)
        print(sum(1 for row in reader))
    with open(csv_manual, newline="") as csvfile:
        reader = csv.DictReader(csvfile)
        i = 0
        for domain in reader:
            i += 1
            if i % 100 == 0:
                print(i)
            domain["org_id"] = ""
            if len(domain["golden_email"]) > 0:
                cur.execute(
                    """
                    SELECT DISTINCT organizations.organization_id FROM organizations
                    WHERE organization_name = '{}';""".format(
                        domain["domain"]
                    )
                )
                orgs = cur.fetchall()
                if len(orgs) == 1:
                    domain["org_id"] = orgs[0][0]
            domains.append(domain)

    with open(csv_manual_treated, "w", newline="") as csvfile:
        fieldnames = [
            "origin_domain",
            "is_scrapped",
            "domain",
            "golden_email",
            "golden_score",
            "emails",
            "org_id",
        ]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()
        for domain in domains:
            insert_domain = {
                "origin_domain": domain["origin_domain"],
                "is_scrapped": domain["is_scrapped"],
                "domain": domain["domain"],
                "golden_email": domain["golden_email"],
                "golden_score": domain["golden_score"],
                "emails": domain["emails"],
                "org_id": domain["org_id"],
            }
            writer.writerow(insert_domain)
except Exception as e:
    raise e
