import logging, sys

# Configuring the logger handler.
# 2 levels:
#   stdout => info
#   file => info

logFormatter = logging.Formatter(
    fmt="%(asctime)s [%(levelname)-5.5s]  %(message)s", datefmt="%d/%m/%Y %H:%M:%S"
)
logger = logging.getLogger("misakey_logger")
logger.setLevel(logging.DEBUG)

fileHandler = logging.FileHandler("log.log")
fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.INFO)

logger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler(sys.stdout)
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.DEBUG)
logger.addHandler(consoleHandler)
