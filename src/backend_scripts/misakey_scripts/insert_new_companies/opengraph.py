# encoding: utf-8

# Code from https://github.com/erikriver/opengraph
# Copied here to translate it in python3 and adapt with our needs

import re

import requests

try:
    from bs4 import BeautifulSoup
except ImportError:
    from BeautifulSoup import BeautifulSoup

global import_json
try:
    import json

    import_json = True
except ImportError:
    import_json = False


class OpenGraph(dict):
    """
    """

    required_attrs = ["title", "image", "url"]

    def __init__(self, url=None, **kwargs):
        self._url = ""

        for k in kwargs.keys():
            self[k] = kwargs[k]

        dict.__init__(self)

        if url is not None:
            self.fetch(url)

    def __setattr__(self, name, val):
        self[name] = val

    def __getattr__(self, name):
        return self[name]

    def fetch(self, url):
        """
        """
        try:
            resp = requests.get(url, timeout=3)
            if resp.ok:
                self._url = resp.url
                html = resp.text
                self.parser(html)
            else:
                # print("Error: {}".format(resp.status_code))
                pass
        except:
            pass

    def parser(self, html):
        """
        """
        if not isinstance(html, BeautifulSoup):
            self.doc = BeautifulSoup(html, "html.parser")
        else:
            self.doc = html
        ogs = self.doc.html.head.findAll(property=re.compile(r"^og"))
        for og in ogs:
            if og.has_attr(u"content"):
                self[og[u"property"][3:]] = og[u"content"]
        # Couldn't fetch all attrs from og tags, try scraping body

    def valid_attr(self, attr):
        return self.get(attr) and len(self[attr]) > 0

    def is_valid(self):
        return all([self.valid_attr(attr) for attr in self.required_attrs])

    def to_html(self):
        if not self.is_valid():
            return u'<meta property="og:error" content="og metadata is not valid" />'

        meta = u""
        for key, value in self.iteritems():
            meta += u'\n<meta property="og:%s" content="%s" />' % (key, value)
        meta += u"\n"

        return meta

    def to_json(self):
        # TODO: force unicode
        global import_json
        if not import_json:
            return "{'error':'there isn't json module'}"

        if not self.is_valid():
            return json.dumps({"error": "og metadata is not valid"})

        return json.dumps(self)

    def to_xml(self):
        pass

    def scrape_image(self):
        images = []
        if "doc" in self:
            images = [
                dict(img.attrs)["src"] for img in self.doc.html.body.findAll("img")
            ]

        if images:
            return images[0]

        return ""

    def scrape_title(self):
        try: 
            return self.doc.html.head.title.text
        except:
            return ""

    def scrape_url(self):
        return self._url

    def scrape_description(self):
        tag = self.doc.html.head.findAll("meta", attrs={"name": "description"})
        result = "".join([t["content"] for t in tag])
        return result
