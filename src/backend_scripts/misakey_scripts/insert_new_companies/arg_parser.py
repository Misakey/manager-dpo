import argparse

default_text_domains_file = "domains.txt"
default_csv_data_file = "domains-data.csv"


def get_cli_args():

    arg_parser = argparse.ArgumentParser(
        description="Get data for companies and add them to database"
    )

    arg_parser.add_argument(
        "--domains-file",
        dest="domains_file",
        help="The domains input txt file",
        required=False,
        default=default_text_domains_file,
        type=str,
    )

    arg_parser.add_argument(
        "--data-file",
        dest="csv_data_file",
        help="The enriched data csv file",
        required=False,
        default=default_csv_data_file,
        type=str,
    )

    group = arg_parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "--get",
        action="store_true",
        dest="is_get_mode",
        help="Get mode: enrich the input file to output file",
    )
    group.add_argument(
        "--save",
        action="store_true",
        dest="is_save_mode",
        help="Save mode: get the output file to database",
    )

    arg_parser.add_argument(
        "--force",
        dest="should_ask_before_doing_anything",
        help="Ask user before anything that will touch the database or send emails",
        action="store_false",
    )

    return arg_parser.parse_args()
