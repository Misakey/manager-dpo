import opengraph
import csv
import re
import os
import shutil
import requests
from hashlib import sha256

from PIL import Image

from sqlalchemy import or_, and_, func

from misakey_main.models import Company

from misakey_main.services.file_store.default import file_store_service
from misakey_scripts.insert_new_companies.tools import score_email


class DataSaver:

    logo_fetch_url_base = "http://logo.clearbit.com/{}"

    def __init__(self, csv_data_file_path, session):
        self.session = session

        with open(csv_data_file_path, newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            domains = []
            for domain in reader:
                domains.append(domain)
            self.domains = domains

    def save_to_database(self):
        for domain in self.domains:
            # Todo: find a cleaner way to select only on criterias defined
            if "score" not in domain:
                domain["score"] = 0
            else:
                try:
                    domain["score"] = int(domain["score"])
                except:
                    domain["score"] = 0

            empty_values = [None, ""]

            existing_company = (
                self.session.query(Company)
                .filter(
                    or_(
                        and_(
                            func.lower(Company.domain)
                            == func.lower(domain["domain_name"]),
                            domain["domain_name"] not in empty_values,
                        ),
                        and_(
                            func.lower(Company.name) == func.lower(domain["name"]),
                            domain["name"] not in empty_values,
                        ),
                        and_(
                            func.lower(Company.homepage)
                            == func.lower(domain["homepage"]),
                            domain["homepage"] not in empty_values,
                        ),
                        and_(
                            func.lower(Company.logo) == func.lower(domain["logo"]),
                            domain["logo"] not in empty_values,
                        ),
                        and_(
                            func.lower(Company.dpo_email)
                            == func.lower(domain["email"]),
                            domain["email"] not in empty_values,
                        ),
                    )
                )
                .one_or_none()
            )

            if existing_company:
                print("Existing company for {}".format(existing_company.name))
                print(existing_company.domain)

                if existing_company.domain in empty_values:
                    existing_company.domain = domain["domain_name"]
                if existing_company.name in empty_values:
                    existing_company.name = domain["name"]
                if existing_company.homepage in empty_values:
                    existing_company.homepage = domain["homepage"]
                if existing_company.dpo_email in empty_values:
                    existing_company.dpo_email = domain["email"]
                if existing_company.sorting_weight in empty_values:
                    existing_company.sorting_weight = domain["score"]
                if len(domain["logo"]) > 0:
                    existing_company.logo = domain["logo"]
            else:
                new_company = Company(
                    name=domain["name"],
                    logo=domain["logo"],
                    homepage=domain["homepage"],
                    domain=domain["domain_name"],
                    dpo_email=domain["email"],
                    sorting_weight=domain["score"],
                )
                self.session.add(new_company)
            self.session.commit()
