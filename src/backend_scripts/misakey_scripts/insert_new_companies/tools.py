import re


def score_email(email):
    rating = 0
    golden_keywords = ["dpo", "privacy", "rgpd", "gdpr", "cil"]
    silver_keywords = [
        "donnees",
        "data",
        "protection",
        "personnelle",
        "personal",
        "legal",
        "droit",
        "juridique",
    ]
    copper_keywords = ["contact", "welcome", "hello", "bonjour", "info"]

    handle_extractor = re.compile(r"@.+")
    handle = handle_extractor.sub("", email).lower()

    for keyword in golden_keywords:
        if keyword == handle:
            rating = max(rating, 80)
        else:
            if keyword in handle:
                if handle.index(keyword) == 1:
                    rating = max(rating, 70)
                else:
                    rating = max(rating, 60)

    for keyword in silver_keywords:
        if keyword == handle:
            rating = max(rating, 70)
        else:
            if keyword in handle:
                if handle.index(keyword) == 1:
                    rating = max(rating, 60)
                else:
                    rating = max(rating, 50)

    for keyword in copper_keywords:
        if keyword == handle:
            rating = max(rating, 45)
        else:
            if keyword in handle:
                if handle.index(keyword) == 1:
                    rating = max(rating, 40)
                else:
                    rating = max(rating, 35)
    return rating
