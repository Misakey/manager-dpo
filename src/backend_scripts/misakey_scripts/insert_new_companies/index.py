# Args: inputcsvfilepath, status (idle or start), mode (enrich, insert), outputcsvfile
# Get data from CSV file
# Check all already existing companies (take it aside)
# For all non existing companies
#   If mode = insert
#       Create the company (status)
#       Create the DPO (ask for confirmation if email already exists) ? (link it to multiple companies ??)
#   if mode = enrich
#       Get logo
#       Get name
#       Get homepage
#

# Reflexion, faire une queue ? (SQS / Rabbit)

import csv

from misakey_main.resources.database import session


from misakey_scripts.insert_new_companies.arg_parser import get_cli_args
from misakey_scripts.insert_new_companies.data_fetcher import DataFetcher
from misakey_scripts.insert_new_companies.data_saver import DataSaver


try:
    args = get_cli_args()

    if args.is_get_mode:
        print("Will scrape domains from {}".format(args.domains_file))
        fetcher = DataFetcher(
            session=session,
            input_file_path=args.domains_file,
            output_file_path=args.csv_data_file,
        )
        print("{} domains will be fetched".format(fetcher.get_number_of_domains()))
        fetcher.fetch_everything()
        fetcher.save_to_file()
    elif args.is_save_mode:
        print("Will save domains data from {}".format(args.csv_data_file))
        saver = DataSaver(args.csv_data_file, session)
        saver.save_to_database()


except Exception as e:
    raise e
finally:
    session.remove()
