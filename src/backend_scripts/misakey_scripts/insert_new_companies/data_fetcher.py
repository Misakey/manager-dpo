import csv
import re
import os
import shutil
import requests
from hashlib import sha256

from PIL import Image

from misakey_main.models.companies import Company

from misakey_main.services.file_store.default import file_store_service
from misakey_scripts.insert_new_companies.tools import score_email
import misakey_scripts.insert_new_companies.opengraph as opengraph
from misakey_scripts.logger import logger


class DataFetcher:
    # Todo: dynamise that, or worse case scenario put it somewhere in the configuration
    dpo_study_database_connexion_uri = (
        "dbname='scrappy' user='user' host='172.17.0.3' password='password'"
    )
    logo_fetch_url_base = "http://logo.clearbit.com/{}"

    def __init__(
        self, session=None, input_file_path=None, output_file_path=None, domains=[]
    ):
        self.output_file_path = output_file_path

        if len(domains) > 0:
            self.domains = domains
        else:
            self.session = session

            with open(input_file_path) as file:
                domains = []
                for line in file:
                    stripped_line = line.strip()
                    if len(line) > 1:
                        domains.append({"domain_name": stripped_line})

                db_domains = (
                    self.session.query(Company.domain)
                    .filter(
                        Company.domain.in_(
                            [domain["domain_name"] for domain in domains]
                        )
                    )
                    .all()
                )
                db_domains = [domain[0] for domain in db_domains]

                # Removing all domains that are already in DB
                domains = [
                    domain
                    for domain in domains
                    if domain["domain_name"] not in db_domains
                ]

                self.domains = domains

    def get_number_of_domains(self):
        return len(self.domains)

    def fetch_everything(self):
        self.fetch_name()
        self.fetch_logos()
        self.get_emails()

    def fetch_name(self):
        logger.info("Will fetch {} domains name".format(len(self.domains)))
        for i, domain in enumerate(self.domains):
            if i % 100 == 0:
                logger.info("{} / {} domains names found".format(i, len(self.domains)))
            try:
                url = "https://{}".format(domain["domain_name"])
                og = opengraph.OpenGraph(url=url)

                title = ""
                if "site_name" in og:
                    title = og["site_name"]
                else:
                    full_title = og.scrape_title()
                    all_matches = re.findall(r".+? [-·|,;:]", full_title)
                    if len(all_matches) > 0:
                        title = all_matches[0][:-1].strip()
                    else:
                        title = domain["domain_name"].split(".")[-2].title()

                self.domains[i]["name"] = title
                self.domains[i]["homepage"] = og.scrape_url()

            except Exception as e:
                print(e)
                raise e

    def fetch_logos(self):
        os.makedirs("tmp", exist_ok=True)
        logger.info("Will fetch {} logos".format(len(self.domains)))
        for i, domain in enumerate(self.domains):
            if i % 100 == 0:
                logger.info("{} / {} logos found".format(i, len(self.domains)))
            self.fetch_logo(domain["domain_name"], i)
        # os.rmdir("tmp")

    def fetch_logo(self, domain, i):
        url = self.logo_fetch_url_base.format(domain)
        r = requests.get(url, stream=True)
        if r.status_code == 200:
            content_type = r.headers["Content-Type"]
            res = re.search("image/", content_type)
            if res is not None:
                ext = content_type[6:]
                if ext in ["png", "jpg", "jpeg", "gif"]:
                    domain_hash = sha256(domain.encode("utf-8")).hexdigest()
                    image_name = "{}.{}".format(domain_hash, ext)
                    image_path = "tmp/{}".format(image_name)
                    with open(image_path, "wb") as f:
                        r.raw.decode_content = True
                        shutil.copyfileobj(r.raw, f)
                    im = Image.open(image_path)
                    width, height = im.size
                    if width != height:
                        os.remove(image_path)
                        print("Error: Not squared")
                    else:
                        is_sent, infos = file_store_service.send_file(
                            "dashboard/companies-logo/{}".format(image_name),
                            "static.misakey.com",
                            image_path,
                        )
                        if is_sent:
                            # print("Image sent for {}".format(image_name))
                            os.remove(image_path)
                            image_url = "https://static.misakey.com/dashboard/companies-logo/{}".format(
                                image_name
                            )
                            self.domains[i]["logo"] = image_url
                        else:
                            print(infos)

                        # else:
                        #     print(
                        #         "ERROR while sending image to backend - {}".format(
                        #             infos
                        #         )
                        #     )
        #        else:
        #            print("Error: wring format {}".format(ext))
        #    else:
        #        print("Error: not an image - {}".format(content_type))
        # else:
        #    print("Error: {}".format(r.status_code))

    def get_emails(self):
        import psycopg2

        try:
            conn = psycopg2.connect(self.dpo_study_database_connexion_uri)
            cur = conn.cursor()
            for i, domain in enumerate(self.domains):
                cur.execute(
                    """
                    SELECT DISTINCT scenarios.email_next_iteration FROM organizations
                    JOIN scenarios ON scenarios.organization_id = organizations.organization_id
                    WHERE organization_name = '{}';""".format(
                        domain["domain_name"]
                    )
                )
                emails = cur.fetchall()

                best_scrore = -1
                best_email = ""
                for email_row in emails:
                    email = email_row[0]
                    email_score = score_email(email)
                    if best_scrore < email_score:
                        best_scrore = email_score
                        best_email = email
                self.domains[i]["email"] = best_email

        except:
            print("I am unable to connect to the database")

    def save_to_file(self):
        with open(self.output_file_path, "w", newline="") as csvfile:
            fieldnames = ["domain_name", "name", "homepage", "logo", "email"]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writeheader()
            for domain in self.domains:
                writer.writerow(domain)
