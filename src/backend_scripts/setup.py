from setuptools import setup, find_packages

name = "misakey_scripts"

requires = ["misakey_main"]

extras_require = {"dev": ["alembic", "black"]}


setup(
    name=name,
    description="Misakey scripts package. Used for backend cron, and manual operations",
    classifiers=["Programming Language :: Python"],
    author="Misakey",
    author_email="love@misakey.com",
    license="AGPLv3",
    packages=find_packages(),
    include_package_data=True,
    install_requires=requires,
    extras_require=extras_require,
)
