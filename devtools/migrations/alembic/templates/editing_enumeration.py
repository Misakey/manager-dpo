"""
Template for editing an enumeration
"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "RevID"
down_revision = "PrevID"
branch_labels = None
depends_on = None

# TOEDIT
type_name = "database_type_name"
table_name = "table_name"
column_name = "column_name"

old_options = ("oldOption1", "oldOption2")
new_options = ("oldOption1", "newOption1")

# /TOEDIT

tmp_options = tuple(set(new_options + old_options))

old_type = sa.Enum(*old_options, name=type_name)
new_type = sa.Enum(*new_options, name=type_name)
tmp_type = sa.Enum(*tmp_options, name="_{}".format(type_name))

table = sa.sql.table(table_name, sa.Column(column_name, new_type, nullable=False))


def upgrade():
    tmp_type.create(op.get_bind(), checkfirst=False)
    op.execute(
        "ALTER TABLE {table_name} ALTER COLUMN {column_name} TYPE _{type_name} USING {column_name}::text::_{type_name}".format(
            type_name=type_name, column_name=column_name, table_name=table_name
        )
    )

    # TOEDIT: mapping new value > old value for deleted vals
    op.execute(
        table.update()
        .where(table.c[column_name] == u"oldOption2")
        .values(status="newOption1")
    )
    # /TOEDIT

    old_type.drop(op.get_bind(), checkfirst=False)
    # Create and convert to the "new" status type
    new_type.create(op.get_bind(), checkfirst=False)

    op.execute(
        "ALTER TABLE {table_name} ALTER COLUMN {column_name} TYPE {type_name} USING {column_name}::text::{type_name}".format(
            type_name=type_name, column_name=column_name, table_name=table_name
        )
    )
    tmp_type.drop(op.get_bind(), checkfirst=False)
    # ### end Alembic commands ###


def downgrade():
    tmp_type.create(op.get_bind(), checkfirst=False)
    op.execute(
        "ALTER TABLE {table_name} ALTER COLUMN {column_name} TYPE _{type_name} USING {column_name}::text::_{type_name}".format(
            type_name=type_name, column_name=column_name, table_name=table_name
        )
    )

    # TOEDIT: mapping new value > old value for deleted vals
    op.execute(
        table.update()
        .where(table.c[column_name] == u"newOption1")
        .values(status="oldOption1")
    )
    # /TOEDIT

    new_type.drop(op.get_bind(), checkfirst=False)
    # Create and convert to the "old" status type
    old_type.create(op.get_bind(), checkfirst=False)
    op.execute(
        "ALTER TABLE {table_name} ALTER COLUMN {column_name} TYPE {type_name} USING {column_name}::text::{type_name}".format(
            type_name=type_name, column_name=column_name, table_name=table_name
        )
    )
    tmp_type.drop(op.get_bind(), checkfirst=False)
    # ### end Alembic commands ###
