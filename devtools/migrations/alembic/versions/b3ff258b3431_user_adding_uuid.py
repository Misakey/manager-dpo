"""User adding uuid

Revision ID: b3ff258b3431
Revises: 7928e7e9905e
Create Date: 2019-01-25 10:59:36.757364

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'b3ff258b3431'
down_revision = '7928e7e9905e'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('users', sa.Column('uuid', postgresql.UUID(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('users', 'uuid')
    # ### end Alembic commands ###
