"""Adding emails

Revision ID: 683f8a819ebf
Revises: 136eb1e968d4
Create Date: 2019-02-21 15:16:45.539283

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "683f8a819ebf"
down_revision = "136eb1e968d4"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "emails",
        sa.Column("id", sa.Integer(), autoincrement=True, nullable=False),
        sa.Column("email_provider_message_id", sa.String(), nullable=True),
        sa.Column("email_to", sa.String(), nullable=True),
        sa.Column(
            "email_type",
            sa.Enum(
                "not_onboarded_new_requests", "onboarded_new_requests", name="emailtype"
            ),
            nullable=True,
        ),
        sa.Column("date_sent", sa.DateTime(), nullable=True),
        sa.Column("is_delivered", sa.Boolean(), nullable=True),
        sa.Column("date_deliver", sa.DateTime(), nullable=True),
        sa.Column("is_bounced", sa.Boolean(), nullable=True),
        sa.Column("date_bounce", sa.DateTime(), nullable=True),
        sa.Column("bounce_informations", sa.JSON(), nullable=True),
        sa.Column("is_complaint", sa.Boolean(), nullable=True),
        sa.Column("date_complaint", sa.DateTime(), nullable=True),
        sa.Column("complaint_informations", sa.JSON(), nullable=True),
        sa.Column("is_opened", sa.Boolean(), nullable=True),
        sa.Column("date_first_open", sa.DateTime(), nullable=True),
        sa.Column("is_clicked", sa.Boolean(), nullable=True),
        sa.Column("date_first_click", sa.DateTime(), nullable=True),
        sa.Column("click_informations", sa.JSON(), nullable=True),
        sa.Column("company_id", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(["company_id"], ["companies.id"]),
        sa.PrimaryKeyConstraint("id"),
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("emails")
    # ### end Alembic commands ###
