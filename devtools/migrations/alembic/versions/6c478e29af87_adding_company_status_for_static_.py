"""Adding company status for static companies

Revision ID: 6c478e29af87
Revises: 9659eae9f666
Create Date: 2019-02-21 09:40:51.473030

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "6c478e29af87"
down_revision = "9659eae9f666"
branch_labels = None
depends_on = None


type_name = "companystatus"
column_name = "status"
table_name = "companies"
old_options = ("init_idle", "not_onboarded", "onboarded", "blacklisted", "disabled")
new_options = (
    "init_idle",
    "not_onboarded",
    "onboarded",
    "blacklisted",
    "disabled",
    "static",
)

tmp_options = tuple(set(new_options + old_options))

old_type = sa.Enum(*old_options, name=type_name)
new_type = sa.Enum(*new_options, name=type_name)
tmp_type = sa.Enum(*tmp_options, name="_{}".format(type_name))

table = sa.sql.table(table_name, sa.Column(column_name, new_type, nullable=False))


def upgrade():
    tmp_type.create(op.get_bind(), checkfirst=False)
    op.execute(
        "ALTER TABLE {table_name} ALTER COLUMN {column_name} TYPE _{type_name} USING {column_name}::text::_{type_name}".format(
            type_name=type_name, column_name=column_name, table_name=table_name
        )
    )

    old_type.drop(op.get_bind(), checkfirst=False)
    # Create and convert to the "new" status type
    new_type.create(op.get_bind(), checkfirst=False)

    op.execute(
        "ALTER TABLE {table_name} ALTER COLUMN {column_name} TYPE {type_name} USING {column_name}::text::{type_name}".format(
            type_name=type_name, column_name=column_name, table_name=table_name
        )
    )
    tmp_type.drop(op.get_bind(), checkfirst=False)
    # ### end Alembic commands ###


def downgrade():
    tmp_type.create(op.get_bind(), checkfirst=False)
    op.execute(
        "ALTER TABLE {table_name} ALTER COLUMN {column_name} TYPE _{type_name} USING {column_name}::text::_{type_name}".format(
            type_name=type_name, column_name=column_name, table_name=table_name
        )
    )

    # TO EDIT: mapping new value > old value for deleted vals
    op.execute(
        table.update()
        .where(table.c[column_name] == u"static")
        .values(status="disabled")
    )

    new_type.drop(op.get_bind(), checkfirst=False)
    # Create and convert to the "old" status type
    old_type.create(op.get_bind(), checkfirst=False)
    op.execute(
        "ALTER TABLE {table_name} ALTER COLUMN {column_name} TYPE {type_name} USING {column_name}::text::{type_name}".format(
            type_name=type_name, column_name=column_name, table_name=table_name
        )
    )
    tmp_type.drop(op.get_bind(), checkfirst=False)
    # ### end Alembic commands ###
