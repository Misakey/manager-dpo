"""Message complementary infos

Revision ID: c4a261c7774f
Revises: 40fd06afa148
Create Date: 2019-01-29 11:49:40.452808

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c4a261c7774f'
down_revision = '40fd06afa148'
branch_labels = None
depends_on = None


# Altering an enum, from column {column_name} in {table_name} with type {type_name}
# set those vars
# copy / paste the rest of the code

type_name = 'messagetype'
column_name = 'message_type'
table_name = 'messages'
old_options = ('accessRequestAsked', 'accessRequestSent', 'accessAccept', 'accessRefusal', 'portabilityRequestAsked', 'portabilityRequestSent', 'portabilityAccept', 'portabilityRefusal', 'deleteRequestAsked', 'deleteRequestSent', 'deleteAccept', 'deleteRefusal')
new_options = ('accessRequestAsked', 'accessRequestSent', 'accessAcceptData', 'accessAcceptNoData', 'accessRefusal', 'portabilityRequestAsked', 'portabilityRequestSent', 'portabilityAccept', 'portabilityRefusal', 'deleteRequestAsked', 'deleteRequestSent', 'deleteAccept', 'deleteRefusal')

# To copy
tmp_options = tuple(set(new_options + old_options))

old_type = sa.Enum(*old_options, name=type_name)
new_type = sa.Enum(*new_options, name=type_name)
tmp_type = sa.Enum(*tmp_options, name='_{}'.format(type_name))

messages = sa.sql.table(table_name, sa.Column(column_name, new_type, nullable=False))


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('messages', sa.Column('complementary_informations', sa.Text(), nullable=True))
    # ### end Alembic commands ###

    tmp_type.create(op.get_bind(), checkfirst=False)
    op.execute('ALTER TABLE {table_name} ALTER COLUMN {column_name} TYPE _{type_name} USING {column_name}::text::_{type_name}'.
        format(type_name=type_name, column_name=column_name, table_name=table_name))

    # TO EDIT: mapping old value > new value for deleted vals
    op.execute(messages.update().where(messages.c.message_type == u'accessAccept')
               .values(message_type='accessAcceptData'))

    old_type.drop(op.get_bind(), checkfirst=False)
    # Create and convert to the "new" message_type type
    new_type.create(op.get_bind(), checkfirst=False)

    op.execute('ALTER TABLE {table_name} ALTER COLUMN {column_name} TYPE {type_name} USING {column_name}::text::{type_name}'.
        format(type_name=type_name, column_name=column_name, table_name=table_name))
    tmp_type.drop(op.get_bind(), checkfirst=False)


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('messages', 'complementary_informations')
    # ### end Alembic commands ###

    # Convert 'output_limit_exceeded' message_type into 'timed_out'
    # Create a tempoary "_message_type" type, convert and drop the "new" type
    tmp_type.create(op.get_bind(), checkfirst=False)
    op.execute('ALTER TABLE {table_name} ALTER COLUMN {column_name} TYPE _{type_name} USING {column_name}::text::_{type_name}'.
        format(type_name=type_name, column_name=column_name, table_name=table_name))

    # TO EDIT: mapping new value > old value for deleted vals
    op.execute(messages.update().where(messages.c.message_type == u'accessAcceptData')
               .values(message_type='accessAccept'))
    op.execute(messages.update().where(messages.c.message_type == u'accessAcceptNoData')
               .values(message_type='accessAccept'))

    new_type.drop(op.get_bind(), checkfirst=False)
    # Create and convert to the "old" message_type type
    old_type.create(op.get_bind(), checkfirst=False)
    op.execute('ALTER TABLE {table_name} ALTER COLUMN {column_name} TYPE {type_name} USING {column_name}::text::{type_name}'.
        format(type_name=type_name, column_name=column_name, table_name=table_name))
    tmp_type.drop(op.get_bind(), checkfirst=False)
