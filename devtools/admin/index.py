from flask import Flask
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from os import path
import sys
import os

sys.path.append(
    path.join(
        #  misakey;   devtools;    migrations;  alembic;
        path.dirname(path.dirname(path.dirname(path.abspath(__file__)))),
        "src/backend/",
    )
)


from misakey_main.models import (
    User,
    Company,
    Key,
    Mandate,
    UserCompanyLink,
    Message,
    Dpo,
    DpoCompanyLink,
    Email,
)
from misakey_main.resources.database import session

dbSession = session()

app = Flask(__name__)
app.secret_key = "34fyJRRZJgxeq2pM9rMLiNdhxCZLbDTiTc6qsaQHpL9qDmM59PyHXVyxRgTwsg6zzwSDMoHLF6z7GcKYzzerhukuFZ4TN3BijewuBnC4h6retLFyarWiDANnE722LcQ5"

# set optional bootswatch theme
app.config["FLASK_ADMIN_SWATCH"] = "superhero"


class WithUUIDView(ModelView):
    form_excluded_columns = ["uuid"]


admin = Admin(app, name="Misakey App", template_mode="bootstrap3")
admin.add_view(WithUUIDView(User, dbSession))
admin.add_view(WithUUIDView(Company, dbSession))
admin.add_view(ModelView(Key, dbSession))
admin.add_view(ModelView(Mandate, dbSession))
admin.add_view(ModelView(UserCompanyLink, dbSession))
admin.add_view(WithUUIDView(Message, dbSession))
admin.add_view(WithUUIDView(Dpo, dbSession))
admin.add_view(ModelView(DpoCompanyLink, dbSession))
admin.add_view(ModelView(Email, dbSession))


app.run(port=4444, host="0.0.0.0")
