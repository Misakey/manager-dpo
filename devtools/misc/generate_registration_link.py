from misakey_main.models import Company
from misakey_main.models.companies import CompanyStatus

from misakey_main.resources.database import session as session_generator

try:
    session = session_generator()

    company = session.query(Company).filter(Company.status == CompanyStatus.not_onboarded).limit(1).one_or_none()
    if company:
        print(company.generate_register_link())
    else:
        print("There is no company not_onboarded")

except:
    pass
finally:
    session_generator.remove()