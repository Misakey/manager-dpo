cd ../../src/frontend

if [ -e .env.dpo.prod ] && [ -e .env.user.prod ]
then
    echo "Backing up .env.local"
    [ -e .env.local ] && mv .env.local .env.local.bak

    echo "Removing old build directory"
    rm -rf build

    echo "Preparing DPO build"
    cp public/index.dpo.html public/index.html
    cp .env.dpo.prod .env.local
    yarn build
    rm build/index.user.html
    rm build/index.dpo.html
    rm build/index.dev.html
    echo "Sending DPO build to S3"
    aws s3 sync build/ s3://dpo.misakey.com --delete
    echo "Creating cloudfront invalidation"
    aws cloudfront create-invalidation --distribution-id E2VHXEB08I7RPW --paths /*

    echo "Cleaning build"
    rm -fr build

    echo "Preparing citizen build"
    cp public/index.user.html public/index.html
    cp .env.user.prod .env.local
    yarn build
    rm build/index.user.html
    rm build/index.dpo.html
    rm build/index.dev.html
    echo "Sending citizen build to S3"
    aws s3 sync build/ s3://app.misakey.com --delete
    echo "Creating cloudfront invalidation"
    aws cloudfront create-invalidation --distribution-id E36C26TXHPRFZ --paths /*

    echo "Cleaning folder (rm build, restoring .env.local"
    rm -rf build

    rm .env.local 

    [ -e .env.local.bak ] && mv .env.local.bak .env.local

    cp public/index.dev.html public/index.html
else
    echo "Should set env files (.env.user.prod and .env.dpo.prod) in the src/frontend folder"
fi
